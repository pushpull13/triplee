package com.pushpull.triplee.custom

import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.Network
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.pushpull.triplee.R
import com.pushpull.triplee.dataClass.AppTheme
import com.pushpull.triplee.konstant.Path
import com.pushpull.triplee.konstant.Constant.Companion.Settings
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.io.File

abstract class BaseActivity : AppCompatActivity() {

	val job: Job = Job()
	val ioScope = CoroutineScope(Dispatchers.IO + job)
	val mainScope = CoroutineScope(Dispatchers.Main)
	val objectMapper: ObjectMapper = ObjectMapper().registerModule(KotlinModule())

	lateinit var localAppTheme: AppTheme

	var density: Float = 0f
	var aspectRatio: Float = 0f
	var widthDp: Int = 0
	var heightDp: Int = 0

	lateinit var sharedPreferencesAppConfig: SharedPreferences

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		sharedPreferencesAppConfig = getSharedPreferences(Settings.APP_CONFIG.name, MODE_PRIVATE)
		sharedPreferencesAppConfig.apply {
			setTheme(getInt(Settings.APP_THEME.name, R.style.Theme_Triplee_Aqua))
		}

		Path.ROOT_PATH = applicationContext.applicationInfo.dataDir
		File(Path.APP_DATA).mkdirs()
		File(Path.TRIP_FOLDER_NAME).mkdirs()

		getDisplayMetrics()

		localAppTheme = getLocalTheme(this)
	}

	fun onConnectivityChange(onNetworkAvailable: () -> Unit, onNetworkLost: () -> Unit) {
		val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
		connectivityManager.registerDefaultNetworkCallback(object : ConnectivityManager.NetworkCallback() {
			override fun onAvailable(network: Network) {
				Log.i(TAG, "network available : $network")
				onNetworkAvailable()
			}

			override fun onLost(network: Network) {
				Log.i(TAG, "network lost : $network")
				onNetworkLost()
			}
		})
	}

	fun showTransactionUpdatedToast() {
		mainScope.launch {
			Toast.makeText(this@BaseActivity, "Triplee Plus subscription updated.", Toast.LENGTH_LONG).show()
		}
	}

	fun showTransactionFailedToast() {
		mainScope.launch {
			Toast.makeText(this@BaseActivity, "There was a problem in transaction. Please click \"Restore Purchase\".", Toast.LENGTH_LONG).show()
		}
	}

	fun showPlusExpiredToast() {
		mainScope.launch {
			Toast.makeText(this@BaseActivity, "Triplee Plus has been expired.", Toast.LENGTH_LONG).show()
		}
	}

	fun showTransactionErrorToast() {
		mainScope.launch {
			Toast.makeText(this@BaseActivity, "There was a problem in transaction. Please click \"Restore Purchase\".", Toast.LENGTH_LONG).show()
		}
	}

	fun showErrorRetrievingDataToast() {
		mainScope.launch {
			Toast.makeText(this@BaseActivity, "There was an error retrieving transaction data. Please try again.", Toast.LENGTH_LONG).show()
		}
	}

	private fun getDisplayMetrics() {
		density = resources.displayMetrics.density
		aspectRatio = resources.displayMetrics.heightPixels.toFloat() / resources.displayMetrics.widthPixels.toFloat()
		widthDp = resources.configuration.screenWidthDp
		heightDp = resources.configuration.screenHeightDp
	}

	fun savePreference(key: String, value: Int) {
		with(sharedPreferencesAppConfig.edit()) {
			putInt(key, value)
			commit()
		}
	}

	val Int.d
		get() = (this * density).toInt()

	companion object {
		const val TAG = "BASE_ACTIVITY"
	}
}
