package com.pushpull.triplee.custom

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.location.Location
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.blue
import androidx.core.graphics.green
import androidx.core.graphics.red
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.asLiveData
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMapOptions
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.*
import com.pushpull.triplee.R
import com.pushpull.triplee.background.TripService
import com.pushpull.triplee.dataClass.AppTheme
import com.pushpull.triplee.dataClass.TripData
import com.pushpull.triplee.database.UserDatabase
import com.pushpull.triplee.database.place.Place
import com.pushpull.triplee.database.place.PlaceTableDao
import com.pushpull.triplee.konstant.ActivityId
import com.pushpull.triplee.konstant.Constant
import com.pushpull.triplee.utility.Utility
import kotlin.collections.List
import kotlin.collections.MutableList
import kotlin.collections.MutableMap
import kotlin.collections.component1
import kotlin.collections.component2
import kotlin.collections.first
import kotlin.collections.forEach
import kotlin.collections.get
import kotlin.collections.isNotEmpty
import kotlin.collections.last
import kotlin.collections.mutableListOf
import kotlin.collections.mutableMapOf
import kotlin.collections.set
import kotlin.collections.toList


class HomeMapView : MapView,
	SensorEventListener {
	constructor(context: Context) : super(context)
	constructor(context: Context, options: GoogleMapOptions) : super(context, options)

	private var density: Float = context.resources.displayMetrics.density

	private var localAppTheme: AppTheme = getLocalTheme()
	private var sharedPreferencesAppConfig: SharedPreferences = context.getSharedPreferences(Constant.Companion.Settings.APP_CONFIG.name, AppCompatActivity.MODE_PRIVATE)

	private val placeTable: PlaceTableDao
	private lateinit var placeLiveData: LiveData<List<Place>>
	private val placeObserver: Observer<List<Place>> = Observer { placeList ->
		placeMarkerMap.forEach { it.value.remove() }
		placeMarkerMap = mutableMapOf()

		placeList.forEach { place ->
			if (!placeMarkerMap.containsKey(place.primaryKey)) {
				if (googleMap != null) {
					val placeMarker = googleMap!!.addMarker(MarkerOptions()
						.icon(getTagBitmap(R.drawable.ic_map_marker_1, place.colorTag)?.let { BitmapDescriptorFactory.fromBitmap(it) })
						.position(LatLng(place.latitude, place.longitude))
						.anchor(0.5f, 1f)
						.title(place.primaryKey.toString())
					)
					if (placeMarker != null) {
						placeMarkerMap[place.primaryKey] = placeMarker
					}
				}
			}
		}
	}

	interface OnMapReadyListener {
		fun onMapReady(isMapReady: Boolean)
	}

	private var onMapReadyListener: OnMapReadyListener? = null
	fun onMapReady(listener: OnMapReadyListener) {
		onMapReadyListener = listener
	}

	interface OnCompassValueChangeListener {
		fun onCompassValueChange(azimuth: Float)
	}

	private var onCompassValueChangeListener: OnCompassValueChangeListener? = null
	fun onCompassValueChange(listener: OnCompassValueChangeListener) {
		onCompassValueChangeListener = listener
	}

	private val sensorManager: SensorManager

	var googleMap: GoogleMap? = null
	private var primaryKey: Long = -1
	private var currentLocationMarker: Marker? = null
	lateinit var currentPlaceMarker: Marker

	private var startLocationMarker: Marker? = null
	private var endLocationMarker: Marker? = null

	private var accessedFileList: MutableList<String> = mutableListOf()
	private var previousPathLatLngList: MutableList<LatLng> = mutableListOf()
	private var previousPathPolyline: Polyline? = null
	private var currentPathLatLngList: MutableList<LatLng> = mutableListOf()
	private var currentPathPolyline: Polyline? = null

	var placeMarkerMap: MutableMap<Long, Marker> = mutableMapOf()
	private var timelineDataList: MutableList<TimelineData> = mutableListOf()
	private var timelineMarkerMap: MutableMap<Long, Pair<Marker, Int>> = mutableMapOf()
	private var currentActivityMarker: Marker? = null

	private var pointerColor: Int = 0
	private var polylineColor = 0
	private var mapStyle: Constant.Companion.MapStyle = Constant.Companion.MapStyle.COFFEE
	private var coordinateUnit: Constant.Companion.Settings = Constant.Companion.Settings.DD

	private var minLatitude: Double = Double.MAX_VALUE
	private var maxLatitude: Double = -Double.MAX_VALUE
	private var minLongitude: Double = Double.MAX_VALUE
	private var maxLongitude: Double = -Double.MAX_VALUE

	init {
		coordinateUnit =
			Constant.Companion.Settings.values()[sharedPreferencesAppConfig
				.getInt(Constant.Companion.Settings.COORDINATE.name, Constant.Companion.Settings.DD.ordinal)]

		pointerColor =
			sharedPreferencesAppConfig.getInt(Constant.Companion.Settings.MAP_POINTER_COLOR.name, ContextCompat.getColor(context, R.color.mapPointerColor0))
		polylineColor =
			sharedPreferencesAppConfig.getInt(Constant.Companion.Settings.MAP_POLYLINE_COLOR.name, ContextCompat.getColor(context, R.color.mapPointerColor0))
		mapStyle =
			Constant.Companion.MapStyle.values()[sharedPreferencesAppConfig
				.getInt(Constant.Companion.Settings.MAP_STYLE.name, Constant.Companion.MapStyle.COFFEE.ordinal)]

		placeTable = UserDatabase.getInstance(context).placeTableDao
		initializePlaces()

		sensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
		initializeRotationalSensor()

		onCreate(null)
		onResume()
		getMapAsync { googleMap ->
			this.googleMap = googleMap
			val mapStyle = Constant.Companion.MapStyle.values()[sharedPreferencesAppConfig
				.getInt(Constant.Companion.Settings.MAP_STYLE.name, Constant.Companion.MapStyle.PAPER.ordinal)]
			setMapStyle(mapStyle)

			measure(
				MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
				MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED))
			layout(0, 0, 480, 480)

			invokeMapReady()

			placeLiveData.removeObserver(placeObserver)
			placeLiveData.observeForever(placeObserver)
		}
	}

	private fun initializePlaces() {
		placeLiveData = placeTable.getAllAsFlow().asLiveData()
		placeLiveData.observeForever(placeObserver)
	}

	override fun onDestroy() {
		super.onDestroy()

		placeLiveData.removeObserver(placeObserver)
	}

	fun onTripStatusChange(status: TripService.STATUS, primaryKey: Long) {
		Log.i(TAG, "${status.name} : $primaryKey")

		this.primaryKey = primaryKey
		when (status) {
			TripService.STATUS.NOT_STARTED -> {
			}
			TripService.STATUS.PROCESSING -> {
			}
			TripService.STATUS.CRASH_RECOVERY -> {
				try {
					var tripFile = Utility.readDataFile(primaryKey, "0")
					var tripData: TripData = Utility.readEncryptedFileAsObject(tripFile)
					var locationDataList = tripData.tripLocationDataMap.toList()
					var startLatLng: LatLng? = null
					var endLatLng: LatLng? = null
					locationDataList.forEach {
						val latLng = LatLng(it.second[0], it.second[1])
						currentPathLatLngList.add(latLng)
						calcCoordinateRange(latLng)
					}
					if (locationDataList.isNotEmpty()) {
						val latLng = LatLng(locationDataList.first().second[0], locationDataList.first().second[1])
						startLatLng = latLng
						endLatLng = LatLng(locationDataList.last().second[0], locationDataList.last().second[1])
						timelineDataList.add(
							TimelineData(
								timestamp = tripData.katoTimestamp,
								name = tripData.address?.split(",")?.get(0) ?: Utility.coordinateFilter(latLng, coordinateUnit),
								address = tripData.address,
								coordinateString = Utility.coordinateFilter(latLng, coordinateUnit),
								latLng = latLng,
								activity = tripData.activity,
								pseudoActivity = tripData.pseudoActivity,
								index = -1,
								katoTimestamp = tripData.katoTimestamp,
								panoTimestamp = tripData.panoTimestamp,
							)
						)
					}

					var pointer = tripData.next
					while (pointer != null) {
						tripFile = Utility.readDataFile(primaryKey, pointer)
						tripData = Utility.readEncryptedFileAsObject(tripFile)
						tripData.tripLocationDataMap.forEach {
							val latLng = LatLng(it.value[0], it.value[1])
							currentPathLatLngList.add(latLng)
							calcCoordinateRange(latLng)
						}
						locationDataList = tripData.tripLocationDataMap.toList()
						locationDataList.forEach {
							val latLng = LatLng(it.second[0], it.second[1])
							currentPathLatLngList.add(latLng)
							calcCoordinateRange(latLng)
						}
						if (locationDataList.isNotEmpty()) {
							val latLng = LatLng(locationDataList.first().second[0], locationDataList.first().second[1])
							endLatLng = LatLng(locationDataList.last().second[0], locationDataList.last().second[1])
							timelineDataList.add(
								TimelineData(
									timestamp = tripData.katoTimestamp,
									name = tripData.address?.split(",")?.get(0) ?: Utility.coordinateFilter(latLng, coordinateUnit),
									address = tripData.address,
									coordinateString = Utility.coordinateFilter(latLng, coordinateUnit),
									latLng = latLng,
									activity = tripData.activity,
									pseudoActivity = tripData.pseudoActivity,
									index = -1,
									katoTimestamp = tripData.katoTimestamp,
									panoTimestamp = tripData.panoTimestamp,
								)
							)
						}
						pointer = tripData.next
					}

					addTimelineMarker()

					if (startLocationMarker == null && startLatLng != null) {
						startLocationMarker = googleMap?.addMarker(MarkerOptions()
							.icon(getBitmap(R.drawable.ic_map_marker_2, Color.argb(255, 73, 148, 113))?.let { BitmapDescriptorFactory.fromBitmap(it) })
							.position(startLatLng)
							.anchor(0.5f, 1f)
							.alpha(1F)
							.title("-2")
						)
					}

					if (endLatLng != null) {
						endLocationMarker = googleMap?.addMarker(MarkerOptions()
							.icon(getBitmap(R.drawable.ic_map_marker_2, Color.argb(255, 235, 127, 116))?.let { BitmapDescriptorFactory.fromBitmap(it) })
							.position(endLatLng)
							.anchor(0.5f, 1f)
							.alpha(0f)
							.title("-2")
						)
					}
				} catch (exception: Exception) {
					exception.printStackTrace()
				}

				currentPathPolyline?.remove()
				previousPathPolyline = googleMap?.addPolyline(PolylineOptions()
					.addAll(currentPathLatLngList)
					.width(16F)
					.startCap(RoundCap())
					.endCap(RoundCap())
					.color(polylineColor)
					.jointType(JointType.ROUND)
				)
			}
			TripService.STATUS.READY -> {
				startLocationMarker?.remove()
				endLocationMarker?.remove()
				startLocationMarker = null
				endLocationMarker = null

				accessedFileList = mutableListOf()

				previousPathLatLngList = mutableListOf()
				currentPathLatLngList = mutableListOf()

				currentActivityMarker?.remove()
				timelineMarkerMap.forEach { it.value.first.remove() }
				currentActivityMarker = null
				timelineDataList = mutableListOf()
				timelineMarkerMap = mutableMapOf()

				previousPathPolyline?.remove()
				currentPathPolyline?.remove()
				previousPathPolyline = null
				currentPathPolyline = null

				minLatitude = Double.MAX_VALUE
				maxLatitude = -Double.MAX_VALUE
				minLongitude = Double.MAX_VALUE
				maxLongitude = -Double.MAX_VALUE
			}
			TripService.STATUS.RUNNING -> {
			}
			TripService.STATUS.PAUSED -> {
			}
			TripService.STATUS.STOPPED -> {
				endLocationMarker?.alpha = 1f
			}
			TripService.STATUS.SUCCEED -> {
			}
			TripService.STATUS.FAILED -> {
			}
		}
	}

	override fun onSensorChanged(event: SensorEvent?) {
		if (event != null) {
			when (event.sensor.type) {
				Sensor.TYPE_ROTATION_VECTOR -> updateDirection(event)
				else -> Log.w(TAG, "Unexpected sensor changed event of type ${event.sensor.type}")
			}
		}
	}

	override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

	fun updateCurrentLocation(location: Location) {
		try {
			currentLocationMarker?.remove()
			currentLocationMarker = googleMap?.addMarker(MarkerOptions()
				.icon(getBitmap(R.drawable.ic_current_location)?.let { BitmapDescriptorFactory.fromBitmap(it) })
				.position(LatLng(location.latitude, location.longitude))
				.anchor(0.5f, 0.5f)
				.alpha(1F)
				.title("-2")
			)!!
		} catch (exception: Exception) {
//			TODO
		}
	}

	fun onReceiveTripLocation(
		location: Location?,
		locationList: MutableMap<Long, List<Double>>?,
		activity: ActivityId.Companion.ACTIVITY,
		pseudoActivity: Int,
		fileList: List<String>,
	) {
		if (location != null) {
			currentPathLatLngList = mutableListOf()
			locationList?.forEach { (timestamp, locationData) ->
				val latLng = LatLng(locationData[0], locationData[1])
				calcCoordinateRange(latLng)
				currentPathLatLngList.add(latLng)
			}

			if (currentActivityMarker==null) {
				currentActivityMarker = googleMap?.addMarker(MarkerOptions()
					.icon(getActivityMarker(ActivityId.ACTIVITY_ID_RES_MAP[activity]!!, Constant.MAP_STYLE_DARK[mapStyle]!!).let { BitmapDescriptorFactory.fromBitmap(it) })
					.position(LatLng(location.latitude, location.longitude))
					.anchor(0.5f, 0.5f)
					.alpha(1F)
					.title("-3")
				)
			}

			fileList.forEach {
				if (!accessedFileList.contains(it)) {
					accessedFileList.add(it)
					val fileData = Utility.readDataFile(primaryKey, it)
					val tripData: TripData = Utility.readEncryptedFileAsObject(fileData)
					val locationDataList = tripData.tripLocationDataMap.toList()
					locationDataList.forEach { locationData ->
						val latLng = LatLng(locationData.second[0], locationData.second[1])
						calcCoordinateRange(latLng)
						previousPathLatLngList.add(latLng)
					}
					if (locationDataList.isNotEmpty()) {
						val latLng = LatLng(locationDataList.first().second[0], locationDataList.first().second[1])
						timelineDataList.add(
							TimelineData(
								timestamp = tripData.katoTimestamp,
								name = tripData.address?.split(",")?.get(0) ?: Utility.coordinateFilter(latLng, coordinateUnit),
								address = tripData.address,
								coordinateString = Utility.coordinateFilter(latLng, coordinateUnit),
								latLng = latLng,
								activity = tripData.activity,
								pseudoActivity = tripData.pseudoActivity,
								index = -1,
								katoTimestamp = tripData.katoTimestamp,
								panoTimestamp = tripData.panoTimestamp,
							)
						)

						if (startLocationMarker == null) {
							startLocationMarker = googleMap?.addMarker(MarkerOptions()
								.icon(getBitmap(R.drawable.ic_map_marker_2, Color.argb(255, 73, 148, 113))?.let { BitmapDescriptorFactory.fromBitmap(it) })
								.position(latLng)
								.anchor(0.5f, 1f)
								.alpha(1F)
								.title("-2")
							)
						}
					}

					if (previousPathPolyline == null) {
						previousPathPolyline = googleMap?.addPolyline(PolylineOptions()
							.addAll(previousPathLatLngList)
							.width(16F)
							.startCap(RoundCap())
							.endCap(RoundCap())
							.color(Color.argb(144, polylineColor.red, polylineColor.green, polylineColor.blue))
							.jointType(JointType.ROUND)
						)
					} else {
						previousPathPolyline!!.points = previousPathLatLngList
					}

					currentActivityMarker!!.position = LatLng(location.latitude, location.longitude)
					currentActivityMarker!!.setIcon(getActivityMarker(ActivityId.ACTIVITY_ID_RES_MAP[activity]!!,
						Constant.MAP_STYLE_DARK[mapStyle]!!).let { BitmapDescriptorFactory.fromBitmap(it) })

				}
			}
		}

		if (startLocationMarker == null && location != null) {
			startLocationMarker = googleMap?.addMarker(MarkerOptions()
				.icon(getBitmap(R.drawable.ic_map_marker_2, Color.argb(255, 73, 148, 113))?.let { BitmapDescriptorFactory.fromBitmap(it) })
				.position(LatLng(location.latitude, location.longitude))
				.anchor(0.5f, 1f)
				.alpha(1F)
				.title("-2")
			)
		}

		if (location != null) {
			endLocationMarker = googleMap?.addMarker(MarkerOptions()
				.icon(getBitmap(R.drawable.ic_map_marker_2, Color.argb(255, 235, 127, 116))?.let { BitmapDescriptorFactory.fromBitmap(it) })
				.position(LatLng(location.latitude, location.longitude))
				.anchor(0.5f, 1f)
				.alpha(0f)
				.title("-2")
			)
		}

		if (currentPathPolyline == null) {
			currentPathPolyline = googleMap?.addPolyline(PolylineOptions()
				.addAll(currentPathLatLngList)
				.width(16F)
				.startCap(RoundCap())
				.endCap(RoundCap())
				.color(Color.argb(144, polylineColor.red, polylineColor.green, polylineColor.blue))
				.jointType(JointType.ROUND)
			)
		} else {
			currentPathPolyline!!.points = currentPathLatLngList
		}

		addTimelineMarker()
	}

	private fun addTimelineMarker() {
		Log.i(TAG, "addTimelineMarker : ${timelineDataList.size}")
		timelineDataList.forEach { timelineData ->
			if (!timelineMarkerMap.containsKey(timelineData.timestamp)) {
				val imageResource = if (timelineData.activity != null) {
					ActivityId.ACTIVITY_ID_RES_MAP[ActivityId.Companion.ACTIVITY.values()[timelineData.activity!!]]!!
				} else {
					ActivityId.PSEUDO_ACTIVITY_RES_MAP[timelineData.pseudoActivity]!!
				}

				val marker = googleMap?.addMarker(MarkerOptions()
					.icon(getActivityMarker(imageResource, Constant.MAP_STYLE_DARK[mapStyle]!!).let { BitmapDescriptorFactory.fromBitmap(it) })
					.position(timelineData.latLng)
					.anchor(0.5f, 0.5f)
					.alpha(1F)
					.title("-2")
				)
				if (marker != null) {
					timelineMarkerMap[timelineData.timestamp] = Pair(marker, imageResource)
				}
			}
		}
	}

	fun refreshPlaceMarkers() {
		placeLiveData.removeObserver(placeObserver)
		placeLiveData.observeForever(placeObserver)
	}

	fun onNewCurrentPlace(place: Place) {
		if (this::currentPlaceMarker.isInitialized) {
			currentPlaceMarker.remove()
		}
		currentPlaceMarker = googleMap?.addMarker(MarkerOptions()
			.icon(getTagBitmap(R.drawable.ic_map_marker_1, place.colorTag)?.let { BitmapDescriptorFactory.fromBitmap(it) })
			.position(LatLng(place.latitude, place.longitude))
			.anchor(0.5f, 1f)
			.alpha(1F)
		)!!
	}

	fun moveCamera(location: Location, animate: Boolean = false): Boolean {
		return if (googleMap != null) {
			if (animate) {
				googleMap!!.animateCamera(
					CameraUpdateFactory.newCameraPosition(
						CameraPosition(
							LatLng(location.latitude, location.longitude),
							17f,
							0f,
							0f
						)
					)
				)
			} else {
				googleMap!!.moveCamera(
					CameraUpdateFactory.newCameraPosition(
						CameraPosition(
							LatLng(location.latitude, location.longitude),
							17f,
							0f,
							0f
						)
					)
				)
			}
			true
		} else {
			false
		}
	}

	fun zoomOnCurrentTrip() {
		googleMap?.animateCamera(CameraUpdateFactory.newLatLngBounds(LatLngBounds.Builder()
			.include(LatLng(minLatitude, minLongitude))
			.include(LatLng(maxLatitude, maxLongitude))
			.build(), 128)
		)
	}

	fun setMapStyle(mapStyle: Constant.Companion.MapStyle) {
		sharedPreferencesAppConfig.edit().putInt(Constant.Companion.Settings.MAP_STYLE.name, mapStyle.ordinal).commit()
		this.mapStyle = mapStyle
		try {
			googleMap?.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, Constant.MAP_STYLE_RES_MAP[mapStyle]!!))
		} catch (exception: Exception) {
		}

		val isDark = Constant.MAP_STYLE_DARK[mapStyle]!!
		if (isDark) {
			timelineMarkerMap.forEach {
				it.value.first.setIcon(getActivityMarker(it.value.second, isDark).let { it1 -> BitmapDescriptorFactory.fromBitmap(it1) })
			}
		}
	}

	fun setMapPointerColor(pointerColor: Int) {
		sharedPreferencesAppConfig.edit().putInt(Constant.Companion.Settings.MAP_POINTER_COLOR.name, pointerColor).commit()

		this.pointerColor = pointerColor
		currentLocationMarker?.setIcon(getBitmap(R.drawable.ic_current_location)?.let { BitmapDescriptorFactory.fromBitmap(it) })
	}

	fun setMapPolylineColor(polylineColor: Int) {
		sharedPreferencesAppConfig.edit().putInt(Constant.Companion.Settings.MAP_POLYLINE_COLOR.name, polylineColor).commit()

		this.polylineColor = polylineColor
		previousPathPolyline?.color = Color.argb(144, polylineColor.red, polylineColor.green, polylineColor.blue)
		currentPathPolyline?.color = Color.argb(144, polylineColor.red, polylineColor.green, polylineColor.blue)
	}

	fun invokeMapReady() {
		if (googleMap != null) {
			onMapReadyListener?.onMapReady(true)
		} else {
			onMapReadyListener?.onMapReady(false)
		}
	}

	private fun initializeRotationalSensor() {
		val rotationVectorSensor: Sensor? = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR)
		if (rotationVectorSensor == null) {
			Log.w(TAG, "Rotation vector sensor not available")
			return
		}

		sensorManager.registerListener(this, rotationVectorSensor, 10000000)
	}

	private fun updateDirection(event: SensorEvent) {
		val rotationVector = RotationVector(event.values[0], event.values[1], event.values[2])
		val azimuth = calculateAzimuth(rotationVector)
		if (googleMap != null) {
			currentLocationMarker?.rotation = azimuth + 315 - googleMap!!.cameraPosition.bearing
			onCompassValueChangeListener?.onCompassValueChange(-azimuth)
		}
	}

	private fun getBitmap(drawableRes: Int, tint: Int? = null): Bitmap? {
		val drawable = ResourcesCompat.getDrawable(resources, drawableRes, null)!!
		if (tint == null) {
			drawable.setTint(pointerColor)
		} else {
			drawable.setTint(tint)
		}
		val canvas = Canvas()
		val bitmap = Bitmap.createBitmap(28.d, 28.d, Bitmap.Config.ARGB_8888)
		canvas.setBitmap(bitmap)
		drawable.setBounds(0, 0, 28.d, 28.d)
		drawable.draw(canvas)
		return bitmap
	}

	private fun getActivityMarker(resourceInt: Int, isDark: Boolean): Bitmap {
		val backgroundDrawable = AppCompatResources.getDrawable(context, R.drawable.ic_circle)!!
		val activityDrawable = AppCompatResources.getDrawable(context, resourceInt)!!

		if (isDark) {
			backgroundDrawable.setTint(Color.WHITE)
			activityDrawable.setTint(Color.BLACK)
		} else {
			backgroundDrawable.setTint(Color.BLACK)
			activityDrawable.setTint(Color.WHITE)
		}

		val bitmap: Bitmap = Bitmap.createBitmap(32.d, 32.d, Bitmap.Config.ARGB_8888)
		val canvas = Canvas(bitmap)

		backgroundDrawable.setBounds(0, 0, canvas.width, canvas.height)
		backgroundDrawable.draw(canvas)

		activityDrawable.setBounds(6.d, 6.d, canvas.width - 6.d, canvas.height - 6.d)
		activityDrawable.draw(canvas)

		return bitmap
	}

	private fun getTagBitmap(drawableRes: Int, colorTag: Int = -1): Bitmap? {
		val iconTint = when (colorTag) {
			0 -> context.resources.getColor(R.color.placeColor0, null)
			1 -> context.resources.getColor(R.color.placeColor1, null)
			2 -> context.resources.getColor(R.color.placeColor2, null)
			3 -> context.resources.getColor(R.color.placeColor3, null)
			4 -> context.resources.getColor(R.color.placeColor4, null)
			5 -> context.resources.getColor(R.color.placeColor5, null)
			6 -> context.resources.getColor(R.color.placeColor6, null)
			else -> localAppTheme.colorPrimary
		}
		val drawable = ResourcesCompat.getDrawable(resources, drawableRes, null)!!
		drawable.setTint(iconTint)
		val canvas = Canvas()
		val bitmap = Bitmap.createBitmap(28.d, 28.d, Bitmap.Config.ARGB_8888)
		canvas.setBitmap(bitmap)
		drawable.setBounds(0, 0, 28.d, 28.d)
		drawable.draw(canvas)
		return bitmap
	}

	private fun calcCoordinateRange(latLng: LatLng) {
		minLatitude = minOf(minLatitude, latLng.latitude)
		maxLatitude = maxOf(maxLatitude, latLng.latitude)
		minLongitude = minOf(minLongitude, latLng.longitude)
		maxLongitude = maxOf(maxLongitude, latLng.longitude)
	}

	data class RotationVector(val x: Float, val y: Float, val z: Float) {
		fun toArray(): FloatArray = floatArrayOf(x, y, z)
	}

	data class TimelineData(
		val timestamp: Long,
		val name: String?,
		val address: String?,
		val coordinateString: String,
		val latLng: LatLng,
		var activity: Int?,
		var pseudoActivity: Int?,
		var index: Int,
		val katoTimestamp: Long,
		val panoTimestamp: Long,
	)

	val Int.d
		get() = (this * density).toInt()

	companion object {
		const val TAG = "HOME_MAP_VIEW"

		fun calculateAzimuth(rotationVector: RotationVector): Float {
			val AZIMUTH = 0
			val AXIS_SIZE = 3
			val ROTATION_MATRIX_SIZE = 9

			val rotationMatrix = FloatArray(ROTATION_MATRIX_SIZE)
			SensorManager.getRotationMatrixFromVector(rotationMatrix, rotationVector.toArray())
			val orientationAnglesInRadians = SensorManager.getOrientation(rotationMatrix, FloatArray(AXIS_SIZE))
			val radians = orientationAnglesInRadians[AZIMUTH]
			return Math.toDegrees(radians.toDouble()).toFloat()
		}
	}
}
