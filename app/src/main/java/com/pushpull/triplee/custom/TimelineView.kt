package com.pushpull.triplee.custom

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.*
import android.text.TextUtils
import android.text.format.DateFormat
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.core.content.res.ResourcesCompat
import com.google.android.flexbox.AlignItems
import com.pushpull.triplee.R
import com.pushpull.triplee.dataClass.AppTheme
import com.pushpull.triplee.konstant.ActivityId
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayout
import com.google.android.flexbox.JustifyContent
import com.google.android.gms.maps.model.LatLng
import com.pushpull.triplee.tripDetail.TripDetailViewModel
import java.util.*


class TimelineView(context: Context) : LinearLayout(context) {


	private lateinit var localAppTheme: AppTheme

	private var density: Float = context.resources.displayMetrics.density
	private var heightDp: Int = context.resources.configuration.screenHeightDp

	interface OnTimelineViewClick {
		fun onTimelineViewClick(timelineData: TimelineData)
	}

	private var onTimelineViewClickListener: OnTimelineViewClick? = null
	fun setOnTimelineViewClickListener(listener: OnTimelineViewClick) {
		onTimelineViewClickListener = listener
	}

	init {
		localAppTheme = getLocalTheme()

		layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT).apply {
			setMargins(0, 8.d, 0, 0)
		}
		orientation = VERTICAL
	}

	fun updateView(timelineDataList: MutableList<TimelineData>) {
		removeAllViews()
		for (index in 0 until timelineDataList.size) {
			addView(getSingleLayout(timelineDataList[index], index==timelineDataList.size-1))
		}
	}

	private fun getSingleLayout(timelineData: TimelineData, isLast: Boolean = false): View {

		val topLayout = CardView(context).apply {
			layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT).apply {
				setMargins(16.d, 0, 16.d, 0)
			}
			radius = 24.d.toFloat()
			cardElevation = 0F
			isClickable = true
			isFocusable = true

			var isExpanded = false
			val expandedView = LinearLayout(context).apply {
				layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT).apply {
					setPadding(8.d, 0, 8.d, 4.d)
				}
				orientation = VERTICAL

				setOnClickListener {  }

				addView(
					TextView(context).apply {
						layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT).apply {
							setPadding(0, 2.d, 0, 2.d)
						}

						var address = ""
						timelineData.address?.split(", ")?.forEach {
							address += "$it\n"
						}

						if(timelineData.address.isNullOrBlank()) {
							address = "Fetching Address..."						}

						text = address
						setTextColor(localAppTheme.colorOnGamma)
						setTextSize(TypedValue.COMPLEX_UNIT_SP, 12F)
						setTypeface(ResourcesCompat.getFont(context, localAppTheme.textFontFamily), Typeface.BOLD)
						includeFontPadding = false
					}
				)

				addView(
					FlexboxLayout(context).apply {
						layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT).apply {
							setPadding(0, 2.d, 0, 2.d)
						}
						flexDirection = FlexDirection.ROW
						justifyContent = JustifyContent.SPACE_BETWEEN

						addView(
							TextView(context).apply {
								layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT).apply {
									setPadding(0, 0, 8.d, 0)
								}

								text = "${timelineData.coordinateString}"
								setTextColor(localAppTheme.colorOnGamma)
								setTextSize(TypedValue.COMPLEX_UNIT_SP, 12F)
								setTypeface(ResourcesCompat.getFont(context, R.font.baloo2_regular), Typeface.BOLD)
								includeFontPadding = false
							}
						)

						addView(
							TextView(context).apply {
								layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT).apply {
									setPadding(8.d, 0, 0, 0)
								}

								val calendar: Calendar = Calendar.getInstance(Locale.ENGLISH)
								calendar.timeInMillis = timelineData.timestamp

								text = DateFormat.format("HH:mm a, EEE dd MMM", calendar)
								setTextColor(localAppTheme.colorOnGamma)
								setTextSize(TypedValue.COMPLEX_UNIT_SP, 12F)
								setTypeface(ResourcesCompat.getFont(context, R.font.baloo2_regular), Typeface.BOLD)
								includeFontPadding = false
							}
						)
					}
				)

			}
			setOnClickListener {
				isExpanded = !isExpanded
				expandedView.visibility = if (isExpanded) {
					View.VISIBLE
				} else {
					View.GONE
				}
			}
			setOnLongClickListener {
				onTimelineViewClickListener?.onTimelineViewClick(timelineData)
				true
			}

			addView(
				LinearLayout(context).apply {
					layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT).apply {
						setMargins(16.d, 8.d, 16.d, 8.d)
					}
					orientation = VERTICAL

					addView(
						FlexboxLayout(context).apply {
							layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
							flexDirection = FlexDirection.ROW
							justifyContent = JustifyContent.SPACE_BETWEEN
							alignItems = AlignItems.CENTER

							addView(
								TextView(context).apply {
									layoutParams = LayoutParams(32.d, 32.d).apply {
										setMargins(0, 0, 3.d, 0)
									}
									text = "${timelineData.index + 1}"
									setTextColor(localAppTheme.colorGamma)
									setTextSize(TypedValue.COMPLEX_UNIT_SP, 16F)
									setTypeface(ResourcesCompat.getFont(context, R.font.baloo2_bold), Typeface.BOLD)
									includeFontPadding = false
									gravity = Gravity.CENTER
									background = ResourcesCompat.getDrawable(resources, R.drawable.ic_circle, context.theme)
									backgroundTintList = ColorStateList.valueOf(localAppTheme.colorOnGamma)
								}
							)

							addView(
								TextView(context).apply {
									layoutParams = FlexboxLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT).apply {
										setMargins(3.d, 0, 0, 0)
										flexGrow = 1F
									}
									text = timelineData.name
									setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)
									setTypeface(ResourcesCompat.getFont(context, R.font.baloo2_bold), Typeface.BOLD)
									setTextColor(localAppTheme.colorOnGamma)
									ellipsize = TextUtils.TruncateAt.END
									maxLines = 1
								}
							)

							addView(
								ImageView(context).apply {
									layoutParams = LayoutParams(32.d, 32.d).apply {
										setPadding(8.d, 8.d, 8.d, 8.d)
										setMargins(4.d, 4.d, 4.d, 4.d)
									}

									if(isLast) {
										setImageResource(ActivityId.PSEUDO_ACTIVITY_RES_MAP[-2]!!)
									} else {
										if (timelineData.activity != null) {
											setImageResource(ActivityId.ACTIVITY_ID_RES_MAP[ActivityId.Companion.ACTIVITY.values()[timelineData.activity!!]]!!)
										} else {
											setImageResource(ActivityId.PSEUDO_ACTIVITY_RES_MAP[timelineData.pseudoActivity]!!)
										}
									}

									setColorFilter(localAppTheme.colorPrimary)
									setBackgroundResource(R.drawable.ic_circle)
									backgroundTintList = ColorStateList.valueOf(localAppTheme.colorBeta)

									scaleType = ImageView.ScaleType.FIT_CENTER
								}
							)

							addView(
								ImageView(context).apply {
									layoutParams = LayoutParams(28.d, 28.d).apply {
										setPadding(0, 0, 0, 0)
										setMargins(4.d, 4.d, 4.d, 4.d)
									}
									setImageResource(R.drawable.ic_arrow_down_s)
									scaleType = ImageView.ScaleType.FIT_CENTER
									setColorFilter(localAppTheme.colorOnGamma)
									setBackgroundColor(0)
								}
							)
						}
					)

					expandedView.visibility = View.GONE
					addView(expandedView)
				}
			)
		}

		val connectionLayout = View(context).apply {
			layoutParams = LayoutParams(2.d, 8.d).apply {
				setMargins(64.d, 0, 0, 0)
			}
			setBackgroundColor(localAppTheme.colorOnGamma)
		}

		val bottomLayout = CardView(context).apply {
			layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT).apply {
				setMargins(16.d, 0, 16.d, 0)
			}

			radius = 24.d.toFloat()
			cardElevation = 0F

			addView(
				LinearLayout(context).apply {
					layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
					orientation = HORIZONTAL
					gravity = Gravity.CENTER_VERTICAL

					addView(
						ImageView(context).apply {
							layoutParams = LayoutParams(32.d, 32.d).apply {
								setMargins(16.d, 8.d, 3.d, 8.d)
								setPadding(8.d, 8.d, 8.d, 8.d)
							}

							if(isLast){
								setImageResource(ActivityId.PSEUDO_ACTIVITY_RES_MAP[-2]!!)
							} else {
								setImageResource(ActivityId.PSEUDO_ACTIVITY_RES_MAP[timelineData.pseudoActivity] ?: ActivityId.PSEUDO_ACTIVITY_RES_MAP[-1]!!)
							}
							setColorFilter(localAppTheme.colorPrimary)
							setBackgroundResource(R.drawable.ic_circle)
							backgroundTintList = ColorStateList.valueOf(localAppTheme.colorBeta)

							scaleType = ImageView.ScaleType.FIT_CENTER
						}
					)

					addView(
						TextView(context).apply {
							layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT).apply {
								setMargins(3.d, 0, 0, 0)
							}
							text = if(isLast) {
								"End"
							} else {
								ActivityId.PSEUDO_ACTIVITY_NAME_MAP[timelineData.pseudoActivity] ?: ActivityId.PSEUDO_ACTIVITY_NAME_MAP[-1]!!
							}
							setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)
							setTypeface(ResourcesCompat.getFont(context, R.font.baloo2_bold), Typeface.BOLD)
							setTextColor(localAppTheme.colorPrimary)
							ellipsize = TextUtils.TruncateAt.END
							maxLines = 1
						}
					)

				}
			)
		}

		val endConnectionLayout = if(isLast) {
			View(context).apply {
				layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, (heightDp.d*0.5).toInt())
				setBackgroundColor(0)
			}
		} else {
			View(context).apply {
				layoutParams = LayoutParams(2.d, 8.d).apply {
					setMargins(64.d, 0, 0, 0)
				}
				setBackgroundColor(localAppTheme.colorOnSurface)
			}
		}

		return LinearLayout(context).apply {
			layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
			orientation = VERTICAL

			addView(topLayout)
			addView(connectionLayout)
			addView(bottomLayout)
			addView(endConnectionLayout)
		}
	}

	val Int.d
		get() = (this * density).toInt()

	data class TimelineData(
		val timestamp: Long,
		val name: String?,
		val address: String?,
		val coordinateString: String,
		val latLng: LatLng,
		var activity: Int?,
		var pseudoActivity: Int?,
		var index: Int,
		val katoTimestamp: Long,
		val panoTimestamp: Long,
		val filePointer: String?,
		var mediaStoreDataList: List<TripDetailViewModel.MediaStoreData>
	)

	companion object {
		const val TAG = "TIMELINE_VIEW"
	}
}
