package com.pushpull.triplee.custom

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayout
import com.google.android.flexbox.JustifyContent
import com.pushpull.triplee.R
import com.pushpull.triplee.dataClass.AppTheme
import com.pushpull.triplee.utility.Utility


@SuppressLint("ClickableViewAccessibility")
class LiveInfo : LinearLayout {
	constructor(context: Context) : super(context)
	constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet)

	private lateinit var localAppTheme: AppTheme
	private var rippleDrawable: Int = 0

	private var density: Float = context.resources.displayMetrics.density
	private var aspectRatio: Float = context.resources.displayMetrics.heightPixels.toFloat() / context.resources.displayMetrics.widthPixels.toFloat()
	private var widthDp: Int = context.resources.configuration.screenWidthDp
	private var heightDp: Int = context.resources.configuration.screenHeightDp

	private val viewWidth: Int = (context.resources.configuration.screenWidthDp * 3 / 4).d
	private val viewHeight: Int = (context.resources.configuration.screenHeightDp * 3 / 4).d

	private var collapseLayout: LinearLayout

	init {
		localAppTheme = getLocalTheme()

		layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
		orientation = VERTICAL
		clipChildren = false
		clipToPadding = false

		collapseLayout = LinearLayout(context).apply {
			layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
			orientation = VERTICAL
			translationZ = 1F
			setBackgroundColor(localAppTheme.colorAlpha)
			addView(
				FlexboxLayout(context).apply {
					layoutParams = FlexboxLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT).apply {
						setPadding(0, 4.d, 0, 4.d)
					}

					justifyContent = JustifyContent.SPACE_EVENLY
					flexDirection = FlexDirection.ROW
					setBackgroundColor(0)
					addView(captionTextView("0 m", R.drawable.ic_ascent, 1F, true))
					addView(captionTextView("0 Km/hr", R.drawable.ic_speedometer, 1F, true))
				}
			)
			addView(
				FlexboxLayout(context).apply {
					layoutParams = FlexboxLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT).apply {
						setPadding(0, 4.d, 0, 4.d)
					}

					justifyContent = JustifyContent.SPACE_EVENLY
					flexDirection = FlexDirection.ROW
					setBackgroundColor(0)
					addView(captionTextView("0 sec", R.drawable.ic_duration, 1F, true))
					addView(captionTextView("0 m", R.drawable.ic_distance, 1F, true))
				}
			)
		}

		addView(collapseLayout)
	}

	private fun captionTextView(text: String, imageResource: Int?, flexGrow: Float, isLightBackground: Boolean): LinearLayout {
		return LinearLayout(context).apply {
			layoutParams = FlexboxLayout.LayoutParams(flexGrow.toInt(), LayoutParams.WRAP_CONTENT).apply {
				if (flexGrow > 0) {
					this.flexGrow = flexGrow
				}
				gravity = Gravity.CENTER
			}
			setBackgroundColor(0)
			orientation = HORIZONTAL
			if (imageResource != null) {
				addView(
					ImageView(context).apply {
						layoutParams = LayoutParams(16.d, 16.d).apply {
							gravity = Gravity.CENTER
							setMargins(0, 0, 12.d, 0)
						}
						setImageResource(imageResource)
						setBackgroundColor(0)
						setColorFilter(localAppTheme.colorOnAlpha)
					}
				)
			}
			addView(
				TextView(context).apply {
					layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
					this.text = text
					setTextColor(localAppTheme.colorOnAlpha)
					setBackgroundColor(0)
					setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)
					setTypeface(ResourcesCompat.getFont(context, localAppTheme.textFontFamily), Typeface.BOLD)
				}
			)
		}
	}

	fun updateLocationData(liveInfoLocationData: LiveInfoLocationData) {
		val altitude = if (liveInfoLocationData.altitude == null) {
			"~"
		} else {
			"${liveInfoLocationData.altitude.round(1)} m"
		}
		val speed = if (liveInfoLocationData.speed == null) {
			"~"
		} else {
			"${liveInfoLocationData.speed.round(1)} Km/hr"
		}

		(((collapseLayout.getChildAt(0) as FlexboxLayout).getChildAt(0) as LinearLayout).getChildAt(1) as TextView).text = altitude
		(((collapseLayout.getChildAt(0) as FlexboxLayout).getChildAt(1) as LinearLayout).getChildAt(1) as TextView).text = speed
	}

	fun updateTripData(liveInfoTripData: LiveInfoTripData, unit: com.pushpull.triplee.konstant.Constant.Companion.Settings) {
		val duration = if (liveInfoTripData.startTimestamp == null) {
			"~"
		} else {
			val elapsedTime = (System.currentTimeMillis() - liveInfoTripData.startTimestamp)/1000
			val seconds = elapsedTime%60
			val minutes = (elapsedTime/60)%60
			val hours = elapsedTime/(60*60)
			if (hours == 0L) {
				"$minutes min $seconds sec"
			} else {
				"$hours hr $minutes min"
			}
		}
		val distance = if (liveInfoTripData.distance == null) {
			"~"
		} else {
			Utility.distanceFilter(liveInfoTripData.distance, unit)
		}

		(((collapseLayout.getChildAt(1) as FlexboxLayout).getChildAt(0) as LinearLayout).getChildAt(1) as TextView).text = duration
		(((collapseLayout.getChildAt(1) as FlexboxLayout).getChildAt(1) as LinearLayout).getChildAt(1) as TextView).text = distance
	}


	data class LiveInfoLocationData(
		val altitude: Double?,
		val speed: Float?,
	)

	data class LiveInfoTripData(
		val startTimestamp: Long?,
		val distance: Int?,
	)

	val Int.d
		get() = (this * density).toInt()

	companion object {
		const val TAG = "LIVE_TRIP_INFO"
	}
}
