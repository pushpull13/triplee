package com.pushpull.triplee.custom

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.Typeface
import android.text.TextUtils
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.widget.NestedScrollView
import com.pushpull.triplee.dataClass.AppTheme
import com.google.android.flexbox.AlignItems
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayout
import com.google.android.flexbox.JustifyContent
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.card.MaterialCardView
import com.pushpull.triplee.R
import com.pushpull.triplee.konstant.Constant
import com.pushpull.triplee.konstant.Constant.Companion.Settings

class MapStyleBottomSheet(context: Context, private val homeMapView: HomeMapView) : BottomSheetDialog(context) {

	private lateinit var localAppTheme: AppTheme

	private var density: Float = context.resources.displayMetrics.density
	private var aspectRatio: Float = context.resources.displayMetrics.heightPixels.toFloat() / context.resources.displayMetrics.widthPixels.toFloat()
	private var widthDp: Int = context.resources.configuration.screenWidthDp
	private var heightDp: Int = context.resources.configuration.screenHeightDp

	private var sharedPreferencesAppConfig: SharedPreferences
	private var mapStyle: Constant.Companion.MapStyle = Constant.Companion.MapStyle.COFFEE
	private var colorList: List<Int> = listOf()
	private var mapPointerStyle: Int = 0
	private var mapPolylineStyle: Int = 0

	private var innerLayout: LinearLayout

	init {
		localAppTheme = getLocalTheme(context)

		window?.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
		sharedPreferencesAppConfig = context.getSharedPreferences(Settings.APP_CONFIG.name, AppCompatActivity.MODE_PRIVATE)

		behavior.peekHeight = heightDp.d / 3

		mapPointerStyle = localAppTheme.colorPrimary
		mapPolylineStyle = localAppTheme.colorPrimary

		colorList = listOf(
			ContextCompat.getColor(context, R.color.mapPointerColor0),
			ContextCompat.getColor(context, R.color.mapPointerColor1),
			ContextCompat.getColor(context, R.color.mapPointerColor2),
			ContextCompat.getColor(context, R.color.mapPointerColor3),
			ContextCompat.getColor(context, R.color.mapPointerColor4),
			ContextCompat.getColor(context, R.color.mapPointerColor5),
			ContextCompat.getColor(context, R.color.mapPointerColor6),
			ContextCompat.getColor(context, R.color.mapPointerColor7)
		)

		innerLayout = LinearLayout(context).apply {
			layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT).apply {
				setPadding(8.d, 16.d, 8.d, 8.d)
			}
			orientation = LinearLayout.VERTICAL
			setBackgroundColor(localAppTheme.colorBeta)
		}

		setContentView(
			NestedScrollView(context).apply {
				addView(innerLayout)
			}
		)

		updateView()
	}

	private fun updateView() {
		innerLayout.removeAllViews()

		innerLayout.addView(
			TextView(context).apply {
				layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
					setPadding(12.d, 0, 0, 8.d)
				}
				text = "Choose your style..."
				setTextSize(TypedValue.COMPLEX_UNIT_SP, 20F)
				setTypeface(ResourcesCompat.getFont(context, localAppTheme.textFontFamily), Typeface.BOLD)
				setTextColor(localAppTheme.colorOnBeta)
			}
		)

		mapStyle = Constant.Companion.MapStyle.values()[sharedPreferencesAppConfig.getInt(Settings.MAP_STYLE.name, Constant.Companion.MapStyle.COFFEE.ordinal)]

		innerLayout.addView(
			TextView(context).apply {
				layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
					setPadding(12.d, 0, 0, 4.d)
				}
				text = "Map pointer color"
				setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
				setTypeface(ResourcesCompat.getFont(context, localAppTheme.textFontFamily), Typeface.BOLD)
				setTextColor(localAppTheme.colorOnBeta)
			}
		)

		innerLayout.addView(
			HorizontalScrollView(context).apply {
				layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
					setPadding(8.d, 0, 8.d, 0)
				}
				isHorizontalScrollBarEnabled = false

				addView(
					LinearLayout(context).apply {
						layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
						orientation = LinearLayout.HORIZONTAL

						colorList.forEach { color ->
							addView(
								CardView(context).apply {
									layoutParams = LinearLayout.LayoutParams(40.d, 40.d).apply {
										setMargins(6.d, 4.d, 6.d, 16.d)
									}
									radius = 8.d.toFloat()
									cardElevation = 4.d.toFloat()
									isClickable = true
									isFocusable = true

									foreground = AppCompatResources.getDrawable(context, localAppTheme.roundedCornerRipple)
									setCardBackgroundColor(color)

									setOnClickListener { homeMapView.setMapPointerColor(color) }
								}
							)
						}
					}
				)
			}
		)

		innerLayout.addView(
			TextView(context).apply {
				layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
					setPadding(12.d, 0, 0, 4.d)
				}
				text = "Map polyline color"
				setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
				setTypeface(ResourcesCompat.getFont(context, localAppTheme.textFontFamily), Typeface.BOLD)
				setTextColor(localAppTheme.colorOnBeta)
			}
		)

		innerLayout.addView(
			HorizontalScrollView(context).apply {
				layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
					setPadding(8.d, 0, 8.d, 0)
				}
				isHorizontalScrollBarEnabled = false

				addView(
					LinearLayout(context).apply {
						layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
						orientation = LinearLayout.HORIZONTAL

						colorList.forEach { color ->
							addView(
								CardView(context).apply {
									layoutParams = LinearLayout.LayoutParams(40.d, 40.d).apply {
										setMargins(6.d, 4.d, 6.d, 16.d)
									}
									radius = 8.d.toFloat()
									cardElevation = 4.d.toFloat()
									isClickable = true
									isFocusable = true

									foreground = AppCompatResources.getDrawable(context, localAppTheme.roundedCornerRipple)
									setCardBackgroundColor(color)

									setOnClickListener { homeMapView.setMapPolylineColor(color) }
								}
							)
						}
					}
				)
			}
		)

		innerLayout.addView(
			FlexboxLayout(context).apply {
				layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
				flexDirection = FlexDirection.ROW
				justifyContent = JustifyContent.CENTER
				alignItems = AlignItems.CENTER

				addView(getFlexItem(Constant.Companion.MapStyle.PAPER, false))
				addView(getFlexItem(Constant.Companion.MapStyle.RETRO, false))
				addView(getFlexItem(Constant.Companion.MapStyle.BLACK_WHITE, false))
			}
		)
		innerLayout.addView(
			FlexboxLayout(context).apply {
				layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
				flexDirection = FlexDirection.ROW
				justifyContent = JustifyContent.CENTER

				addView(getFlexItem(Constant.Companion.MapStyle.GREENER))
				addView(getFlexItem(Constant.Companion.MapStyle.COFFEE))
				addView(getFlexItem(Constant.Companion.MapStyle.WATER))
			}
		)
		innerLayout.addView(
			FlexboxLayout(context).apply {
				layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
				flexDirection = FlexDirection.ROW
				justifyContent = JustifyContent.CENTER

				addView(getFlexItem(Constant.Companion.MapStyle.MIDNIGHT))
				addView(getFlexItem(Constant.Companion.MapStyle.OREGANO))
				addView(getFlexItem(Constant.Companion.MapStyle.GOLD))
			}
		)
		innerLayout.addView(
			FlexboxLayout(context).apply {
				layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
				flexDirection = FlexDirection.ROW
				justifyContent = JustifyContent.FLEX_START

				addView(getFlexItem(Constant.Companion.MapStyle.NIGHT_LIGHT))
				addView(getFlexItem(Constant.Companion.MapStyle.NIGHT_LIGHT, true))
				addView(getFlexItem(Constant.Companion.MapStyle.NIGHT_LIGHT, true))
			}
		)
	}

	private fun getFlexItem(mapStyle: Constant.Companion.MapStyle, isBlank: Boolean = false): FlexboxLayout {
		return FlexboxLayout(context).apply {
			layoutParams = FlexboxLayout.LayoutParams(0, 80.d).apply {
				setMargins(16.d, 2.d, 16.d, 4.d)
				flexGrow = 1F
			}
			flexDirection = FlexDirection.COLUMN
			alignItems = AlignItems.CENTER
			foreground = AppCompatResources.getDrawable(context, localAppTheme.roundedCornerRipple)

			setOnClickListener {
				homeMapView.setMapStyle(mapStyle)
				updateView()
			}

			if (!isBlank) {
				addView(
					MaterialCardView(context).apply {
						layoutParams = FlexboxLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0).apply {
							flexGrow = 1F
						}
						cardElevation = 0F
						radius = 8.d.toFloat()
						setCardBackgroundColor(Color.TRANSPARENT)

						addView(
							ImageView(context).apply {
								layoutParams = FlexboxLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
								if (mapStyle == this@MapStyleBottomSheet.mapStyle) {
									strokeColor = localAppTheme.colorPrimary
									strokeWidth = 3.d
								}
								setImageResource(Constant.MAP_STYLE_IMG_MAP[mapStyle]!!)
								scaleType = ImageView.ScaleType.CENTER_CROP
							}
						)
					}
				)
				addView(
					TextView(context).apply {
						layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
						text = context.getString(Constant.MAP_STYLE_NAME_MAP[mapStyle]!!)
						ellipsize = TextUtils.TruncateAt.END
						maxLines = 1
						setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)
						setTypeface(ResourcesCompat.getFont(context, localAppTheme.textFontFamily), Typeface.NORMAL)
						setTextColor(localAppTheme.colorOnBackground)
					}
				)
			}
		}
	}

	val Int.d
		get() = (this * density).toInt()

	companion object {
		const val TAG = "MAP_STYLE_BOTTOM_SHEET"
	}
}
