package com.pushpull.triplee.custom

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.RectF
import android.util.TypedValue
import android.view.View
import com.pushpull.triplee.R
import com.pushpull.triplee.dataClass.AppTheme

fun View.getLocalTheme(): AppTheme {
	val typedValue = TypedValue()
	val localTheme = AppTheme()

	context.theme.resolveAttribute(R.attr.colorPrimary, typedValue, true).apply { localTheme.colorPrimary = typedValue.data }
	context.theme.resolveAttribute(R.attr.colorOnPrimary, typedValue, true).apply { localTheme.colorOnPrimary = typedValue.data }

	context.theme.resolveAttribute(R.attr.colorAlpha, typedValue, true).apply { localTheme.colorAlpha = typedValue.data }
	context.theme.resolveAttribute(R.attr.colorOnAlpha, typedValue, true).apply { localTheme.colorOnAlpha = typedValue.data }

	context.theme.resolveAttribute(R.attr.colorBeta, typedValue, true).apply { localTheme.colorBeta = typedValue.data }
	context.theme.resolveAttribute(R.attr.colorOnBeta, typedValue, true).apply { localTheme.colorOnBeta = typedValue.data }

	context.theme.resolveAttribute(R.attr.colorGamma, typedValue, true).apply { localTheme.colorGamma = typedValue.data }
	context.theme.resolveAttribute(R.attr.colorOnGamma, typedValue, true).apply { localTheme.colorOnGamma = typedValue.data }

	context.theme.resolveAttribute(R.attr.colorBackground, typedValue, true).apply { localTheme.colorBackground = typedValue.data }
	context.theme.resolveAttribute(R.attr.colorOnBackground, typedValue, true).apply { localTheme.colorOnBackground = typedValue.data }

	context.theme.resolveAttribute(R.attr.colorSurface, typedValue, true).apply { localTheme.colorSurface = typedValue.data }
	context.theme.resolveAttribute(R.attr.colorOnSurface, typedValue, true).apply { localTheme.colorOnSurface = typedValue.data }

	context.theme.resolveAttribute(R.attr.textFontFamily, typedValue, true).apply { localTheme.textFontFamily = typedValue.resourceId }

	localTheme.roundedCornerRipple = R.drawable.rounded_corner_ripple
	return localTheme
}

fun getLocalTheme(context: Context): AppTheme {
	val typedValue = TypedValue()
	val localTheme = AppTheme()

	context.theme.resolveAttribute(R.attr.colorPrimary, typedValue, true).apply { localTheme.colorPrimary = typedValue.data }
	context.theme.resolveAttribute(R.attr.colorOnPrimary, typedValue, true).apply { localTheme.colorOnPrimary = typedValue.data }

	context.theme.resolveAttribute(R.attr.colorAlpha, typedValue, true).apply { localTheme.colorAlpha = typedValue.data }
	context.theme.resolveAttribute(R.attr.colorOnAlpha, typedValue, true).apply { localTheme.colorOnAlpha = typedValue.data }

	context.theme.resolveAttribute(R.attr.colorBeta, typedValue, true).apply { localTheme.colorBeta = typedValue.data }
	context.theme.resolveAttribute(R.attr.colorOnBeta, typedValue, true).apply { localTheme.colorOnBeta = typedValue.data }

	context.theme.resolveAttribute(R.attr.colorGamma, typedValue, true).apply { localTheme.colorGamma = typedValue.data }
	context.theme.resolveAttribute(R.attr.colorOnGamma, typedValue, true).apply { localTheme.colorOnGamma = typedValue.data }

	context.theme.resolveAttribute(R.attr.colorBackground, typedValue, true).apply { localTheme.colorBackground = typedValue.data }
	context.theme.resolveAttribute(R.attr.colorOnBackground, typedValue, true).apply { localTheme.colorOnBackground = typedValue.data }

	context.theme.resolveAttribute(R.attr.colorSurface, typedValue, true).apply { localTheme.colorSurface = typedValue.data }
	context.theme.resolveAttribute(R.attr.colorOnSurface, typedValue, true).apply { localTheme.colorOnSurface = typedValue.data }

	context.theme.resolveAttribute(R.attr.textFontFamily, typedValue, true).apply { localTheme.textFontFamily = typedValue.resourceId }

	localTheme.roundedCornerRipple = R.drawable.rounded_corner_ripple
	return localTheme
}

fun Bitmap.toRoundedCorners(cornerRadius: Float = 25F): Bitmap?{
	val bitmap = Bitmap.createBitmap(
		width, // width in pixels
		height, // height in pixels
		Bitmap.Config.ARGB_8888
	)
	val canvas = Canvas(bitmap)

	// path to draw rounded corners bitmap
	val path = Path().apply {
		addRoundRect(
			RectF(0f,0f,width.toFloat(),height.toFloat()),
			cornerRadius,
			cornerRadius,
			Path.Direction.CCW
		)
	}
	canvas.clipPath(path)

	// draw the rounded corners bitmap on canvas
	canvas.drawBitmap(this,0f,0f,null)
	return bitmap
}

fun Double.round(decimals: Int): Double {
	var multiplier = 1.0
	repeat(decimals) { multiplier *= 10 }
	return kotlin.math.round(this * multiplier) / multiplier
}

fun Float.round(decimals: Int): Double {
	var multiplier = 1.0
	repeat(decimals) { multiplier *= 10 }
	return kotlin.math.round(this * multiplier) / multiplier
}
