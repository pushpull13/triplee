package com.pushpull.triplee.custom

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout.LayoutParams

class Divider: View {
	constructor(context: Context): super(context)
	constructor(context: Context, attributeSet: AttributeSet): super(context, attributeSet)

	private var density: Float = context.resources.displayMetrics.density

	init {
		layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, 1.d).apply {
			setMargins(24.d, 0, 24.d, 0)
		}
		setBackgroundColor(Color.argb(235, 235, 235, 235))
	}

	val Int.d
		get() = (this * density).toInt()
}
