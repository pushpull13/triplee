package com.pushpull.triplee.custom

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.text.TextUtils
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.core.content.res.ResourcesCompat
import com.pushpull.triplee.R
import com.pushpull.triplee.dataClass.AppTheme
import com.pushpull.triplee.konstant.ActivityId


class ActivitySelectionView(context: Context) : FrameLayout(context) {

	private lateinit var activityList: List<ActivityId.Companion.ACTIVITY>

	private var localAppTheme: AppTheme = getLocalTheme()

	private var density: Float = context.resources.displayMetrics.density

	interface OnClickActivityListener {
		fun onClickActivity(activity: ActivityId.Companion.ACTIVITY)
	}

	private lateinit var onClickActivityListener: OnClickActivityListener
	fun setOnClickActivity(listener: OnClickActivityListener) {
		onClickActivityListener = listener
	}

	private var innerLayout: LinearLayout

	init {

		layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
		setBackgroundColor(localAppTheme.colorBeta)

		innerLayout = LinearLayout(context).apply {
			layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
			orientation = LinearLayout.HORIZONTAL
		}
		addView(
			HorizontalScrollView(context).apply {
				layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
				isHorizontalScrollBarEnabled = false
				addView(innerLayout)
			}
		)
	}

	fun updateView(activityList: List<ActivityId.Companion.ACTIVITY>, currentActivity: ActivityId.Companion.ACTIVITY) {
		this.activityList = activityList
		innerLayout.removeAllViews()
		activityList.forEach {
			innerLayout.addView(getActivityLayout(it, currentActivity))
			innerLayout.addView(
				View(context).apply {
					layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, 1).apply {
						setMargins(2.d, 0, 2.d, 0)
					}
					setBackgroundColor(Color.argb(128, 128, 128, 128))
				}
			)
		}
		innerLayout.addView(getActivityLayout(ActivityId.Companion.ACTIVITY.OTHER, currentActivity))
	}

	private fun getActivityLayout(activity: ActivityId.Companion.ACTIVITY, currentActivity: ActivityId.Companion.ACTIVITY): View {
		return CardView(context).apply {
			layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT).apply {
				setMargins(4.d, 8.d, 4.d, 8.d)
			}
			radius = 64.d.toFloat()
			cardElevation = 0F


			setOnClickListener {
				if (!ActivityId.ACTIVITY_ID_LOCK_MAP[activity]!!) {
					onClickActivityListener.onClickActivity(activity)
				} else {
					Toast.makeText(context, "Subscribe to Triplee Plus to unlock this activity", Toast.LENGTH_SHORT).show()
				}
			}

			if (currentActivity == activity) {
				setCardBackgroundColor(localAppTheme.colorPrimary)
			}
			addView(
				LinearLayout(context).apply {
					layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
					orientation = LinearLayout.VERTICAL
					addView(
						ImageView(context).apply {
							layoutParams = LayoutParams(24.d, 24.d).apply {
								setMargins(8.d, 16.d, 8.d, 4.d)
							}
							gravity = Gravity.CENTER_HORIZONTAL
							setImageResource(ActivityId.ACTIVITY_ID_RES_MAP[activity]!!)
							if (currentActivity == activity) {
								setColorFilter(localAppTheme.colorOnPrimary)
							} else {
								setColorFilter(localAppTheme.colorOnGamma)
							}
						})
					addView(
						TextView(context).apply {
							layoutParams = LayoutParams(72.d, 48.d).apply {
								setMargins(2.d, 16.d, 2.d, 0)
							}
							gravity = Gravity.CENTER_HORIZONTAL
							text = ActivityId.ACTIVITY_ID_NAME_MAP[activity]
							ellipsize = TextUtils.TruncateAt.END
							maxLines = 2
							setTextColor(localAppTheme.colorOnBackground)
							setTextSize(TypedValue.COMPLEX_UNIT_SP, 12F)
							setTypeface(ResourcesCompat.getFont(context, localAppTheme.textFontFamily), Typeface.BOLD)
							if (currentActivity == activity) {
								setTextColor(localAppTheme.colorOnPrimary)
							}
						})
					addView(
						ImageView(context).apply {
							layoutParams = LayoutParams(24.d, 24.d).apply {
								setMargins(8.d, 0.d, 8.d, 16.d)
							}
							if (!ActivityId.ACTIVITY_ID_LOCK_MAP[activity]!!) {
								visibility = View.INVISIBLE
							}
							gravity = Gravity.CENTER_HORIZONTAL
							setImageResource(R.drawable.ic_lock)
							if (currentActivity == activity) {
								setColorFilter(localAppTheme.colorOnPrimary)
							}
						})
					addView(
						ImageView(context).apply {
							layoutParams = LayoutParams(24.d, 24.d).apply {
								setMargins(8.d, 0.d, 8.d, 16.d)
							}
							gravity = Gravity.CENTER_HORIZONTAL
							setImageResource(R.drawable.ic_arrow_right)
							if (currentActivity == activity) {
								setColorFilter(localAppTheme.colorOnPrimary)
							} else {
								setColorFilter(localAppTheme.colorOnGamma)
							}
						})

				}
			)
		}
	}

	val Int.d
		get() = (this * density).toInt()

	companion object {
		const val TAG = "ACTIVITY_VIEW"
	}
}
