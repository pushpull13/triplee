package com.pushpull.triplee.tripDetail

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class TripDetailViewModelFactory(private val application: Application, private val primaryKey: Long) : ViewModelProvider.Factory {
	override fun <T : ViewModel?> create(modelClass: Class<T>): T {
		if (modelClass.isAssignableFrom(TripDetailViewModel::class.java)) {
			return TripDetailViewModel(application, primaryKey) as T
		}
		throw IllegalArgumentException("Unknown ViewModel class")
	}
}
