package com.pushpull.triplee.tripDetail

import android.app.Application
import android.content.ContentUris
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.exifinterface.media.ExifInterface
import androidx.lifecycle.AndroidViewModel
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.exc.MismatchedInputException
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.pushpull.triplee.custom.TimelineView
import com.pushpull.triplee.dataClass.TripData
import com.pushpull.triplee.database.UserDatabase
import com.pushpull.triplee.database.trip.Trip
import com.pushpull.triplee.konstant.Constant.Companion.Settings
import com.pushpull.triplee.utility.Utility
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.io.IOException

class TripDetailViewModel(_application: Application, val primaryKey: Long) : AndroidViewModel(_application) {

	private val job: Job = Job()
	private val ioScope = CoroutineScope(Dispatchers.IO + job)
	private val objectMapper = ObjectMapper().registerModule(KotlinModule())

	val tripTable = UserDatabase.getInstance(getApplication()).tripTableDao
	lateinit var trip: Trip

	interface OnDataAvailableListener {
		fun onDataAvailableListener(isDataAvailable: Boolean, isDataCorrupted: Boolean?)
	}

	private var onDataAvailableListener: OnDataAvailableListener? = null
	fun setOnDataAvailableListener(listener: OnDataAvailableListener) {
		onDataAvailableListener = listener
	}

	interface OnMediaAvailableListener {
		fun onMediaAvailableListener(isMediaAvailable: Boolean)
	}

	private var onMediaAvailableListener: OnMediaAvailableListener? = null
	fun setOnMediaAvailableListener(listener: OnMediaAvailableListener) {
		onMediaAvailableListener = listener
	}

	interface OnTimelineUpdateListener {
		fun onTimelineUpdateListener()
	}

	private var onTimelineUpdateListener: OnTimelineUpdateListener? = null
	fun setOnTimelineUpdateListener(listener: OnTimelineUpdateListener) {
		onTimelineUpdateListener = listener
	}

	private fun onMediaAvailableCallback() {
		timelineDataList.forEachIndexed { index, timelineData ->
			val timelineViewMedia = imageMediaStoreDataList.filter { it.timestamp >= timelineData.katoTimestamp && it.timestamp < timelineData.panoTimestamp }
			timelineDataList[index].mediaStoreDataList = timelineViewMedia
			onTimelineUpdateListener?.onTimelineUpdateListener()
		}
	}

	private var isDataAvailable = false
		set(value) {
			field = value
			onDataAvailableListener?.onDataAvailableListener(value, isDataCorrupted)
		}

	private var isDataCorrupted: Boolean? = null
		set(value) {
			field = value
			onDataAvailableListener?.onDataAvailableListener(isDataAvailable, value)
		}

	private val sharedPreferencesAppConfig: SharedPreferences = _application.getSharedPreferences(Settings.APP_CONFIG.name, AppCompatActivity.MODE_PRIVATE)
	private val coordinateUnit: Settings = Settings.values()[sharedPreferencesAppConfig.getInt(Settings.COORDINATE.name, Settings.DD.ordinal)]
	val distanceUnit: Settings = Settings.values()[sharedPreferencesAppConfig.getInt(Settings.DISTANCE.name, Settings.METRE.ordinal)]

	var minLatitude: Double = Double.MAX_VALUE
	var maxLatitude: Double = -Double.MAX_VALUE
	var minLongitude: Double = Double.MAX_VALUE
	var maxLongitude: Double = -Double.MAX_VALUE

	private var minAltitude: Double = Double.MAX_VALUE
	private var maxAltitude: Double = -Double.MAX_VALUE

	private var minSpeed: Double = Double.MAX_VALUE
	private var maxSpeed: Double = -Double.MAX_VALUE

	private var startLatLng: LatLng? = null

	var tripDataMap: MutableMap<String, TripData> = mutableMapOf()
	var locationDataList: MutableList<LocationData> = mutableListOf()
	private lateinit var tripActivityList: List<Int>
	var timelineDataList: MutableList<TimelineView.TimelineData> = mutableListOf()
	val imageMediaStoreDataList = mutableListOf<MediaStoreData>()
	val videoMediaStoreDataList = mutableListOf<MediaStoreData>()
	val audioMediaStoreDataList = mutableListOf<MediaStoreData>()
	private var isMediaDataQueried = false

	fun initData() {
		ioScope.launch {
			try {
				trip = tripTable.get(primaryKey)!!
				tripActivityList = objectMapper.readValue(trip.activityList)
				getAllData()
			} catch (exception: MismatchedInputException) {
				exception.printStackTrace()
				Log.e(TAG, "file corrupted")
				isDataCorrupted = true
			} catch (exception: Exception) {
				exception.printStackTrace()
				Log.e(TAG, "uncaught exception")
				isDataCorrupted = true
			}
		}
	}

	fun getAllData() {
		tripDataMap = mutableMapOf()
		locationDataList = mutableListOf()
		timelineDataList = mutableListOf()

		val baseFile = Utility.readDataFile(primaryKey, "0")
		val baseFileData: TripData = Utility.readEncryptedFileAsObject(baseFile)
		tripDataMap[baseFile.name] = baseFileData
		var pointer: String? = baseFileData.next
		var index = 0

		try {
			val baseLocationDataList = baseFileData.tripLocationDataMap.toList()
			if (baseLocationDataList.isNotEmpty()) {
				val baseLocationData = baseLocationDataList[0].second
				val latLng = LatLng(baseLocationData[0], baseLocationData[1])

				timelineDataList.add(
					TimelineView.TimelineData(
						timestamp = baseFileData.katoTimestamp,
						name = baseFileData.address?.split(",")?.get(0) ?: Utility.coordinateFilter(latLng, coordinateUnit),
						address = baseFileData.address,
						latLng = latLng,
						coordinateString = Utility.coordinateFilter(latLng, coordinateUnit),
						activity = baseFileData.activity,
						pseudoActivity = baseFileData.pseudoActivity,
						index = index,
						katoTimestamp = baseFileData.katoTimestamp,
						panoTimestamp = baseFileData.panoTimestamp,
						filePointer = "0",
						mediaStoreDataList = listOf()
					)
				)
				index++
			}
			baseFileData.tripLocationDataMap.forEach { (timestamp, locationData) -> processData(timestamp, locationData) }
		} catch (exception: Exception) {
			throw Exception()
		}

		while (pointer != null) {
			try {
				val nextFile = Utility.readDataFile(primaryKey, pointer)
				val nextFileData: TripData = Utility.readEncryptedFileAsObject(nextFile)
				tripDataMap[nextFile.name] = nextFileData

				val locationDataList = nextFileData.tripLocationDataMap.toList()
				if (locationDataList.isNotEmpty()) {
					val locationData = locationDataList[0].second
					val latLng = LatLng(locationData[0], locationData[1])

					timelineDataList.add(
						TimelineView.TimelineData(
							timestamp = nextFileData.katoTimestamp,
							name = nextFileData.address?.split(",")?.get(0) ?: Utility.coordinateFilter(latLng, coordinateUnit),
							address = nextFileData.address,
							latLng = latLng,
							coordinateString = Utility.coordinateFilter(latLng, coordinateUnit),
							activity = nextFileData.activity,
							pseudoActivity = nextFileData.pseudoActivity,
							index = index,
							katoTimestamp = nextFileData.katoTimestamp,
							panoTimestamp = nextFileData.panoTimestamp,
							filePointer = pointer,
							mediaStoreDataList = listOf()
						)
					)
				}
				nextFileData.tripLocationDataMap.forEach { (timestamp, locationData) -> processData(timestamp, locationData) }
				index++
				pointer = nextFileData.next
			} catch (exception: Exception) {
			}
		}

		timelineDataList.add(
			TimelineView.TimelineData(
				timestamp = trip.endTime!!,
				name = trip.destinationLocation?.split(",")?.get(0) ?: Utility.coordinateFilter(LatLng(trip.endLatitude, trip.endLongitude), coordinateUnit),
				address = trip.destinationLocation,
				latLng = LatLng(trip.endLatitude, trip.endLongitude),
				coordinateString = Utility.coordinateFilter(LatLng(trip.endLatitude, trip.endLongitude), coordinateUnit),
				activity = null,
				pseudoActivity = null,
				index = index,
				katoTimestamp = trip.endTime!!,
				panoTimestamp = trip.endTime!!,
				filePointer = null,
				mediaStoreDataList = listOf()
			)
		)

		startLatLng = LatLng(trip.startLatitude, trip.startLongitude)
		isDataAvailable = true
		isDataCorrupted = false
	}

	private fun processData(timestamp: Long, locationData: List<Double>) {
		val latLng = LatLng(locationData[0], locationData[1])
		locationDataList.add(
			LocationData(
				timestamp = timestamp,
				index = timestamp - trip.primaryKey,
				latLng = LatLng(locationData[0], locationData[1]),
				altitude = locationData[2],
				speed = locationData[3]))
		calculateBoundary(latLng)
		minAltitude = minOf(minAltitude, locationData[2])
		maxAltitude = maxOf(maxAltitude, locationData[2])
		minSpeed = minOf(minSpeed, locationData[2])
		maxSpeed = maxOf(maxSpeed, locationData[2])
	}

	private fun calculateBoundary(latLng: LatLng) {
		minLatitude = minOf(minLatitude, latLng.latitude)
		maxLatitude = maxOf(maxLatitude, latLng.latitude)
		minLongitude = minOf(minLongitude, latLng.longitude)
		maxLongitude = maxOf(maxLongitude, latLng.longitude)
	}

	fun refreshUI() {
		isDataAvailable = isDataAvailable
	}

	data class LocationData(
		val timestamp: Long,
		val index: Long,
		val latLng: LatLng,
		val altitude: Double,
		val speed: Double,
	)

	data class MediaStoreData(
		val id: Long,
		val displayName: String,
		val mediaType: MediaType,
		val timestamp: Long,
		val contentUri: Uri,
		var latLng: LatLng?,
	) {
		var thumbnailBitmap: Bitmap? = null
		var marker: Marker? = null
	}

	enum class MediaType {
		IMAGE,
		VIDEO,
		AUDIO
	}

	fun queryMediaData() {
		if (!isMediaDataQueried) {
			isMediaDataQueried = true
			ioScope.launch {
				getImageMedia()
				getVideoMedia()

				onMediaAvailableCallback()
				onMediaAvailableListener?.onMediaAvailableListener(true)
			}
		} else {
			onMediaAvailableCallback()
			onMediaAvailableListener?.onMediaAvailableListener(true)
		}
	}

	private fun getImageMedia() {
		val projection = arrayOf(
			MediaStore.Images.Media._ID,
			MediaStore.Images.Media.DISPLAY_NAME,
			MediaStore.Images.Media.DATE_ADDED
		)
		val selection = "${MediaStore.Images.Media.DATE_ADDED} >=? AND ${MediaStore.Images.Media.DATE_ADDED} <=?"
		val selectionArgs = arrayOf(
			"${0}",
			"${Long.MAX_VALUE}"
		)
		val sortOrder = "${MediaStore.Images.Media.DATE_ADDED} DESC"

		getApplication<Application>().contentResolver.query(
			MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
			projection,
			selection,
			selectionArgs,
			sortOrder
		)?.use { cursor ->
			val idColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID)
			val timestampColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATE_ADDED)
			val displayNameColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME)
			cursor.apply {
				while (moveToNext()) {
					val contentUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, cursor.getLong(idColumn))
					var latLng: LatLng? = null
					try {
						getApplication<Application>().contentResolver.openInputStream(contentUri).use { inputStream ->
							val exif = ExifInterface(inputStream!!)
							latLng = exif.latLong?.let { LatLng(it[0], it[1]) }
						}
					} catch (e: IOException) {
						e.printStackTrace()
					}
					imageMediaStoreDataList.add(
						MediaStoreData(
							id = cursor.getLong(idColumn),
							displayName = cursor.getString(displayNameColumn),
							mediaType = MediaType.IMAGE,
							timestamp = cursor.getLong(timestampColumn),
							contentUri = contentUri,
							latLng = latLng
						))
				}
			}
		}
	}

	private fun getVideoMedia() {
		val projection = arrayOf(
			MediaStore.Video.Media._ID,
			MediaStore.Video.Media.DISPLAY_NAME,
			MediaStore.Video.Media.DATE_ADDED
		)
		val selection = "${MediaStore.Video.Media.DATE_ADDED} >=? AND ${MediaStore.Video.Media.DATE_ADDED} <=?"
		val selectionArgs = arrayOf(
			"${trip.primaryKey / 1000}",
			"${trip.endTime!! / 1000}"
		)
		val sortOrder = "${MediaStore.Video.Media.DATE_ADDED} DESC"

		getApplication<Application>().contentResolver.query(
			MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
			projection,
			selection,
			selectionArgs,
			sortOrder
		)?.use { cursor ->
			val idColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media._ID)
			val timestampColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATE_ADDED)
			val displayNameColumn = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DISPLAY_NAME)
			cursor.apply {
				while (moveToNext()) {
					val contentUri = ContentUris.withAppendedId(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, cursor.getLong(idColumn))
					var latLng: LatLng? = null
					try {
						getApplication<Application>().contentResolver.openInputStream(contentUri).use { inputStream ->
							val exif = ExifInterface(inputStream!!)
							latLng = exif.latLong?.let { LatLng(it[0], it[1]) }
						}
					} catch (e: IOException) {
						e.printStackTrace()
					}
					videoMediaStoreDataList.add(
						MediaStoreData(
							id = cursor.getLong(idColumn),
							displayName = cursor.getString(displayNameColumn),
							mediaType = MediaType.VIDEO,
							timestamp = cursor.getLong(timestampColumn),
							contentUri = contentUri,
							latLng = latLng
						))
				}
			}
		}
	}

	companion object {
		const val TAG = "TRIP_DETAIL_VIEW_MODEL"
	}
}
