package com.pushpull.triplee.tripDetail

import android.Manifest
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.*
import android.graphics.drawable.Drawable
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.os.CancellationSignal
import android.util.Log
import android.util.Size
import android.util.TypedValue
import android.view.*
import android.view.View.MeasureSpec
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.content.res.AppCompatResources
import androidx.cardview.widget.CardView
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.blue
import androidx.core.graphics.green
import androidx.core.graphics.red
import androidx.core.widget.NestedScrollView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.google.android.flexbox.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.button.MaterialButton
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.maps.android.clustering.Cluster
import com.google.maps.android.clustering.ClusterItem
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import com.pushpull.triplee.custom.BaseActivity
import com.pushpull.triplee.custom.TimelineView
import com.pushpull.triplee.custom.toRoundedCorners
import com.pushpull.triplee.databinding.*
import com.pushpull.triplee.konstant.ActivityId
import com.pushpull.triplee.konstant.Constant
import com.pushpull.triplee.konstant.Path
import com.pushpull.triplee.utility.Utility
import io.ktor.http.cio.websocket.*
import kotlinx.coroutines.launch
import java.io.File
import java.util.*
import kotlin.properties.Delegates
import android.widget.Toast
import io.jenetics.jpx.GPX
import com.pushpull.triplee.R

class TripDetailActivity : BaseActivity(),
	OnMapReadyCallback,
	TripDetailViewModel.OnDataAvailableListener,
	TripDetailViewModel.OnTimelineUpdateListener {

	private lateinit var viewModel: TripDetailViewModel
	private lateinit var dataBinding: TripDetailActivityBinding

	private lateinit var inputMethodManager: InputMethodManager

	private var primaryKey by Delegates.notNull<Long>()

	private lateinit var geoCoder: Geocoder
	private var isReverseGeocoded = false

	private lateinit var googleMap: GoogleMap
	private var mapStyle: Constant.Companion.MapStyle = Constant.Companion.MapStyle.COFFEE
	private lateinit var timelineView: TimelineView
	private lateinit var localityChipGroup: ChipGroup
	private lateinit var localityChipList: MutableList<Chip>
	private lateinit var activityChipGroup: ChipGroup
	private lateinit var activityChipList: MutableList<Chip>

	private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>
	private lateinit var innerViewAdapter: InnerViewAdapter

	private val mediaLoadCancellationSignal = CancellationSignal()
	private var mediaClusterManager: ClusterManager<MediaClusterItem>? = null

	private var polylineColor = 0

	private var tripPolyline: Polyline? = null
	private var startMarker: Marker? = null
	private var endMaker: Marker? = null
	private var markerList: MutableList<Marker> = mutableListOf()

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		density = resources.displayMetrics.density

		window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

		polylineColor = sharedPreferencesAppConfig.getInt(Constant.Companion.Settings.MAP_POLYLINE_COLOR.name, localAppTheme.colorPrimary)

		dataBinding = DataBindingUtil.setContentView(this, R.layout.trip_detail_activity)
		dataBinding.tripDetailActivity = this

		inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager

		primaryKey = intent.getLongExtra(Constant.Companion.Settings.PRIMARY_KEY.name, -1L)

		viewModel = ViewModelProvider(this, TripDetailViewModelFactory(application, primaryKey)).get(TripDetailViewModel::class.java)

		dataBinding.tripDetailActivityBack.setOnClickListener { finish() }
		dataBinding.tripDetailActivityShare.setOnClickListener {

		}
		dataBinding.tripDetailActivityExport.setOnClickListener {
			try {
				registerForCreateFileActivityResult.launch("${viewModel.trip.title}.gpx")			} catch (exception: Exception) {
				registerForCreateFileActivityResult.launch("${viewModel.trip.title}.gpx")
			}
		}

		viewModel.setOnDataAvailableListener(this)
		viewModel.initData()

		dataBinding.tripDetailActivityGoogleMap.onCreate(savedInstanceState)
		dataBinding.tripDetailActivityGoogleMap.onResume()
		dataBinding.tripDetailActivityGoogleMap.getMapAsync(this)

		timelineView = TimelineView(this).apply { id = View.generateViewId() }
		localityChipGroup = ChipGroup(this).apply {
			id = View.generateViewId()
			layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
		}
		activityChipGroup = ChipGroup(this).apply {
			id = View.generateViewId()
			layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
		}

		bottomSheetBehavior = BottomSheetBehavior.from(dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityBottomSheet)
		bottomSheetBehavior.halfExpandedRatio = 0.6f
		bottomSheetBehavior.isFitToContents = false
		bottomSheetBehavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
			override fun onStateChanged(bottomSheet: View, newState: Int) {
				when (newState) {
					BottomSheetBehavior.STATE_EXPANDED -> {
						val scaleAnimator = ValueAnimator
							.ofInt(32.d, 0)
							.setDuration(100)
						scaleAnimator.addUpdateListener { animator ->
							val value = animator.animatedValue as Int
							dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityBottomSheetScrollBeacon.layoutParams.width = value
							dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityBottomSheetScrollBeacon.requestLayout()
						}
						if (dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityBottomSheetScrollBeacon.layoutParams.width == 32.d) {
							scaleAnimator.start()
						}
					}
					BottomSheetBehavior.STATE_HALF_EXPANDED -> {
						val scaleAnimator = ValueAnimator
							.ofInt(0, 32.d)
							.setDuration(100)
						scaleAnimator.addUpdateListener { animator ->
							val value = animator.animatedValue as Int
							dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityBottomSheetScrollBeacon.layoutParams.width = value
							dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityBottomSheetScrollBeacon.requestLayout()
						}
						if (dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityBottomSheetScrollBeacon.layoutParams.width == 0) {
							scaleAnimator.start()
						}
					}
					BottomSheetBehavior.STATE_COLLAPSED -> {
						val scaleAnimator = ValueAnimator
							.ofInt(0, 32.d)
							.setDuration(100)
						scaleAnimator.addUpdateListener { animator ->
							val value = animator.animatedValue as Int
							dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityBottomSheetScrollBeacon.layoutParams.width = value
							dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityBottomSheetScrollBeacon.requestLayout()
						}
						if (dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityBottomSheetScrollBeacon.layoutParams.width == 0) {
							scaleAnimator.start()
						}
					}
				}
			}

			override fun onSlide(bottomSheet: View, slideOffset: Float) {}
		})
	}

	override fun onDestroy() {
		super.onDestroy()
		mediaLoadCancellationSignal.cancel()
	}

	override fun onMapReady(map: GoogleMap) {
		googleMap = map

		googleMap.apply {
			mapStyle = Constant.Companion.MapStyle.values()[sharedPreferencesAppConfig
				.getInt(Constant.Companion.Settings.MAP_STYLE.name, Constant.Companion.MapStyle.PAPER.ordinal)]
			googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this@TripDetailActivity, Constant.MAP_STYLE_RES_MAP[mapStyle]!!))

			setMaxZoomPreference(20f)
			setMinZoomPreference(2.911f)
			uiSettings.isTiltGesturesEnabled = true
			isBuildingsEnabled = false

			mediaClusterManager = ClusterManager(this@TripDetailActivity, googleMap)
			mediaClusterManager!!.renderer = MarkerClusterRenderer()
			setOnCameraIdleListener(mediaClusterManager)
			setOnMarkerClickListener(mediaClusterManager)
		}

		viewModel.refreshUI()
	}

	override fun onDataAvailableListener(isDataAvailable: Boolean, isDataCorrupted: Boolean?) {
		mainScope.launch {
			if (this@TripDetailActivity::googleMap.isInitialized && isDataAvailable && isDataCorrupted == false) {
				dataBinding.tripDetailActivityGoogleMap.visibility = View.VISIBLE
				dataBinding.tripDetailActivityLottieLoading.visibility = View.GONE
				dataBinding.tripDetailActivityLottieLoading.pauseAnimation()

				startMarker?.remove()
				endMaker?.remove()
				markerList.forEach { it.remove() }
				markerList = mutableListOf()
				startMarker = googleMap.addMarker(
					MarkerOptions()
						.icon(getBitmap(R.drawable.ic_map_marker_2, Color.argb(255, 73, 148, 113))?.let { BitmapDescriptorFactory.fromBitmap(it) })
						.position(LatLng(viewModel.trip.startLatitude, viewModel.trip.startLongitude))
				)
				endMaker = googleMap.addMarker(
					MarkerOptions()
						.icon(getBitmap(R.drawable.ic_map_marker_2, Color.argb(255, 235, 127, 116))?.let { BitmapDescriptorFactory.fromBitmap(it) })
						.position(LatLng(viewModel.trip.endLatitude, viewModel.trip.endLongitude))
				)
				viewModel.timelineDataList.forEachIndexed { index, timelineData ->
					if (index != viewModel.timelineDataList.size - 1) {
						val marker = googleMap.addMarker(
							MarkerOptions()
								.icon(getActivityMarker(timelineData.activity, timelineData.pseudoActivity)?.let { BitmapDescriptorFactory.fromBitmap(it) })
								.position(timelineData.latLng)
								.anchor(0.5f, 0.5f)
						)!!
						markerList.add(marker)
					}
				}

				val latLngList: MutableList<LatLng> = mutableListOf()
				viewModel.locationDataList.forEach { latLngList.add(it.latLng) }
				if (latLngList.isNotEmpty()) {
					tripPolyline?.remove()
					tripPolyline = googleMap.addPolyline(
						PolylineOptions()
							.addAll(latLngList)
							.zIndex(2F)
							.width(16f)
							.startCap(RoundCap())
							.endCap(RoundCap())
							.jointType(JointType.ROUND)
							.color(Color.argb(144, polylineColor.red, polylineColor.green, polylineColor.blue))
					)
				}

				zoomOut(false)

				initializeUI()
				initializeLocalityChips()
				initializeActivityChips()
				initializeTimeline()
				checkAddress()
				getAddress()
			} else if (isDataCorrupted == true) {
				dataBinding.tripDetailActivityLottieLoading.visibility = View.GONE
				dataBinding.tripDetailActivityLottieLoading.pauseAnimation()

				dataBinding.tripDetailActivityDataCorrupted.visibility = View.VISIBLE
			}
		}
	}

	override fun onTimelineUpdateListener() {
		timelineView.updateView(viewModel.timelineDataList)
	}

	override fun onBackPressed() {
		when (bottomSheetBehavior.state) {
			BottomSheetBehavior.STATE_HALF_EXPANDED -> bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
			BottomSheetBehavior.STATE_EXPANDED -> bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
			else -> finish()
		}
	}

	private fun initializeUI() {
		if (!this::innerViewAdapter.isInitialized) {
			dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityTitle.text = viewModel.trip.title

			dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityDuration.text =
				Utility.durationFilter(viewModel.trip.primaryKey - (viewModel.trip.endTime ?: 0), true)

			val distance = viewModel.trip.distance
			dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityDistance.text = Utility.distanceFilter(distance, viewModel.distanceUnit)

			innerViewAdapter = InnerViewAdapter()

			dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityViewPager.adapter = innerViewAdapter
			dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityViewPager.registerOnPageChangeCallback(object :
				ViewPager2.OnPageChangeCallback() {
				@SuppressLint("UseCompatTextViewDrawableApis")
				override fun onPageSelected(position: Int) {
					super.onPageSelected(position)
					bleachDrawable()

					val buttonStates = arrayOf(intArrayOf(android.R.attr.state_enabled))
					val tripPageStateColors = intArrayOf(Color.argb(255, 73, 148, 113))
					val mediaPageStateColors = intArrayOf(Color.argb(255, 168, 193, 232))

					val tripPageButtonStateColors = ColorStateList(buttonStates, tripPageStateColors)
					val mediaPageButtonStateColors = ColorStateList(buttonStates, mediaPageStateColors)
					when (position) {
						0 -> dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityTripPage.apply {
							setTextColor(localAppTheme.colorOnBackground)
							compoundDrawableTintList = tripPageButtonStateColors
							setTypeface(ResourcesCompat.getFont(context, R.font.rubik_medium), Typeface.BOLD)
						}
						1 -> dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityMediaPage.apply {
							setTextColor(localAppTheme.colorOnBackground)
							compoundDrawableTintList = mediaPageButtonStateColors
							setTypeface(ResourcesCompat.getFont(context, R.font.rubik_medium), Typeface.BOLD)
						}
					}
				}

				@SuppressLint("UseCompatTextViewDrawableApis")
				private fun bleachDrawable() {
					val buttonStates = arrayOf(intArrayOf(android.R.attr.state_enabled))
					val stateColors = intArrayOf(0)
					val buttonStateColors = ColorStateList(buttonStates, stateColors)

					dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityTripPage.apply {
						setTextColor(localAppTheme.colorOnSurface)
						compoundDrawableTintList = buttonStateColors
						setTypeface(ResourcesCompat.getFont(context, R.font.rubik_medium), Typeface.NORMAL)
					}
					dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityMediaPage.apply {
						setTextColor(localAppTheme.colorOnSurface)
						compoundDrawableTintList = buttonStateColors
						setTypeface(ResourcesCompat.getFont(context, R.font.rubik_medium), Typeface.NORMAL)
					}

				}
			})

			initializeButton()
		}
	}

	private fun initializeButton() {
		dataBinding.tripDetailZoomButton.setOnClickListener {
			zoomOut(true)
		}
		dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityRenameButton.setOnClickListener {
			dataBinding.tripDetailZoomButton.visibility = View.GONE
			dataBinding.tripDetailActivityRenameLayout.visibility = View.VISIBLE
			dataBinding.tripDetailZoomButton.hide()
		}
		dataBinding.tripDetailActivityRenameDiscardButton.setOnClickListener {
			dataBinding.tripDetailActivityRenameLayout.visibility = View.GONE
			dataBinding.tripDetailZoomButton.show()
			inputMethodManager.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY)
		}
		dataBinding.tripDetailActivityRenameSaveButton.setOnClickListener {
			viewModel.trip.title = dataBinding.tripDetailActivityNewTitle.text.toString()
			ioScope.launch { viewModel.tripTable.insert(viewModel.trip) }

			dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityTitle.text = viewModel.trip.title
			dataBinding.tripDetailActivityRenameLayout.visibility = View.GONE
			dataBinding.tripDetailZoomButton.show()
			inputMethodManager.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY)
		}
		dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityTripPage.setOnClickListener {
			dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityViewPager.setCurrentItem(0, true)
		}
		dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityMediaPage.setOnClickListener {
			dataBinding.tripDetailActivityBottomSheetLayout.tripDetailActivityViewPager.setCurrentItem(2, true)
		}
	}

	private fun zoomOut(doAnimate: Boolean = false) {
		if (doAnimate) {
			googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(LatLngBounds.Builder()
				.include(LatLng(viewModel.minLatitude, viewModel.minLongitude))
				.include(LatLng(viewModel.maxLatitude, viewModel.maxLongitude))
				.build(), 96))
		} else {
			googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(LatLngBounds.Builder()
				.include(LatLng(viewModel.minLatitude, viewModel.minLongitude))
				.include(LatLng(viewModel.maxLatitude, viewModel.maxLongitude))
				.build(), 96))
		}
	}

	private fun checkAddress() {
		viewModel.tripDataMap.forEach {
			if (it.value.address.isNullOrBlank() || it.value.locality.isNullOrBlank()) {
				mainScope.launch { innerViewAdapter.fetchAddressButton.visibility = View.VISIBLE }
			}
		}
	}

	private fun getAddress() {
		geoCoder = Geocoder(this)

		ioScope.launch {
			viewModel.tripDataMap.forEach {
				if (it.value.address.isNullOrBlank() || it.value.locality.isNullOrBlank()) {
					val locationDataList = it.value.tripLocationDataMap.toList()
					if (locationDataList.isNotEmpty()) {
						try {
							val locationData = locationDataList[0]
							val address = geoCoder.getFromLocation(locationData.second[0], locationData.second[1], 1)
							if (address.isNotEmpty()) {
								val _address = address[0]
								it.value.address = _address.getAddressLine(0)
								it.value.locality = _address.subAdminArea

								val file = File("${Path.TRIP_FOLDER_NAME}/${viewModel.primaryKey}/${it.key}")
								Utility.writeFileAsEncryptedObject(file, it.value)
							}
						} catch (exception: Exception) {
							exception.printStackTrace()
						}
					}
				}
			}

			if (viewModel.trip.departureLocation.isNullOrBlank()) {
				try {
					val address = geoCoder.getFromLocation(viewModel.trip.startLatitude, viewModel.trip.startLongitude, 1)
					if (address.isNotEmpty()) {
						val _address = address[0]
						viewModel.trip.departureLocation = _address.getAddressLine(0)
					}
				} catch (exception: Exception) {
					exception.printStackTrace()
				}
			}

			if (viewModel.trip.destinationLocation.isNullOrBlank()) {
				try {
					val address = geoCoder.getFromLocation(viewModel.trip.endLatitude, viewModel.trip.endLongitude, 1)
					if (address.isNotEmpty()) {
						val _address = address[0]
						viewModel.trip.destinationLocation = _address.getAddressLine(0)
					}
				} catch (exception: Exception) {
					exception.printStackTrace()
				}
			}

			viewModel.tripTable.insert(viewModel.trip)

			viewModel.getAllData()
			mainScope.launch { innerViewAdapter.fetchAddressButton.visibility = View.GONE }

			isReverseGeocoded = true
		}
	}

	private fun initializeLocalityChips() {
		localityChipGroup.removeAllViews()
		localityChipList = mutableListOf()
		val localitySet: MutableSet<String> = mutableSetOf()
		viewModel.tripDataMap.forEach {
			it.value.locality?.let { locality ->
				localitySet.add(locality)
			}
		}

		localitySet.forEach { locality ->
			val chip = Chip(this).apply {
				layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)

				text = locality
				setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)
				setTypeface(ResourcesCompat.getFont(context, R.font.baloo2_bold), Typeface.BOLD)
				setTextColor(localAppTheme.colorOnBackground)
			}

			localityChipList.add(chip)
		}

		localityChipList.forEach { localityChipGroup.addView(it) }
	}

	private fun initializeActivityChips() {
		activityChipGroup.removeAllViews()
		activityChipList = mutableListOf()
		val activitySet: MutableSet<Int> = mutableSetOf()
		viewModel.tripDataMap.forEach {
			activitySet.add(it.value.activity)
		}

		activitySet.forEach { activityId ->
			val chip = Chip(this).apply {
				id = View.generateViewId()
				layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)

				val activity = ActivityId.Companion.ACTIVITY.values()[activityId]

				text = ActivityId.ACTIVITY_ID_NAME_MAP[activity]
				setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)
				setTypeface(ResourcesCompat.getFont(context, R.font.baloo2_bold), Typeface.BOLD)
				setTextColor(localAppTheme.colorOnBackground)

				chipIcon = AppCompatResources.getDrawable(this@TripDetailActivity, ActivityId.ACTIVITY_ID_RES_MAP[activity]!!)

				val stateList = arrayOf(intArrayOf(android.R.attr.state_enabled))
				val colorList = intArrayOf(localAppTheme.colorPrimary)
				chipIconTint = ColorStateList(stateList, colorList)
				chipIconSize = 20.d.toFloat()
				iconStartPadding = 8.d.toFloat()
			}

			activityChipList.add(chip)
		}

		activityChipList.forEach { activityChipGroup.addView(it) }
	}

	private fun initializeTimeline() {
		timelineView.updateView(viewModel.timelineDataList)
	}

	private fun showMediaOnMap() {
		mediaClusterManager!!.clearItems()
		viewModel.imageMediaStoreDataList.forEach { mediaStoreData ->
			if (mediaStoreData.latLng != null) {
				val thumbnailBitmap = mediaStoreData.thumbnailBitmap
				if (thumbnailBitmap != null) {
					if (mediaClusterManager == null) {
						if (this::googleMap.isInitialized) {
							mediaClusterManager = ClusterManager(this, googleMap)
							mediaClusterManager!!.renderer = MarkerClusterRenderer()
						}
					}
					Log.i(TAG, mediaStoreData.timestamp.toString())
					addMediaToCluster(mediaStoreData)
				}
			}
		}

		viewModel.videoMediaStoreDataList.forEach { mediaStoreData ->
			if (mediaStoreData.latLng != null) {
				val thumbnailBitmap = mediaStoreData.thumbnailBitmap
				if (thumbnailBitmap != null) {
					if (mediaClusterManager == null) {
						if (this::googleMap.isInitialized) {
							mediaClusterManager = ClusterManager(this, googleMap)
							mediaClusterManager!!.renderer = MarkerClusterRenderer()
						}
					}
					Log.i(TAG, mediaStoreData.timestamp.toString())
					addMediaToCluster(mediaStoreData)
				}
			}
		}

		viewModel.audioMediaStoreDataList.forEach { mediaStoreData ->
			if (mediaStoreData.latLng != null) {
				val thumbnailBitmap = mediaStoreData.thumbnailBitmap
				if (thumbnailBitmap != null) {
					if (mediaClusterManager == null) {
						if (this::googleMap.isInitialized) {
							mediaClusterManager = ClusterManager(this, googleMap)
							mediaClusterManager!!.renderer = MarkerClusterRenderer()
						}
					}
					Log.i(TAG, mediaStoreData.timestamp.toString())
					addMediaToCluster(mediaStoreData)
				}
			}
		}
	}

	private fun addMediaToCluster(mediaStoreData: TripDetailViewModel.MediaStoreData) {
		if (mediaClusterManager != null) {
			mediaClusterManager!!.addItem(MediaClusterItem(mediaStoreData, "", ""))
			mediaClusterManager!!.cluster()
		}
	}

//	private fun showDeleteDialog() {
//		AlertDialog.Builder(this).apply {
//			setTitle("Delete Trip")
//			setMessage("Are you sure you want to delete this trip? You won't be able to recover it afterwards.")
//			setPositiveButton("DELETE") { _, _ ->
//
//				Intent().apply {
//					putExtra("TO_DELETE", primaryKey)
//					setResult(RESULT_OK, this)
//				}
//				finish()
//			}
//			setNegativeButton("CANCEL", null)
//			show()
//		}
//	}

	private val registerReadExternalStorageResult = registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
		if (isGranted) {
			viewModel.queryMediaData()
		} else {
			Log.i(TAG, "denied...")
		}
	}

	private val registerMediaLocationAccessResult = registerForActivityResult(ActivityResultContracts.RequestPermission()) {}

	private inner class TripDetailMediaGridAdapter(val mediaType: TripDetailViewModel.MediaType) : BaseAdapter() {
		val mediaStoreDataList: MutableList<TripDetailViewModel.MediaStoreData> = when (mediaType) {
			TripDetailViewModel.MediaType.IMAGE -> viewModel.imageMediaStoreDataList
			TripDetailViewModel.MediaType.VIDEO -> viewModel.videoMediaStoreDataList
			TripDetailViewModel.MediaType.AUDIO -> viewModel.audioMediaStoreDataList
		}

		override fun getCount(): Int = mediaStoreDataList.size

		override fun getItem(position: Int): Any {
			return mediaStoreDataList[position]
		}

		override fun getItemId(position: Int): Long {
			return mediaStoreDataList[position].id
		}

		override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
			return CardView(this@TripDetailActivity).apply {
				id = View.generateViewId()
				layoutParams = FrameLayout.LayoutParams((widthDp * 0.29).toInt().d, (widthDp * 0.29).toInt().d).apply {
					if (position == mediaStoreDataList.size - 1) {
						setMargins(0, 0, 0, (heightDp.d * 0.5).toInt())
					}
				}
				radius = 48f
				cardElevation = 0f

				val mediaStoreData = mediaStoreDataList[position]

				setOnClickListener {
					Intent().apply {
						action = Intent.ACTION_VIEW
						when (mediaType) {
							TripDetailViewModel.MediaType.IMAGE -> setDataAndType(mediaStoreData.contentUri, "image/*")
							TripDetailViewModel.MediaType.VIDEO -> setDataAndType(mediaStoreData.contentUri, "video/*")
							TripDetailViewModel.MediaType.AUDIO -> setDataAndType(mediaStoreData.contentUri, "audio/*")
						}
						startActivity(this)
					}
				}

				addView(
					ImageView(this@TripDetailActivity).apply {
						id = View.generateViewId()
						layoutParams = ViewGroup.LayoutParams((widthDp * 0.3).toInt().d, (widthDp * 0.3).toInt().d)
						scaleType = ImageView.ScaleType.CENTER_CROP

						val contentUri = mediaStoreData.contentUri

						when {
							Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q -> {
								val bitmap = contentResolver.loadThumbnail(
									contentUri,
									Size((widthDp * 0.3).toInt().d, (widthDp * 0.3).toInt().d),
									mediaLoadCancellationSignal)
								mediaStoreData.thumbnailBitmap = bitmap
								setImageBitmap(bitmap)
							}
							else -> {
								ioScope.launch {
									val bitmap = Utility.getThumbnail(context, mediaStoreData.id)
									mediaStoreData.thumbnailBitmap = bitmap
									if (bitmap != null) {
										mainScope.launch { setImageBitmap(bitmap) }
									}
								}
							}
						}
					}
				)
			}
		}
	}

	inner class InnerViewAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

		val fetchAddressButton = MaterialButton(this@TripDetailActivity).apply {
			id = View.generateViewId()
			layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
				setPadding(16.d, 8.d, 16.d, 8.d)
			}
			visibility = View.GONE

			setOnClickListener {
				getAddress()
			}

			text = "FETCH ADDRESS"
			setTextColor(localAppTheme.colorOnPrimary)
			setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)
			setTypeface(ResourcesCompat.getFont(context, localAppTheme.textFontFamily), Typeface.BOLD)
		}
		val localityChipScrollView = HorizontalScrollView(this@TripDetailActivity).apply {
			id = View.generateViewId()
			layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
				setPadding(16.d, 0, 16.d, 0)
			}
			isHorizontalScrollBarEnabled = false
			isVerticalScrollBarEnabled = false

			addView(localityChipGroup)
		}

		val activityChipScrollView = HorizontalScrollView(this@TripDetailActivity).apply {
			id = View.generateViewId()
			layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
				setPadding(16.d, 0, 16.d, 0)
			}
			isHorizontalScrollBarEnabled = false
			isVerticalScrollBarEnabled = false

			addView(activityChipGroup)
		}

		private inner class TripPageViewHolder(val innerLayout: NestedScrollView) : RecyclerView.ViewHolder(innerLayout) {
			fun bind() {
				innerLayout.id = View.generateViewId()
				val linearLayout = LinearLayout(this@TripDetailActivity).apply {
					id = View.generateViewId()
					layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
					orientation = LinearLayout.VERTICAL
					gravity = Gravity.CENTER_HORIZONTAL

					addView(localityChipScrollView)
					addView(activityChipScrollView)
					addView(fetchAddressButton)
					addView(timelineView)
				}
				innerLayout.isFillViewport = true
				innerLayout.addView(linearLayout)
			}
		}

		private inner class MediaPageViewHolder(val innerLayout: FrameLayout) : RecyclerView.ViewHolder(innerLayout),
			TripDetailViewModel.OnMediaAvailableListener {
			fun bind() {
				innerLayout.id = View.generateViewId()
				viewModel.setOnMediaAvailableListener(this)

				if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
					viewModel.queryMediaData()
				} else {
					innerLayout.addView(
						FrameLayout(this@TripDetailActivity).apply {
							id = View.generateViewId()
							layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
								setPadding(16.d, 16.d, 16.d, 16.d)
							}
							addView(
								FlexboxLayout(this@TripDetailActivity).apply {
									id = View.generateViewId()
									layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
									flexDirection = FlexDirection.ROW
									justifyContent = JustifyContent.CENTER

									addView(
										MaterialButton(this@TripDetailActivity).apply {
											id = View.generateViewId()
											layoutParams =
												ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
													setPadding(16.d, 8.d, 16.d, 8.d)
												}
											text = "Show Media"
											setTextColor(localAppTheme.colorOnPrimary)
											setTextSize(TypedValue.COMPLEX_UNIT_SP, 16F)
											setTypeface(ResourcesCompat.getFont(context, localAppTheme.textFontFamily), Typeface.BOLD)

											setOnClickListener { registerReadExternalStorageResult.launch(Manifest.permission.READ_EXTERNAL_STORAGE) }
										}
									)
								}
							)
						}
					)
				}
			}

			override fun onMediaAvailableListener(isMediaAvailable: Boolean) {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
					registerMediaLocationAccessResult.launch(Manifest.permission.ACCESS_MEDIA_LOCATION)
				}
				mainScope.launch {
					if (isMediaAvailable) {
						innerLayout.removeAllViews()

						val linearLayout = LinearLayout(this@TripDetailActivity).apply {
							id = View.generateViewId()
							layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
							orientation = LinearLayout.VERTICAL

							var isMediaDataAvailable = false
							if (viewModel.imageMediaStoreDataList.isNotEmpty()) {
								addView(drawShowOnMapButton(TripDetailViewModel.MediaType.IMAGE, true))
								addView(drawGridView(TripDetailViewModel.MediaType.IMAGE))
								isMediaDataAvailable = true
							}

							if (viewModel.videoMediaStoreDataList.isNotEmpty()) {
								addView(drawShowOnMapButton(TripDetailViewModel.MediaType.VIDEO, false))
								addView(drawGridView(TripDetailViewModel.MediaType.VIDEO))
								isMediaDataAvailable = true
							}

							if (viewModel.audioMediaStoreDataList.isNotEmpty()) {
								addView(drawShowOnMapButton(TripDetailViewModel.MediaType.AUDIO, false))
								addView(drawGridView(TripDetailViewModel.MediaType.AUDIO))
								isMediaDataAvailable = true
							}

							if (!isMediaDataAvailable) {
								addView(drawShowOnMapButton(null, false))
							} else {
								addView(
									CardView(this@TripDetailActivity).apply {
										layoutParams =
											LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
												setPadding(16.d, 16.d, 16.d, 0)
												setMargins(16.d, 16.d, 16.d, 16.d)
											}
										radius = 16.d.toFloat()
										cardElevation = 0f

										addView(
											TextView(this@TripDetailActivity).apply {
												LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
													setPadding(16.d, 16.d, 16.d, 16.d)
												}

												text = "Media files are not synced but accessed directily from local data."
												setBackgroundColor(0)
												setTextSize(TypedValue.COMPLEX_UNIT_SP, 14F)
												setTypeface(ResourcesCompat.getFont(context, localAppTheme.textFontFamily), Typeface.BOLD)
											}
										)
									}
								)
							}
						}

						innerLayout.addView(linearLayout)
					}
				}
			}

			private fun drawShowOnMapButton(mediaType: TripDetailViewModel.MediaType?, enableShowOnMap: Boolean): View {
				return FrameLayout(this@TripDetailActivity).apply {
					id = View.generateViewId()
					layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
						setPadding(16.d, 0, 16.d, 0)
					}
					addView(
						FlexboxLayout(this@TripDetailActivity).apply {
							id = View.generateViewId()
							layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
							flexDirection = FlexDirection.ROW
							justifyContent = JustifyContent.SPACE_BETWEEN
							alignItems = AlignItems.CENTER

							addView(
								TextView(this@TripDetailActivity).apply {
									id = View.generateViewId()
									layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
									text = when (mediaType) {
										TripDetailViewModel.MediaType.IMAGE -> "Photos"
										TripDetailViewModel.MediaType.VIDEO -> "Videos"
										TripDetailViewModel.MediaType.AUDIO -> "Audios"
										else -> "No media found"
									}
									setTextColor(localAppTheme.colorPrimary)
									setTextSize(TypedValue.COMPLEX_UNIT_SP, 20F)
									setTypeface(ResourcesCompat.getFont(context, R.font.rubik_medium), Typeface.NORMAL)
								}
							)

							if (enableShowOnMap) {
								addView(
									MaterialButton(this@TripDetailActivity).apply {
										id = View.generateViewId()
										layoutParams =
											ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
												setPadding(16.d, 8.d, 16.d, 8.d)
											}
										text = "Show on map"
										setTextColor(localAppTheme.colorOnPrimary)
										setTextSize(TypedValue.COMPLEX_UNIT_SP, 16F)
										setTypeface(ResourcesCompat.getFont(context, localAppTheme.textFontFamily), Typeface.BOLD)

										setOnClickListener { showMediaOnMap() }
									}
								)
							}
						}
					)
				}
			}

			private fun drawGridView(mediaType: TripDetailViewModel.MediaType): View {
				return GridView(this@TripDetailActivity).apply {
					layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
						setPadding(14.d, 8.d, 14.d, 16.d)
						gravity = Gravity.CENTER
					}
					numColumns = 3
					stretchMode = GridView.STRETCH_COLUMN_WIDTH
					verticalSpacing = 8.d

					adapter = TripDetailMediaGridAdapter(mediaType)
				}
			}
		}

		override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
			val tripPageFrameLayout = NestedScrollView(this@TripDetailActivity).apply {
				id = View.generateViewId()
				layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
			}
			val mediaPageFrameLayout = FrameLayout(this@TripDetailActivity).apply {
				id = View.generateViewId()
				layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
			}

			return when (viewType) {
				0 -> TripPageViewHolder(tripPageFrameLayout)
				1 -> MediaPageViewHolder(mediaPageFrameLayout)
				else -> TripPageViewHolder(tripPageFrameLayout)
			}
		}

		override fun getItemCount(): Int = 2

		override fun getItemViewType(position: Int): Int = position

		override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
			when (position) {
				0 -> (holder as TripPageViewHolder).bind()
				1 -> (holder as MediaPageViewHolder).bind()
				else -> (holder as TripPageViewHolder).bind()
			}
		}
	}

	class MediaClusterItem(val mediaStoreData: TripDetailViewModel.MediaStoreData, private val mTitle: String, private val mSnippet: String) : ClusterItem {
		override fun getPosition(): LatLng = this.mediaStoreData.latLng!!
		override fun getTitle(): String = this.mTitle
		override fun getSnippet(): String = this.mSnippet
	}

	inner class MarkerClusterRenderer : DefaultClusterRenderer<MediaClusterItem>(this, googleMap, mediaClusterManager) {
		override fun shouldRenderAsCluster(cluster: Cluster<MediaClusterItem>): Boolean {
			return cluster.size >= 1
		}

		override fun onBeforeClusterItemRendered(item: MediaClusterItem, markerOptions: MarkerOptions) {
			val clusterDataBinding = DataBindingUtil.inflate<ClusterView1Binding>(layoutInflater, R.layout.cluster_view_1, null, false)
			clusterDataBinding.mapCluster00.setImageURI(item.mediaStoreData.contentUri)

			val view = clusterDataBinding.root
			val specWidth = MeasureSpec.makeMeasureSpec(64.d, MeasureSpec.EXACTLY)
			view.measure(specWidth, specWidth)
			val viewSize: Int = 64.d

			val bitmap = Bitmap.createBitmap(viewSize, viewSize, Bitmap.Config.ARGB_8888)
			val canvas = Canvas(bitmap)
			view.layout(view.left, view.top, view.right, view.bottom)
			view.draw(canvas)

			markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bitmap.toRoundedCorners(12.d.toFloat())!!))
			markerOptions.title(item.title)
		}

		override fun onBeforeClusterRendered(cluster: Cluster<MediaClusterItem>, markerOptions: MarkerOptions) {
			val clusterDataList = cluster.items.toList()
			val view = when (cluster.size) {
				1 -> {
					val clusterDataBinding = DataBindingUtil.inflate<ClusterView1Binding>(layoutInflater, R.layout.cluster_view_1, null, false)
					clusterDataBinding.mapCluster00.setImageURI(clusterDataList[0].mediaStoreData.contentUri)

					clusterDataBinding.root
				}
				2 -> {
					val clusterDataBinding = DataBindingUtil.inflate<ClusterView2Binding>(layoutInflater, R.layout.cluster_view_2, null, false)
					clusterDataBinding.mapCluster00.setImageURI(clusterDataList[0].mediaStoreData.contentUri)
					clusterDataBinding.mapCluster01.setImageURI(clusterDataList[1].mediaStoreData.contentUri)
					clusterDataBinding.root
				}
				3 -> {
					val clusterDataBinding = DataBindingUtil.inflate<ClusterView3Binding>(layoutInflater, R.layout.cluster_view_3, null, false)
					clusterDataBinding.mapCluster00.setImageURI(clusterDataList[0].mediaStoreData.contentUri)
					clusterDataBinding.mapCluster10.setImageURI(clusterDataList[1].mediaStoreData.contentUri)
					clusterDataBinding.mapCluster11.setImageURI(clusterDataList[2].mediaStoreData.contentUri)
					clusterDataBinding.root
				}
				else -> {
					val clusterDataBinding = DataBindingUtil.inflate<ClusterView4Binding>(layoutInflater, R.layout.cluster_view_4, null, false)
					clusterDataBinding.mapCluster00.setImageURI(clusterDataList[0].mediaStoreData.contentUri)
					clusterDataBinding.mapCluster01.setImageURI(clusterDataList[1].mediaStoreData.contentUri)
					clusterDataBinding.mapCluster10.setImageURI(clusterDataList[2].mediaStoreData.contentUri)
					clusterDataBinding.mapCluster11.setImageURI(clusterDataList[3].mediaStoreData.contentUri)
					clusterDataBinding.mapClusterSize.text = cluster.size.toString()
					clusterDataBinding.root
				}
			}

			val specSize = MeasureSpec.makeMeasureSpec(64.d, MeasureSpec.EXACTLY)
			view.measure(specSize, specSize)
			val viewSize: Int = 64.d

			val bitmap = Bitmap.createBitmap(viewSize, viewSize, Bitmap.Config.ARGB_8888)
			val canvas = Canvas(bitmap)
			view.layout(view.left, view.top, view.right, view.bottom)
			view.draw(canvas)

			markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bitmap.toRoundedCorners(12.d.toFloat())!!))
		}
	}

	private fun getBitmap(drawableRes: Int, tint: Int = localAppTheme.colorPrimary): Bitmap? {
		val drawable = ResourcesCompat.getDrawable(resources, drawableRes, null)!!
		drawable.setTint(tint)
		val canvas = Canvas()
		val bitmap = Bitmap.createBitmap(32.d, 32.d, Bitmap.Config.ARGB_8888)
		canvas.setBitmap(bitmap)
		drawable.setBounds(0, 0, 32.d, 32.d)
		drawable.draw(canvas)
		return bitmap
	}

	private fun getActivityMarker(activityId: Int?, pseudoActivity: Int?): Bitmap? {
		val backgroundDrawable = ResourcesCompat.getDrawable(resources, R.drawable.ic_circle, null)!!
		val activityDrawable: Drawable = if (activityId != null) {
			val resourceId = ActivityId.ACTIVITY_ID_RES_MAP[ActivityId.Companion.ACTIVITY.values()[activityId]]!!
			ResourcesCompat.getDrawable(resources, resourceId, null)!!
		} else {
			val resourceId = ActivityId.PSEUDO_ACTIVITY_RES_MAP[pseudoActivity] ?: ActivityId.PSEUDO_ACTIVITY_RES_MAP[-1]!!
			ResourcesCompat.getDrawable(resources, resourceId, null)!!
		}

		if (Constant.MAP_STYLE_DARK[mapStyle]!!) {
			backgroundDrawable.setTint(Color.WHITE)
			activityDrawable.setTint(Color.BLACK)
		} else {
			backgroundDrawable.setTint(Color.BLACK)
			activityDrawable.setTint(Color.WHITE)
		}

		val canvas = Canvas()
		val bitmap = Bitmap.createBitmap(32.d, 32.d, Bitmap.Config.ARGB_8888)
		canvas.setBitmap(bitmap)

		backgroundDrawable.setBounds(0, 0, canvas.width, canvas.height)
		backgroundDrawable.draw(canvas)
		activityDrawable.setBounds(6.d, 6.d, canvas.width - 6.d, canvas.height - 6.d)
		activityDrawable.draw(canvas)

		return bitmap
	}

	private val registerForCreateFileActivityResult = registerForActivityResult(Utility.CreateSpecificTypeDocument("application/gpx+xml")) { outputFileUri ->
		if (outputFileUri!=null) {
			val gpx = Utility.generateGpxObject(viewModel.primaryKey)
			GPX.write(gpx, contentResolver.openOutputStream(outputFileUri)!!)
			Toast.makeText(this, "File exported as ${viewModel.trip.title}.gpx", Toast.LENGTH_SHORT).show()
		}
	}

	companion object {
		const val TAG = "TRIP_DETAIL_ACTIVITY"
	}
}
