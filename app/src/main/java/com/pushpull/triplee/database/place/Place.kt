package com.pushpull.triplee.database.place

import androidx.room.*
import com.fasterxml.jackson.annotation.JsonProperty
import com.pushpull.triplee.database.trip.Trip

@Entity(tableName = "place_table")
data class Place(
	@ColumnInfo(name = "latitude")
	@JsonProperty
	val latitude: Double,

	@ColumnInfo(name = "longitude")
	@JsonProperty
	val longitude: Double,

	@ColumnInfo(name = "name")
	@JsonProperty
	var name: String?,
) {
	@PrimaryKey(autoGenerate = true)
	@ColumnInfo(name = "primary_key")
	@JsonProperty
	var primaryKey: Long = 0L

	@ColumnInfo(name = "modified_timestamp")
	@JsonProperty
	var modifiedTimestamp: Long = primaryKey

	@ColumnInfo(name = "address")
	@JsonProperty
	var address: String? = null

	@ColumnInfo(name = "color_tag")
	@JsonProperty
	var colorTag: Int = 0

	@ColumnInfo(name = "is_favourite")
	@JsonProperty
	var isFavourite: Boolean = false

	@ColumnInfo(name = "is_deleted")
	@JsonProperty
	var isDeleted: Boolean = false

	@ColumnInfo(name = "g_drive_file_id")
	@JsonProperty
	var gDriveFileId: String? = null

	@ColumnInfo(name = "hash")
	@JsonProperty
	var hash: Long? = null

	override fun equals(other: Any?): Boolean {
		return if (other is Trip) {
			this.primaryKey == other.primaryKey
		} else {
			false
		}
	}

	override fun hashCode(): Int {
		var result = primaryKey.hashCode()
		result = 31 * result + modifiedTimestamp.hashCode()
		result = 31 * result + isFavourite.hashCode()
		result = 31 * result + (gDriveFileId?.hashCode() ?: 0)
		result = 31 * result + (hash?.hashCode() ?: 0)
		return result
	}
}
