package com.pushpull.triplee.database.trip

import androidx.room.*
import kotlinx.coroutines.flow.Flow


@Dao
interface TripTableDao {
	//    !!!   TODO    Is conflict strategy correct
	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun insert(trip: Trip)

	@Update
	fun update(trip: Trip)

	@Query(value = "SELECT * FROM trip_table WHERE primary_key = :key")
	fun get(key: Long): Trip?

	@Query(value = "SELECT * FROM trip_table ORDER BY primary_key DESC")
	fun getAll() : List<Trip>

	@Query(value = "SELECT * FROM trip_table where is_deleted = 0 ORDER BY primary_key DESC")
	fun getAllAsFlow() : Flow<List<Trip>>

	@Query(value = "SELECT primary_key FROM trip_table ORDER BY primary_key DESC")
	fun getAllKeys() : List<Long>

	@Delete
	fun delete(trip: Trip)

	@Delete
	fun delete(tripList: List<Trip>)

	@Query(value = "DELETE FROM trip_table")
	fun deleteAll()
}
