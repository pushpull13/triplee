package com.pushpull.triplee.database.trip

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty

@Entity(tableName = "trip_table")
data class Trip(
	@PrimaryKey(autoGenerate = false)
	@ColumnInfo(name = "primary_key")
	@JsonProperty
	val primaryKey: Long,
	@ColumnInfo(name = "timezoneOffset")
	@JsonProperty
	val timezoneOffset: Int,
) {
	@ColumnInfo(name = "modified_timestamp")
	@JsonProperty
	var modifiedTimestamp: Long = primaryKey

	@ColumnInfo(name = "title")
	@JsonProperty
	var title: String? = null

	@ColumnInfo(name = "end_time")
	@JsonProperty
	var endTime: Long? = null

	@ColumnInfo(name = "activity_list")
	@JsonProperty
	var activityList: String = ""

	@ColumnInfo(name = "distance")
	@JsonProperty
	var distance: Int = -1

	@ColumnInfo(name = "departure_location")
	@JsonProperty
	var departureLocation: String? = null

	@ColumnInfo(name = "destination_location")
	@JsonProperty
	var destinationLocation: String? = null

	@ColumnInfo(name = "is_favourite")
	@JsonProperty
	var isFavourite: Boolean = false

	@ColumnInfo(name = "start_latitude")
	@JsonProperty
	var startLatitude: Double = 91.0

	@ColumnInfo(name = "start_longitude")
	@JsonProperty
	var startLongitude: Double = 181.0

	@ColumnInfo(name = "end_latitude")
	@JsonProperty
	var endLatitude: Double = 91.0

	@ColumnInfo(name = "end_longitude")
	@JsonProperty
	var endLongitude: Double = 181.0

	@ColumnInfo(name = "is_deleted")
	@JsonProperty
	var isDeleted: Boolean = false

	@ColumnInfo(name = "is_active")
	@JsonIgnore
	var isActive: Boolean = false

	@ColumnInfo(name = "file_version")
	@JsonIgnore
	var fileVersion: Long = -1

	@ColumnInfo(name = "file_id")
	@JsonIgnore
	var fileId: String? = null

	override fun equals(other: Any?): Boolean {
		return if (other is Trip) {
			this.primaryKey == other.primaryKey
		} else {
			false
		}
	}

	override fun hashCode(): Int {
		var result = primaryKey.hashCode()
		result = 31 * result + timezoneOffset
		result = 31 * result + modifiedTimestamp.hashCode()
		result = 31 * result + title.hashCode()
		result = 31 * result + (endTime?.hashCode() ?: 0)
		result = 31 * result + activityList.hashCode()
		result = 31 * result + distance
		result = 31 * result + (departureLocation?.hashCode() ?: 0)
		result = 31 * result + (destinationLocation?.hashCode() ?: 0)
		result = 31 * result + isFavourite.hashCode()
		result = 31 * result + startLatitude.hashCode()
		result = 31 * result + startLongitude.hashCode()
		result = 31 * result + endLatitude.hashCode()
		result = 31 * result + endLongitude.hashCode()
		return result
	}
}
