package com.pushpull.triplee.database.place

import androidx.room.*
import kotlinx.coroutines.flow.Flow


@Dao
interface PlaceTableDao {
	//    !!!   TODO    Is conflict strategy correct
	@Insert(onConflict = OnConflictStrategy.REPLACE)
	suspend fun insert(place: Place)

	@Update
	fun update(place: Place)

	@Query(value = "SELECT * FROM place_table WHERE primary_key = :key")
	fun get(key: Long): Place?

	@Query(value = "SELECT * FROM place_table ORDER BY primary_key DESC")
	fun getAllAsFlow() : Flow<List<Place>>

	@Query(value = "SELECT * FROM place_table ORDER BY primary_key DESC")
	fun getAll() : List<Place>

	@Query(value = "SELECT primary_key FROM place_table ORDER BY primary_key DESC")
	fun getAllKeys() : List<Long>

	@Query(value = "UPDATE place_table SET g_drive_file_id = :gDriveFileId WHERE primary_key = :key")
	fun addGDriveFileId(key: Long, gDriveFileId: String)

	@Delete
	fun delete(place: Place)

	@Delete
	fun delete(placeList: List<Place>)

	@Query(value = "DELETE FROM place_table")
	fun deleteAll()
}
