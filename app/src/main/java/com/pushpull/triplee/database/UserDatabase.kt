package com.pushpull.triplee.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.pushpull.triplee.BuildConfig
import com.pushpull.triplee.database.place.Place
import com.pushpull.triplee.database.place.PlaceTableDao
import com.pushpull.triplee.database.trip.Trip
import com.pushpull.triplee.database.trip.TripTableDao
import javax.inject.Singleton


@Singleton
@Database(entities = [Trip::class, Place::class], version = BuildConfig.VERSION_CODE, exportSchema = false)
abstract class UserDatabase : RoomDatabase() {

	abstract val tripTableDao: TripTableDao
	abstract val placeTableDao: PlaceTableDao

	companion object {
		@Volatile
		private var INSTANCE: UserDatabase? = null

		fun getInstance(context: Context): UserDatabase {
			synchronized(lock = this) {
				var instance = INSTANCE
				if (instance == null) {
					instance = Room.databaseBuilder(context, UserDatabase::class.java, "user_database")
//                            !!!   Will destruct and reconstruct database when version changes
						.fallbackToDestructiveMigration()
						.build()
					INSTANCE = instance
				}
				return instance
			}
		}
	}
}
