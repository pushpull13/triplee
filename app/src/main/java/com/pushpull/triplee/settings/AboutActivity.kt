package com.pushpull.triplee.settings

import android.net.Uri
import android.os.Bundle
import androidx.browser.customtabs.CustomTabColorSchemeParams
import androidx.browser.customtabs.CustomTabsIntent
import androidx.databinding.DataBindingUtil
import com.pushpull.triplee.R
import com.pushpull.triplee.databinding.SettingsActivityAboutBinding

class AboutActivity: SettingsBaseActivity() {

	private lateinit var dataBinding: SettingsActivityAboutBinding

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		dataBinding = DataBindingUtil.setContentView(this, R.layout.settings_activity_about)
		dataBinding.settingsActivityBack.setOnClickListener { finish() }

		initUi()
	}

	private fun initUi() {
		dataBinding.settingsActivityLayout.addView(SettingsButtonView(context = this, title = "Share Triplee", icon = R.drawable.ic_settings) {  })
		dataBinding.settingsActivityLayout.addView(SettingsButtonView(context = this, title = "Privacy Policy", icon = R.drawable.ic_settings) {
			val url = "https://gitlab.com/pushpull13/triplee"

			val defaultColors = CustomTabColorSchemeParams.Builder()
				.setToolbarColor(localAppTheme.colorPrimary)
				.build()

			val customTab = CustomTabsIntent
				.Builder()
				.setDefaultColorSchemeParams(defaultColors)
				.build()
			customTab.launchUrl(this, Uri.parse(url))
		})
		dataBinding.settingsActivityLayout.addView(SettingsButtonView(context = this, title = "Terms of Service", icon = R.drawable.ic_settings) {
			val url = "https://gitlab.com/pushpull13/triplee"

			val defaultColors = CustomTabColorSchemeParams.Builder()
				.setToolbarColor(localAppTheme.colorPrimary)
				.build()

			val customTab = CustomTabsIntent
				.Builder()
				.setDefaultColorSchemeParams(defaultColors)
				.build()
			customTab.launchUrl(this, Uri.parse(url))

		})
	}

	companion object {
		const val TAG = "ABOUT_ACTIVITY"
	}
}
