package com.pushpull.triplee.settings

import android.animation.LayoutTransition
import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.TypedValue
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.google.android.flexbox.AlignItems
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayout
import com.google.android.flexbox.JustifyContent
import com.pushpull.triplee.R
import com.pushpull.triplee.custom.getLocalTheme
import com.pushpull.triplee.dataClass.AppTheme


class SettingsButtonView : FlexboxLayout {

	private lateinit var title: String
	private var subTitle: String? = null
	private var icon: Int? = null
	private var keepMarginOnTop: Boolean = false
	private var isChecked: Boolean? = null
	private lateinit var onClick: () -> Unit

	constructor(
		context: Context,
		title: String,
		subTitle: String? = null,
		icon: Int? = null,
		keepMarginOnTop: Boolean = false,
		isChecked: Boolean? = null,
		onClick: () -> Unit,
	) : super(context) {
		this.title = title
		this.subTitle = subTitle
		this.icon = icon
		this.keepMarginOnTop = keepMarginOnTop
		this.isChecked = isChecked
		this.onClick = onClick

		initUI()
	}

	constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet)

	private lateinit var localAppTheme: AppTheme

	private var density: Float = context.resources.displayMetrics.density
	private var aspectRatio: Float = context.resources.displayMetrics.heightPixels.toFloat() / context.resources.displayMetrics.widthPixels.toFloat()
	private var widthDp: Int = context.resources.configuration.screenWidthDp
	private var heightDp: Int = context.resources.configuration.screenHeightDp

	private val viewWidth: Int = (context.resources.configuration.screenWidthDp * 3 / 4).d
	private val viewHeight: Int = (context.resources.configuration.screenHeightDp * 3 / 4).d

	init {
		localAppTheme = getLocalTheme()
	}

	private fun initUI() {
		layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT).apply {
			setPadding(48.d, 0, 24.d, 0)
			if (keepMarginOnTop) {
				setMargins(4.d, 16.d, 4.d, 4.d)
			} else {
				setMargins(4.d, 4.d, 4.d, 4.d)
			}
		}

		setBackgroundResource(localAppTheme.roundedCornerRipple)
		isClickable = true
		isFocusable = true

		if (isChecked == null) {
			flexDirection = FlexDirection.COLUMN
			justifyContent = JustifyContent.CENTER

			addView(
				TextView(context).apply { text = title }.apply {
					setTextColor(localAppTheme.colorOnBackground)
					setTextSize(TypedValue.COMPLEX_UNIT_SP, 16F)
					setTypeface(ResourcesCompat.getFont(context, localAppTheme.textFontFamily), Typeface.BOLD)
				}
			)
			if (subTitle != null) {
				addView(
					TextView(context).apply {
						text = subTitle
						setTextColor(localAppTheme.colorPrimary)
						setTextSize(TypedValue.COMPLEX_UNIT_SP, 12F)
						setTypeface(ResourcesCompat.getFont(context, localAppTheme.textFontFamily), Typeface.BOLD)
					}
				)
			} else {
				layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, 56.d).apply {
					setPadding(48.d, 0, 24.d, 0)
					setMargins(4.d, 4.d, 4.d, 0)
				}
			}
		} else {
			flexDirection = FlexDirection.ROW
			justifyContent = JustifyContent.SPACE_BETWEEN
			alignItems = AlignItems.CENTER

			val innerFlexboxLayout = FlexboxLayout(context).apply {
				layoutParams = if (subTitle == null) {
					LayoutParams(0, 64.d)
				} else {
					LayoutParams(0, LayoutParams.WRAP_CONTENT)
				}.apply {
					flexGrow = 1F
				}
				flexDirection = FlexDirection.COLUMN
				justifyContent = JustifyContent.CENTER
			}

			innerFlexboxLayout.addView(
				TextView(context).apply { text = title }.apply {
					setTextColor(localAppTheme.colorOnBackground)
					setTextSize(TypedValue.COMPLEX_UNIT_SP, 16F)
					setTypeface(ResourcesCompat.getFont(context, localAppTheme.textFontFamily), Typeface.BOLD)
				}
			)
			if (subTitle != null) {
				innerFlexboxLayout.addView(
					TextView(context).apply {
						text = subTitle
						setTextColor(localAppTheme.colorPrimary)
						setTextSize(TypedValue.COMPLEX_UNIT_SP, 12F)
						setTypeface(ResourcesCompat.getFont(context, localAppTheme.textFontFamily), Typeface.BOLD)
						layoutTransition = LayoutTransition().apply {
							enableTransitionType(LayoutTransition.CHANGING)
						}
					}
				)
			} else {
				layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, 64.d).apply {
					setPadding(32.d, 8.d, 24.d, 8.d)
					if (keepMarginOnTop) {
						setMargins(4.d, 12.d, 4.d, 4.d)
					} else {
						setMargins(4.d, 4.d, 4.d, 4.d)
					}
				}
			}
			addView(innerFlexboxLayout)
			if (isChecked == true) {
				addView(ImageView(context).apply {
					layoutParams = LayoutParams(24.d, 24.d).apply {
						setMargins(16.d, 0.d, 8.d, 0.d)
					}
					setImageResource(R.drawable.ic_check)
					setColorFilter(localAppTheme.colorOnBackground)
				})
			} else {
				addView(ImageView(context).apply {
					layoutParams = LayoutParams(24.d, 24.d).apply {
						setMargins(16.d, 0.d, 8.d, 0.d)
					}
				})
			}
		}

		setOnClickListener { onClick() }
	}

	val Int.d
		get() = (this * density).toInt()

	companion object {
		const val TAG = "SETTINGS_BUTTON"
	}
}
