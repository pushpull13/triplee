package com.pushpull.triplee.settings

import android.graphics.Typeface
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.pushpull.triplee.R
import com.pushpull.triplee.databinding.SettingsActivityBinding
import com.pushpull.triplee.databinding.SettingsActivityBottomSheetBinding
import com.pushpull.triplee.konstant.Constant
import com.pushpull.triplee.konstant.Constant.Companion.Settings


class UnitsActivity : SettingsBaseActivity() {

	private lateinit var dataBinding: SettingsActivityBinding

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		dataBinding = DataBindingUtil.setContentView(this, R.layout.settings_activity)
		dataBinding.settingsActivityBack.setOnClickListener { finish() }
	}

	override fun onStart() {
		super.onStart()
		populateUI()
	}

	private fun populateUI() {
		dataBinding.settingsActivityLayout.removeAllViews()

		val distanceValueRaw = sharedPreferencesAppConfig.getInt(Settings.DISTANCE.name, Settings.KILOMETRE.ordinal)
		val distanceValue = getString(Constant.SETTINGS_NAME_MAP[Settings.values().filter { it.ordinal == distanceValueRaw }[0]]!!)
		dataBinding.settingsActivityLayout.addView(SettingsButtonView(context = this,
			title = getString(R.string.distance),
			subTitle = distanceValue,
			keepMarginOnTop = true) {
			showDistanceBottomSheet()
		})

		val speedValueRaw = sharedPreferencesAppConfig.getInt(Settings.SPEED.name, Settings.KILOMETRE_PER_HOUR.ordinal)
		val speedValue = getString(Constant.SETTINGS_NAME_MAP[Settings.values().filter { it.ordinal == speedValueRaw }[0]]!!)
		dataBinding.settingsActivityLayout.addView(SettingsButtonView(context = this,
			title = getString(R.string.speed),
			subTitle = speedValue) {
			showSpeedBottomSheet()
		})

		val coordinateValueRaw = sharedPreferencesAppConfig.getInt(Settings.COORDINATE.name, Settings.DD.ordinal)
		val coordinateValue = getString(Constant.SETTINGS_NAME_MAP[Settings.values().filter { it.ordinal == coordinateValueRaw }[0]]!!)
		dataBinding.settingsActivityLayout.addView(SettingsButtonView(context = this,
			title = getString(R.string.coordinate),
			subTitle = coordinateValue) {
			showCoordinateBottomSheet()
		})
	}

	private fun showDistanceBottomSheet() {
		val bottomSheet = BottomSheetDialog(this)
		val bottomSheetDataBinding: SettingsActivityBottomSheetBinding =
			DataBindingUtil.inflate(bottomSheet.layoutInflater, R.layout.settings_activity_bottom_sheet, null, false)

		bottomSheet.setContentView(bottomSheetDataBinding.root)

		val distanceValue = sharedPreferencesAppConfig.getInt(Settings.DISTANCE.name, Settings.KILOMETRE.ordinal)

		bottomSheetDataBinding.settingsActivityBottomSheetLayout.addView(
			TextView(this).apply {
				layoutParams = ViewGroup.LayoutParams(-1, TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 64F, resources.displayMetrics).toInt())
				text = getString(R.string.distance)
				gravity = Gravity.CENTER
				setTextSize(TypedValue.COMPLEX_UNIT_SP, 16F)
				setTypeface(ResourcesCompat.getFont(context, localAppTheme.textFontFamily), Typeface.BOLD)
				setTextColor(localAppTheme.colorOnSurface)
			}
		)

		bottomSheetDataBinding.settingsActivityBottomSheetLayout.addView(
			SettingsButtonView(context = this,
				title = getString(R.string.metre),
				subTitle = "m",
				isChecked = distanceValue == Settings.METRE.ordinal) {
				savePreference(Settings.DISTANCE.name, Settings.METRE.ordinal)
				bottomSheet.dismiss()
			}
		)
		bottomSheetDataBinding.settingsActivityBottomSheetLayout.addView(
			SettingsButtonView(context = this,
				title = getString(R.string.kiloMetre),
				subTitle = "Km",
				isChecked = distanceValue == Settings.KILOMETRE.ordinal) {
				savePreference(Settings.DISTANCE.name, Settings.KILOMETRE.ordinal)
				bottomSheet.dismiss()
			}
		)
		bottomSheetDataBinding.settingsActivityBottomSheetLayout.addView(
			SettingsButtonView(context = this,
				title = getString(R.string.feet),
				subTitle = "ft",
				isChecked = distanceValue == Settings.FEET.ordinal) {
				savePreference(Settings.DISTANCE.name, Settings.FEET.ordinal)
				bottomSheet.dismiss()
			}
		)
		bottomSheetDataBinding.settingsActivityBottomSheetLayout.addView(
			SettingsButtonView(context = this,
				title = getString(R.string.mile),
				subTitle = "mi",
				isChecked = distanceValue == Settings.MILE.ordinal) {
				savePreference(Settings.DISTANCE.name, Settings.MILE.ordinal)
				bottomSheet.dismiss()
			}
		)

		bottomSheet.setOnDismissListener {
			populateUI()
		}

		bottomSheet.show()
	}

	private fun showSpeedBottomSheet() {
		val bottomSheet = BottomSheetDialog(this)
		val bottomSheetDataBinding: SettingsActivityBottomSheetBinding =
			DataBindingUtil.inflate(bottomSheet.layoutInflater, R.layout.settings_activity_bottom_sheet, null, false)

		bottomSheet.setContentView(bottomSheetDataBinding.root)

		val speedValue = sharedPreferencesAppConfig.getInt(Settings.SPEED.name, Settings.KILOMETRE_PER_HOUR.ordinal)

		bottomSheetDataBinding.settingsActivityBottomSheetLayout.addView(
			TextView(this).apply {
				layoutParams = ViewGroup.LayoutParams(-1, TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 64F, resources.displayMetrics).toInt())
				text = getString(R.string.speed)
				gravity = Gravity.CENTER
				setTextSize(TypedValue.COMPLEX_UNIT_SP, 16F)
				setTypeface(ResourcesCompat.getFont(context, localAppTheme.textFontFamily), Typeface.BOLD)
				setTextColor(localAppTheme.colorOnSurface)
			}
		)

		bottomSheetDataBinding.settingsActivityBottomSheetLayout.addView(
			SettingsButtonView(context = this,
				title = getString(R.string.metrePerSecond),
				subTitle ="m/s",
				isChecked = speedValue == Settings.METRE_PER_SECOND.ordinal) {
				savePreference(Settings.SPEED.name, Settings.METRE_PER_SECOND.ordinal)
				bottomSheet.dismiss()
			}
		)
		bottomSheetDataBinding.settingsActivityBottomSheetLayout.addView(
			SettingsButtonView(context = this,
				title = getString(R.string.kilometrePerHour),
				subTitle = "Km/hr",
				isChecked = speedValue == Settings.KILOMETRE_PER_HOUR.ordinal) {
				savePreference(Settings.SPEED.name, Settings.KILOMETRE_PER_HOUR.ordinal)
				bottomSheet.dismiss()
			}
		)
		bottomSheetDataBinding.settingsActivityBottomSheetLayout.addView(
			SettingsButtonView(context = this,
				title = getString(R.string.feetPerSecond),
				subTitle = "ft/s",
				isChecked = speedValue == Settings.FEET_PER_SECOND.ordinal) {
				savePreference(Settings.SPEED.name, Settings.FEET_PER_SECOND.ordinal)
				bottomSheet.dismiss()
			}
		)
		bottomSheetDataBinding.settingsActivityBottomSheetLayout.addView(
			SettingsButtonView(context = this,
				title = getString(R.string.milePerHour),
				subTitle ="mi/hr",
				isChecked = speedValue == Settings.MILE_PER_HOUR.ordinal) {
				savePreference(Settings.SPEED.name, Settings.MILE_PER_HOUR.ordinal)
				bottomSheet.dismiss()
			}
		)

		bottomSheet.setOnDismissListener {
			populateUI()
		}

		bottomSheet.show()
	}

	private fun showCoordinateBottomSheet() {
		val bottomSheet = BottomSheetDialog(this)
		val bottomSheetDataBinding: SettingsActivityBottomSheetBinding =
			DataBindingUtil.inflate(bottomSheet.layoutInflater, R.layout.settings_activity_bottom_sheet, null, false)

		bottomSheet.setContentView(bottomSheetDataBinding.root)

		val coordinateValue = sharedPreferencesAppConfig.getInt(Settings.COORDINATE.name, Settings.DD.ordinal)

		bottomSheetDataBinding.settingsActivityBottomSheetLayout.addView(
			TextView(this).apply {
				layoutParams = ViewGroup.LayoutParams(-1, TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 64F, resources.displayMetrics).toInt())
				text = getString(R.string.coordinate)
				gravity = Gravity.CENTER
				setTextSize(TypedValue.COMPLEX_UNIT_SP, 16F)
				setTypeface(ResourcesCompat.getFont(context, localAppTheme.textFontFamily), Typeface.BOLD)
				setTextColor(localAppTheme.colorOnSurface)
			}
		)

		bottomSheetDataBinding.settingsActivityBottomSheetLayout.addView(
			SettingsButtonView(context = this,
				title = getString(R.string.decimalDegrees),
				subTitle ="DD",
				isChecked = coordinateValue == Settings.DD.ordinal) {
				savePreference(Settings.COORDINATE.name, Settings.DD.ordinal)
				bottomSheet.dismiss()
			}
		)
		bottomSheetDataBinding.settingsActivityBottomSheetLayout.addView(
			SettingsButtonView(context = this,
				title = getString(R.string.degreeDecimalMinutes),
				subTitle = "DM",
				isChecked = coordinateValue == Settings.DM.ordinal) {
				savePreference(Settings.COORDINATE.name, Settings.DM.ordinal)
				bottomSheet.dismiss()
			}
		)
		bottomSheetDataBinding.settingsActivityBottomSheetLayout.addView(
			SettingsButtonView(context = this,
				title = getString(R.string.degreeMinutesSeconds),
				subTitle = "DMS",
				isChecked = coordinateValue == Settings.DMS.ordinal) {
				savePreference(Settings.COORDINATE.name, Settings.DMS.ordinal)
				bottomSheet.dismiss()
			}
		)

		bottomSheet.setOnDismissListener {
			populateUI()
		}

		bottomSheet.show()
	}


	companion object {
		const val TAG = "UNITS_ACTIVITY"
	}
}
