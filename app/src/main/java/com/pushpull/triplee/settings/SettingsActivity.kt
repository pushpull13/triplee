package com.pushpull.triplee.settings

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.PowerManager
import android.provider.Settings
import android.view.View
import androidx.core.net.toUri
import androidx.databinding.DataBindingUtil
import com.google.android.gms.oss.licenses.OssLicensesMenuActivity
import com.pushpull.triplee.R
import com.pushpull.triplee.cloud.CloudActivity
import com.pushpull.triplee.custom.Divider
import com.pushpull.triplee.databinding.SettingsActivityBinding
import com.pushpull.triplee.konstant.Constant
import com.pushpull.triplee.konstant.Path
import java.io.File


class SettingsActivity : SettingsBaseActivity() {

	private lateinit var dataBinding: SettingsActivityBinding

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		dataBinding = DataBindingUtil.setContentView(this, R.layout.settings_activity)
		dataBinding.settingsActivityBack.setOnClickListener { finish() }

	}

	override fun onStart() {
		super.onStart()
		dataBinding.settingsActivityProfile.visibility = View.VISIBLE
		populateUI()
	}

	private fun populateUI() {
		dataBinding.settingsActivityLayout.removeAllViews()

		dataBinding.settingsActivityLayout.addView(Divider(this))
		dataBinding.settingsActivityLayout.addView(
			SettingsButtonView(context = this, title = "Personalization", icon = R.drawable.ic_settings) { openSettings(PersonalizationActivity::class.java) })
		dataBinding.settingsActivityLayout.addView(
			SettingsButtonView(context = this, title = "Improve Stability", icon = R.drawable.ic_settings) {
				val powerManager = getSystemService(POWER_SERVICE) as PowerManager
				val message = if (powerManager.isIgnoringBatteryOptimizations(packageName)) {
					BATTERY_OPTIMIZATION_RATIONAL_MESSAGE_TRUE
				} else {
					BATTERY_OPTIMIZATION_RATIONAL_MESSAGE_FALSE
				}
				showRational(showImGood = true, message = message, R.drawable.sticker_battery, {}) {
					Intent().apply {
						action = Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS
						startActivity(this)
					}
				}
			})

		dataBinding.settingsActivityLayout.addView(Divider(this))
		dataBinding.settingsActivityLayout.addView(
			SettingsButtonView(context = this, title = "Rate us", icon = R.drawable.ic_settings) {
				startActivity(Intent(Intent.ACTION_VIEW,
					Uri.parse("market://details?id=${applicationInfo.loadLabel(packageManager)}")))
			})
		dataBinding.settingsActivityLayout.addView(SettingsButtonView(context = this, title = "Open source licenses", icon = R.drawable.ic_settings) {
			OssLicensesMenuActivity.setActivityTitle("Open Source Licenses")
			Intent(this, OssLicensesMenuActivity::class.java).apply {
				startActivity(this)
			}
		})
		dataBinding.settingsActivityLayout.addView(
			SettingsButtonView(context = this, title = "About us", icon = R.drawable.ic_settings) { openSettings(AboutActivity::class.java) })
	}

	companion object {
		const val TAG = "SETTING_ACTIVITY"
		private const val BATTERY_OPTIMIZATION_RATIONAL_MESSAGE_FALSE = "Allow Triplee to turn off battery optimization."
		private const val BATTERY_OPTIMIZATION_RATIONAL_MESSAGE_TRUE = "Battery optimization is already turned off."
	}
}
