package com.pushpull.triplee.settings

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.text.Html
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.databinding.DataBindingUtil
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.pushpull.triplee.R
import com.pushpull.triplee.custom.BaseActivity
import com.pushpull.triplee.databinding.PermissionRationaleBinding
import com.pushpull.triplee.konstant.Constant.Companion.Settings


abstract class SettingsBaseActivity : BaseActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		sharedPreferencesAppConfig = getSharedPreferences(Settings.APP_CONFIG.name, MODE_PRIVATE)

		initUI()
	}

	fun showRational(showImGood: Boolean, message: String, iconId: Int, onIMGood: () -> Unit, onContinue: () -> Unit) {
		val bottomSheet = BottomSheetDialog(this)
		val dataBinding: PermissionRationaleBinding =
			DataBindingUtil.inflate(bottomSheet.layoutInflater, R.layout.permission_rationale, null, false)
		bottomSheet.setContentView(dataBinding.root)

		dataBinding.permissionRationaleCancelButton.setOnClickListener { bottomSheet.dismiss() }

		dataBinding.permissionRationaleMessage.text = Html.fromHtml(message, Html.TO_HTML_PARAGRAPH_LINES_INDIVIDUAL)
		dataBinding.permissionRationaleIcon.setImageDrawable(ResourcesCompat.getDrawable(resources, iconId, null))

		dataBinding.permissionRationaleButtonContinue.setOnClickListener {
			bottomSheet.dismiss()
			onContinue()
		}

		if (showImGood) {
			dataBinding.permissionRationaleButtonIMGood.visibility = View.VISIBLE
			dataBinding.permissionRationaleButtonIMGood.setOnClickListener {
				onIMGood()
				bottomSheet.dismiss()
			}
		} else {
			dataBinding.permissionRationaleButtonIMGood.visibility = View.INVISIBLE
		}

		bottomSheet.show()
	}

	private fun initUI() {
		val decorView = window.decorView
		WindowInsetsControllerCompat(window, decorView).apply {
			when (resources?.configuration?.uiMode?.and(Configuration.UI_MODE_NIGHT_MASK)) {
				Configuration.UI_MODE_NIGHT_YES -> {
					isAppearanceLightStatusBars = false
					isAppearanceLightNavigationBars = true
				}
				Configuration.UI_MODE_NIGHT_NO -> {
					isAppearanceLightStatusBars = true
					isAppearanceLightNavigationBars = true
				}
			}
		}

		window.statusBarColor = localAppTheme.colorBeta
		window.navigationBarColor = localAppTheme.colorBeta
	}

	fun <T> openSettings(javaClass: Class<T>) {
		Intent(this, javaClass).apply {
			startActivity(this)
		}
	}

	companion object {
		const val TAG = "SETTINGS_BASE_ACTIVITY"
	}
}
