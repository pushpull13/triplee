package com.pushpull.triplee.settings

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.pushpull.triplee.R
import com.pushpull.triplee.databinding.SettingsActivityBinding
import com.pushpull.triplee.konstant.Constant


class PersonalizationActivity : SettingsBaseActivity() {

	private lateinit var dataBinding: SettingsActivityBinding

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		dataBinding = DataBindingUtil.setContentView(this, R.layout.settings_activity)
		dataBinding.settingsActivityBack.setOnClickListener { finish() }
	}

	override fun onStart() {
		super.onStart()
		populateUI()
	}

	private fun populateUI() {
		val appTheme = sharedPreferencesAppConfig.getInt(Constant.Companion.Settings.APP_THEME.name, R.style.Theme_Triplee_Aqua)
		val appThemeName = getString(Constant.APP_THEME_NAME_RES_MAP[appTheme]!!)

		dataBinding.settingsActivityLayout.removeAllViews()

		dataBinding.settingsActivityLayout
			.addView(SettingsButtonView(context = this,
				title = "Theme",
				subTitle = appThemeName,
				keepMarginOnTop = true) { openSettings(ThemeActivity::class.java) })
		dataBinding.settingsActivityLayout.addView(SettingsButtonView(context = this, title = "Units") { openSettings(UnitsActivity::class.java) })
	}

	companion object {
		const val TAG = "PERSONALIZATION_ACTIVITY"
	}
}
