package com.pushpull.triplee.settings

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import com.google.android.flexbox.AlignItems
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayout
import com.google.android.flexbox.JustifyContent
import com.pushpull.triplee.R
import com.pushpull.triplee.custom.Phoenix
import com.pushpull.triplee.databinding.SettingsActivityThemeBinding
import com.pushpull.triplee.konstant.Constant
import com.pushpull.triplee.konstant.Constant.Companion.Settings
import kotlin.properties.Delegates


class ThemeActivity : SettingsBaseActivity() {

	private lateinit var dataBinding: SettingsActivityThemeBinding
	private var currentTheme by Delegates.notNull<Int>()
	private var newTheme by Delegates.notNull<Int>()
	private lateinit var frameThemeView: FrameLayout
	private lateinit var themeTitle: TextView

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		dataBinding = DataBindingUtil.setContentView(this, R.layout.settings_activity_theme)

		dataBinding.settingsActivityBack.setOnClickListener {
			if (newTheme != currentTheme) {
				savePreference(Settings.APP_THEME.name, newTheme)
				Phoenix.triggerRebirth(this)
			} else {
				finish()
			}
		}
	}

	override fun onBackPressed() {
		if (newTheme!=currentTheme) {
			savePreference(Settings.APP_THEME.name, newTheme)
			Phoenix.triggerRebirth(this)
		} else {
			super.onBackPressed()
		}
	}

	override fun onStart() {
		super.onStart()

		currentTheme = sharedPreferencesAppConfig.getInt(Settings.APP_THEME.name, R.style.Theme_Triplee_Aqua)
		newTheme = currentTheme

		themeTitle = TextView(this).apply {
			layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT).apply {
				setMargins(0, 8.d, 0, 0)
			}

			setTextColor(getColor(Constant.APP_THEME_COLOR_RES_MAP[newTheme]!!))
			setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
			setTypeface(ResourcesCompat.getFont(context, localAppTheme.textFontFamily), Typeface.BOLD)
			includeFontPadding = false
			textAlignment = View.TEXT_ALIGNMENT_CENTER

			text = getString(Constant.APP_THEME_NAME_RES_MAP[newTheme]!!)
		}

		dataBinding.settingsActivityThemeView.addView(themeTitle)

		frameThemeView = FrameLayout(this).apply {
			layoutParams = FrameLayout.LayoutParams((widthDp * 4 / 7).d, (widthDp * 4 / 7 * aspectRatio).toInt().d).apply {
				gravity = Gravity.CENTER
				setMargins(widthDp.d / 6, 24.d, widthDp.d / 6, 0)
			}

			addView(ThemeView())
		}

		dataBinding.settingsActivityThemeView.addView(frameThemeView)
		dataBinding.settingsActivityThemeView.addView(ThemeGrid())

		updateView()
	}

	private fun updateView() {
		Log.i(TAG, "updateView...")

		frameThemeView.removeAllViews()
		frameThemeView.addView(ThemeView())
		themeTitle.text = getString(Constant.APP_THEME_NAME_RES_MAP[newTheme]!!)
		themeTitle.setTextColor(getColor(Constant.APP_THEME_COLOR_RES_MAP[newTheme]!!))
	}

	inner class ThemeView : CardView(this) {

		private val linearLayout = LinearLayout(context)

		init {
			radius = (heightDp * 0.025).toInt().d.toFloat()
			layoutParams = LayoutParams((widthDp * 4 / 7).d, (widthDp * 4 / 7 * aspectRatio).toInt().d).apply {
				gravity = Gravity.CENTER
				setMargins(widthDp.d / 6, 0, widthDp.d / 6, 0)
			}
			cardElevation = 8.d.toFloat()

			linearLayout.orientation = LinearLayout.VERTICAL

			linearLayout.addView(
				View(context).apply {
					layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, 32.d)
					setBackgroundColor(context.getColor(Constant.APP_THEME_COLOR_RES_MAP[newTheme]!!))
				}
			)

			linearLayout.addView(
				View(context).apply {
					layoutParams = LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0).apply { weight = 1f }
					Log.i(TAG, localAppTheme.colorGamma.toString())
					if (localAppTheme.colorGamma == -14935012) {
						setBackgroundColor(Color.argb(255, 13, 13, 13))
					} else {
						setBackgroundColor(localAppTheme.colorGamma)
					}
				}
			)

			linearLayout.addView(
				FlexboxLayout(context).apply {
					LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
					setBackgroundColor(Color.BLACK)
					justifyContent = JustifyContent.SPACE_AROUND
					addView(bottomBarItem(R.drawable.ic_home, "Home"))
					addView(bottomBarItem(R.drawable.ic_trips, "Trips"))
					addView(bottomBarItem(R.drawable.ic_me, "Me"))
				}
			)

			addView(
				CardView(context).apply {
					layoutParams = LayoutParams(32.d, 72.d).apply {
						setMargins(0, 0, 12.d, 96.d)
						gravity = Gravity.BOTTOM.or(Gravity.END)
					}
					radius = 20.d.toFloat()
					setCardBackgroundColor(localAppTheme.colorGamma)
					isClickable = false
					cardElevation = 16.d.toFloat()

					addView(
						FlexboxLayout(context).apply {
							layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT).apply {
								setMargins(4.d, 0, 4.d, 0)
							}
							flexDirection = FlexDirection.COLUMN
							justifyContent = JustifyContent.CENTER
							alignItems = AlignItems.CENTER

							addView(
								ImageView(context).apply {
									layoutParams = LayoutParams(16.d, 16.d).apply {
										setMargins(0, 0, 0, 6.d)
									}
									setImageResource(R.drawable.activity_trekking)

									val colorGamma = localAppTheme.colorGamma
									if(colorGamma==-14935012) {
										setColorFilter(Color.WHITE)
									} else {
										setColorFilter(context.getColor(Constant.APP_THEME_COLOR_RES_MAP[newTheme]!!))
									}
								}
							)

							addView(
								TextView(context).apply {
									layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT).apply {
										setMargins(0, 6.d, 0, 0)
									}
									val colorGamma = localAppTheme.colorGamma
									if(colorGamma==-14935012) {
										setTextColor(Color.WHITE)
									} else {
										setTextColor(context.getColor(Constant.APP_THEME_COLOR_RES_MAP[newTheme]!!))
									}
									setTextSize(TypedValue.COMPLEX_UNIT_SP, 8F)
									setTypeface(ResourcesCompat.getFont(context, localAppTheme.textFontFamily), Typeface.BOLD)
									includeFontPadding = false
									textAlignment = View.TEXT_ALIGNMENT_CENTER

									text = "Trekking"
								}
							)
						}
					)
				}
			)

			addView(
				CardView(context).apply {
					layoutParams = LayoutParams(32.d, 32.d).apply {
						setMargins(0, 0, 12.d, 56.d)
						gravity = Gravity.BOTTOM.or(Gravity.END)
					}
					radius = 20.d.toFloat()
					setCardBackgroundColor(localAppTheme.colorGamma)
					isClickable = false
					cardElevation = 16.d.toFloat()

					addView(
						ImageView(context).apply {
							layoutParams = LayoutParams(16.d, 16.d).apply {
								gravity = Gravity.CENTER
							}
							setImageResource(R.drawable.ic_gps)
							setColorFilter(localAppTheme.colorOnGamma)
						}
					)
				}
			)

			addView(
				CardView(context).apply {
					layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, 32.d).apply {
						setMargins(0, 0, 0, 56.d)
						gravity = Gravity.BOTTOM.or(Gravity.CENTER)
					}
					setCardBackgroundColor(context.getColor(Constant.APP_THEME_COLOR_RES_MAP[newTheme]!!))
					this.radius = 16.d.toFloat()
					cardElevation = 16.d.toFloat()

					addView(
						LinearLayout(context).apply {
							layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, 32.d).apply {
								setMargins(12.d, 8.d, 12.d, 8.d)
								gravity = Gravity.CENTER
							}
							gravity = Gravity.CENTER

							addView(
								ImageView(context).apply {
									layoutParams = LayoutParams(16.d, 16.d).apply {
										setMargins(0, 0, 4.d, 0)
										gravity = Gravity.CENTER
									}

									setImageResource(R.drawable.ic_record)
									setColorFilter(localAppTheme.colorOnPrimary)
									translationZ = 32F
								}
							)

							addView(
								TextView(context).apply {
									layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, 16.d).apply {
										setMargins(4.d, 0, 0, 0)
										gravity = Gravity.CENTER
									}
									setTextColor(localAppTheme.colorOnPrimary)
									setTextSize(TypedValue.COMPLEX_UNIT_SP, 10F)
									setTypeface(ResourcesCompat.getFont(context, localAppTheme.textFontFamily), Typeface.BOLD)
									includeFontPadding = false

									text = getString(R.string.start).uppercase()
								}
							)
						}
					)
				}
			)

			addView(linearLayout)
		}

		private fun bottomBarItem(iconId: Int, text: String): LinearLayout {
			return LinearLayout(context).apply {
				orientation = LinearLayout.VERTICAL
				layoutParams = LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT).apply { setMargins(0, 8.d, 0, 4.d) }
				addView(
					ImageView(context).apply {
						setImageResource(iconId)
						layoutParams = LayoutParams(16.d, 16.d).apply { setMargins(0, 0, 0, 2.d) }
						setColorFilter(Color.WHITE)
					}
				)
				addView(
					TextView(context).apply {
						this.text = text
						setTextSize(TypedValue.COMPLEX_UNIT_SP, 8f)
						setTextColor(Color.WHITE)
						setTypeface(ResourcesCompat.getFont(context, localAppTheme.textFontFamily), Typeface.BOLD)
						gravity = Gravity.CENTER
					}
				)
			}
		}
	}

	inner class ThemeGrid : LinearLayout(this) {
		init {
			layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT).apply {
				setPadding(0, 16.d, 0, 0)
			}
			orientation = VERTICAL

			val themeList = Constant.APP_THEME_LIST
			for (i in 0..(themeList.size / 4)) {
				addView(
					FlexboxLayout(context).apply {
						layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
						justifyContent = JustifyContent.SPACE_EVENLY

						if (i * 4 < themeList.size) {
							addView(ThemeGridItem(themeList[i * 4]))
						} else {
							addView(ThemeGridItem(null))
						}

						if ((i * 4) + 1 < themeList.size) {
							addView(ThemeGridItem(themeList[(i * 4) + 1]))
						} else {
							addView(ThemeGridItem(null))
						}

						if ((i * 4) + 2 < themeList.size) {
							addView(ThemeGridItem(themeList[(i * 4) + 2]))
						} else {
							addView(ThemeGridItem(null))
						}

						if ((i * 4) + 3 < themeList.size) {
							addView(ThemeGridItem(themeList[(i * 4) + 3]))
						} else {
							addView(ThemeGridItem(null))
						}
					}
				)
			}
		}
	}

	inner class ThemeGridItem(private val appTheme: Int?) : LinearLayout(this) {
		init {
			layoutParams = LayoutParams((widthDp.d * 0.22).toInt(), LayoutParams.WRAP_CONTENT).apply {
				setPadding((widthDp.d * 0.01).toInt(), 8.d, (widthDp.d * 0.01).toInt(), 8.d)
			}

			if (appTheme != null) {
				orientation = VERTICAL
				setBackgroundResource(R.drawable.rounded_corner_ripple)

				setOnClickListener {
					isClickable = true
					isFocusable = true

					if (!Constant.APP_THEME_LOCK_MAP[appTheme]!!) {
						newTheme = appTheme
						updateView()
					} else {
						Toast.makeText(context, "Subscribe to Triplee Plus to unlock this theme", Toast.LENGTH_SHORT).show()
					}
				}

				addView(
					CardView(context).apply {
						layoutParams = LayoutParams(36.d, 36.d).apply {
							gravity = Gravity.CENTER
						}
						gravity = Gravity.CENTER
						radius = 8.d.toFloat()
						cardElevation = 0.d.toFloat()

						setCardBackgroundColor(context.getColor(Constant.APP_THEME_COLOR_RES_MAP[appTheme]!!))

						if (Constant.APP_THEME_LOCK_MAP[appTheme]!!) {
							addView(
								ImageView(context).apply {
									layoutParams = LayoutParams(36.d, 36.d).apply {
										gravity = Gravity.CENTER
										setPadding(8.d, 8.d, 8.d, 8.d)
									}
									scaleType = ImageView.ScaleType.FIT_CENTER

									setImageResource(R.drawable.ic_lock)
								}
							)
						}
					}
				)

				addView(
					TextView(context).apply {
						layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT).apply {
							setMargins(0, 2.d, 0, 0)
							gravity = Gravity.CENTER
						}
						setTextColor(localAppTheme.colorOnSurface)
						setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f)
						setTypeface(ResourcesCompat.getFont(context, localAppTheme.textFontFamily), Typeface.BOLD)
						includeFontPadding = false
						textAlignment = View.TEXT_ALIGNMENT_CENTER

						text = context.getString(Constant.APP_THEME_NAME_RES_MAP[appTheme]!!)
					}
				)
			}
		}
	}

	companion object {
		const val TAG = "THEME_ACTIVITY"
	}
}
