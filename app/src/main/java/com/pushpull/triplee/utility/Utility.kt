package com.pushpull.triplee.utility

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.database.Cursor
import android.graphics.*
import android.net.Uri
import android.provider.MediaStore
import android.provider.Settings
import android.text.format.DateFormat
import android.util.Log
import android.view.View
import com.google.android.gms.maps.GoogleMapOptions
import com.google.android.gms.maps.MapView
import com.pushpull.triplee.konstant.Path
import java.io.*
import java.net.URL
import kotlin.random.Random
import android.graphics.Bitmap
import androidx.activity.result.contract.ActivityResultContracts
import androidx.exifinterface.media.ExifInterface
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.*
import com.pushpull.triplee.alice.Alice
import com.pushpull.triplee.dataClass.TripData
import com.pushpull.triplee.database.trip.Trip
import com.pushpull.triplee.konstant.Constant
import com.pushpull.triplee.konstant.Secret
import io.jenetics.jpx.GPX
import io.jenetics.jpx.Track
import io.jenetics.jpx.TrackSegment
import io.jenetics.jpx.WayPoint
import java.util.concurrent.TimeUnit
import kotlin.math.abs


object Utility {

	fun getRandomString(length: Int = 8): String {
		val characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".toCharArray()
		var randomString = ""
		for (i in 0 until length) {
			randomString += characters[Random.nextInt(61)]
		}
		return randomString
	}

	fun timestampToPretty(timestamp: Long): String {
		return DateFormat.format("dd MMM HH:mm", timestamp).toString()
	}

	fun goToSettings(applicationContext: Context) {
		Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:${applicationContext.packageName}")).apply {
			addCategory(Intent.CATEGORY_DEFAULT)
			addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
		}.also { intent ->
			applicationContext.startActivity(intent)
		}
	}

	fun readDataFile(prefix: Long, suffix: String): File {
		File("${Path.TRIP_FOLDER_NAME}/$prefix").mkdirs()
		return File(Path.TRIP_FILE_XXXXXX_NAME.replace("yyyyyy", "$prefix").replace("xxxxxx", "${prefix}_$suffix"))
	}

	fun <T> writeFileAsEncryptedObject(file: File, classObject: T) {
		val objectMapper = ObjectMapper().registerModule(KotlinModule())
		file.writeText(Alice.encrypt(objectMapper.writeValueAsString(classObject), Secret.PASSWORD)!!)
	}

	inline fun <reified T> readEncryptedFileAsObject(file: File): T {
		val objectMapper = ObjectMapper().registerModule(KotlinModule())

		return objectMapper.readValue<T>(Alice.decrypt(file.readText(), Secret.PASSWORD)!!)
	}

	fun getMapSnapShot(context: Context, trip: Trip) {
		if (Thread.currentThread() == context.mainLooper.thread) {

			val latLngList: MutableList<LatLng> = mutableListOf()
			var minLatitude: Double = Double.MAX_VALUE
			var maxLatitude: Double = -Double.MAX_VALUE
			var minLongitude: Double = Double.MAX_VALUE
			var maxLongitude: Double = -Double.MAX_VALUE

			var pointer: String? = null
			try {
				val baseFile = readDataFile(trip.primaryKey, "0")
				val baseTripData: TripData = readEncryptedFileAsObject(baseFile)
				val baseTripLocationDataList = baseTripData.tripLocationDataMap.toList()
				baseTripLocationDataList.forEach {
					val nextLatLng = LatLng(it.second[0], it.second[1])
					latLngList.add(nextLatLng)
					minLatitude = minOf(minLatitude, nextLatLng.latitude)
					maxLatitude = maxOf(maxLatitude, nextLatLng.latitude)
					minLongitude = minOf(minLongitude, nextLatLng.longitude)
					maxLongitude = maxOf(maxLongitude, nextLatLng.longitude)
				}
				pointer = baseTripData.next
			} catch (exception: Exception) {
			}
			try {
				while (pointer != null) {
					val nextFile = readDataFile(trip.primaryKey, pointer)
					val nextTripData: TripData = readEncryptedFileAsObject(nextFile)
					val nextTripLocationDataList = nextTripData.tripLocationDataMap.toList()
					nextTripLocationDataList.forEach {
						val nextLatLng = LatLng(it.second[0], it.second[1])
						latLngList.add(nextLatLng)
						minLatitude = minOf(minLatitude, nextLatLng.latitude)
						maxLatitude = maxOf(maxLatitude, nextLatLng.latitude)
						minLongitude = minOf(minLongitude, nextLatLng.longitude)
						maxLongitude = maxOf(maxLongitude, nextLatLng.longitude)
					}
					pointer = nextTripData.next
				}
			} catch (exception: Exception) {
			}

			val options = GoogleMapOptions()
				.compassEnabled(false)
				.mapToolbarEnabled(false)
				.liteMode(true)
			val mapView = MapView(context, options)

			mapView.onCreate(null)
			mapView.onResume()
			mapView.getMapAsync { googleMap ->
				mapView.measure(
					View.MeasureSpec.makeMeasureSpec(480, View.MeasureSpec.EXACTLY),
					View.MeasureSpec.makeMeasureSpec(480, View.MeasureSpec.EXACTLY))
				mapView.layout(0, 0, 480, 480)

				googleMap.apply {
					setMapStyle(MapStyleOptions.loadRawResourceStyle(context, Constant.MAP_STYLE_RES_MAP[Constant.Companion.MapStyle.COFFEE]!!))

					if(latLngList.isNotEmpty()) {
						moveCamera(CameraUpdateFactory.newLatLngBounds(LatLngBounds.Builder()
							.include(LatLng(minLatitude, minLongitude))
							.include(LatLng(maxLatitude, maxLongitude))
							.build(), 128))

						addPolyline(PolylineOptions()
							.addAll(latLngList)
							.width(16F)
							.startCap(RoundCap())
							.endCap(RoundCap())
							.color(Color.argb(255, 18, 181, 203))
							.jointType(JointType.ROUND))
					}

					setOnMapLoadedCallback {
						googleMap.snapshot {
							try {
								if (it != null) {
									val thumbnailPath = "${Path.TRIP_FOLDER_NAME}/thumbnail/${trip.primaryKey}.png"
									File("${Path.TRIP_FOLDER_NAME}/thumbnail").mkdirs()
									File(thumbnailPath).createNewFile()
									FileOutputStream(thumbnailPath).use { out ->
										it.compress(Bitmap.CompressFormat.PNG, 100, out)
									}
									Log.i(TAG, "snapshot successful")
								}
							} catch (e: IOException) {
								e.printStackTrace()
								Log.i(TAG, "snapshot unsuccessful")
							}
						}
					}
				}
			}
		}
	}

	class CreateSpecificTypeDocument(private val type: String) : ActivityResultContracts.CreateDocument() {
		override fun createIntent(context: Context, input: String): Intent {
			return super.createIntent(context, input).apply {
				type = this@CreateSpecificTypeDocument.type
			}
		}
	}

	fun generateGpxObject(primaryKey: Long): GPX {
		val gpx = GPX.builder()
		val gpxTrack = Track.builder()

		val baseFile = readDataFile(primaryKey, "0")
		val baseFileData: TripData = readEncryptedFileAsObject(baseFile)
		var gpxTrackSegment = TrackSegment.builder()
		baseFileData.tripLocationDataMap.forEach {
			val wayPoint = WayPoint.builder()
				.time(it.key)
				.lat(it.value[0])
				.lon(it.value[1])
				.ele(it.value[2])
				.speed(it.value[3])
				.course(it.value[4])
				.build()
			gpxTrackSegment.addPoint(wayPoint)
		}
		gpxTrack.addSegment(gpxTrackSegment.build())
		var pointer = baseFileData.next

		while (pointer != null) {
			val nextFile = readDataFile(primaryKey, pointer)
			val nextFileData: TripData = readEncryptedFileAsObject(nextFile)
			gpxTrackSegment = TrackSegment.builder()
			nextFileData.tripLocationDataMap.forEach {
				val wayPoint = WayPoint.builder()
					.time(it.key)
					.lat(it.value[0])
					.lon(it.value[1])
					.ele(it.value[2])
					.speed(it.value[3])
					.course(it.value[4])
					.build()
				gpxTrackSegment.addPoint(wayPoint)
			}
			gpxTrack.addSegment(gpxTrackSegment.build())

			pointer = nextFileData.next
		}

		gpx!!.addTrack(gpxTrack.build())

		return gpx.build()
	}

	fun getThumbnail(context: Context, id: Long): Bitmap? {
		val cursor: Cursor? = context.contentResolver.query(
			MediaStore.Images.Media.EXTERNAL_CONTENT_URI, arrayOf(MediaStore.Images.Media.DATA),  // Which columns
			// to return
			MediaStore.Images.Media._ID + "=?", arrayOf(id.toString()),  // Selection arguments
			null) // order
		return if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst()
			val filePath: String = cursor.getString(0)
			cursor.close()
			var rotation = 0
			try {
				val exifInterface = ExifInterface(filePath)
				val exifRotation: Int = exifInterface.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_UNDEFINED)
				if (exifRotation != ExifInterface.ORIENTATION_UNDEFINED) {
					when (exifRotation) {
						ExifInterface.ORIENTATION_ROTATE_180 -> rotation = 180
						ExifInterface.ORIENTATION_ROTATE_270 -> rotation = 270
						ExifInterface.ORIENTATION_ROTATE_90 -> rotation = 90
					}
				}
			} catch (e: IOException) {
				Log.e("getThumbnail", e.toString())
			}
			var bitmap = MediaStore.Images.Thumbnails.getThumbnail(
				context.contentResolver, id,
				MediaStore.Images.Thumbnails.MINI_KIND, null)
			if (rotation != 0) {
				val matrix = Matrix()
				matrix.setRotate(rotation.toFloat())
				bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width,
					bitmap.height, matrix, true)
			}
			bitmap
		} else null
	}


	enum class DEVICE_CATEGORY {
		NORMAL_PORTRAIT,
		NORMAL_LANDSCAPE,
		LARGE_PORTRAIT,
		LARGE_LANDSCAPE
	}

	fun getDeviceCategory(activity: Activity): DEVICE_CATEGORY {
		val configuration = activity.resources.configuration
		return when (configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK) {
			Configuration.SCREENLAYOUT_SIZE_SMALL -> {
				when (configuration.orientation) {
					Configuration.ORIENTATION_PORTRAIT -> DEVICE_CATEGORY.NORMAL_PORTRAIT
					Configuration.ORIENTATION_LANDSCAPE -> DEVICE_CATEGORY.NORMAL_LANDSCAPE
					Configuration.ORIENTATION_UNDEFINED -> DEVICE_CATEGORY.NORMAL_PORTRAIT
					else -> DEVICE_CATEGORY.NORMAL_PORTRAIT
				}
			}
			Configuration.SCREENLAYOUT_SIZE_NORMAL -> {
				when (configuration.orientation) {
					Configuration.ORIENTATION_PORTRAIT -> DEVICE_CATEGORY.NORMAL_PORTRAIT
					Configuration.ORIENTATION_LANDSCAPE -> DEVICE_CATEGORY.NORMAL_LANDSCAPE
					Configuration.ORIENTATION_UNDEFINED -> DEVICE_CATEGORY.NORMAL_PORTRAIT
					else -> DEVICE_CATEGORY.NORMAL_PORTRAIT
				}
			}
			Configuration.SCREENLAYOUT_SIZE_LARGE -> {
				when (configuration.orientation) {
					Configuration.ORIENTATION_PORTRAIT -> DEVICE_CATEGORY.LARGE_PORTRAIT
					Configuration.ORIENTATION_LANDSCAPE -> DEVICE_CATEGORY.LARGE_LANDSCAPE
					Configuration.ORIENTATION_UNDEFINED -> DEVICE_CATEGORY.LARGE_PORTRAIT
					else -> DEVICE_CATEGORY.LARGE_PORTRAIT
				}
			}
			Configuration.SCREENLAYOUT_SIZE_XLARGE -> {
				when (configuration.orientation) {
					Configuration.ORIENTATION_PORTRAIT -> DEVICE_CATEGORY.LARGE_PORTRAIT
					Configuration.ORIENTATION_LANDSCAPE -> DEVICE_CATEGORY.LARGE_LANDSCAPE
					Configuration.ORIENTATION_UNDEFINED -> DEVICE_CATEGORY.LARGE_PORTRAIT
					else -> DEVICE_CATEGORY.LARGE_PORTRAIT
				}
			}
			Configuration.SCREENLAYOUT_SIZE_UNDEFINED -> {
				when (configuration.orientation) {
					Configuration.ORIENTATION_PORTRAIT -> DEVICE_CATEGORY.NORMAL_PORTRAIT
					Configuration.ORIENTATION_LANDSCAPE -> DEVICE_CATEGORY.NORMAL_LANDSCAPE
					Configuration.ORIENTATION_UNDEFINED -> DEVICE_CATEGORY.NORMAL_PORTRAIT
					else -> DEVICE_CATEGORY.NORMAL_PORTRAIT
				}
			}
			else -> {
				when (configuration.orientation) {
					Configuration.ORIENTATION_PORTRAIT -> DEVICE_CATEGORY.NORMAL_PORTRAIT
					Configuration.ORIENTATION_LANDSCAPE -> DEVICE_CATEGORY.NORMAL_LANDSCAPE
					Configuration.ORIENTATION_UNDEFINED -> DEVICE_CATEGORY.NORMAL_PORTRAIT
					else -> DEVICE_CATEGORY.NORMAL_PORTRAIT
				}
			}
		}
	}

	fun download(link: String, path: String, progress: ((Long, Long) -> Unit)? = null): Long {
		val url = URL(link)
		val connection = url.openConnection()
		connection.connect()
		val length = connection.contentLengthLong
		url.openStream().use { input ->
			FileOutputStream(File(path)).use { output ->
				val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
				var bytesRead = input.read(buffer)
				var bytesCopied = 0L
				while (bytesRead >= 0) {
					output.write(buffer, 0, bytesRead)
					bytesCopied += bytesRead
					progress?.invoke(bytesCopied, length)
					bytesRead = input.read(buffer)
				}
				return bytesCopied
			}
		}
	}

	fun viewToBitmap(view: View): Bitmap {
		val bitmap = Bitmap.createBitmap(view.layoutParams.width, view.layoutParams.height, Bitmap.Config.ARGB_8888)
		val canvas = Canvas(bitmap)
		view.layout(view.left, view.top, view.right, view.bottom)
		view.draw(canvas)
		return bitmap
	}

	fun coordinateFilter(latLng: LatLng, unit: com.pushpull.triplee.konstant.Constant.Companion.Settings): String {
		return when (unit) {
			com.pushpull.triplee.konstant.Constant.Companion.Settings.DD -> "${latLng.latitude.round(6)}, ${latLng.longitude.round(6)}"
			com.pushpull.triplee.konstant.Constant.Companion.Settings.DM -> {
				val latitudeDegree = latLng.latitude.toInt()
				val latitudeMinutes = (abs(latLng.latitude - latitudeDegree) * 60).round(3)
				val longitudeDegree = latLng.longitude.toInt()
				val longitudeMinutes = (abs(latLng.longitude - longitudeDegree) * 60).round(3)

				val latitudeUnit = if (latLng.latitude > 0) {
					"N"
				} else {
					"S"
				}

				val longitudeUnit = if (latLng.longitude > 0) {
					"E"
				} else {
					"W"
				}

				"${abs(latitudeDegree)}°$latitudeMinutes' $latitudeUnit, ${abs(longitudeDegree)}°$longitudeMinutes' $longitudeUnit"
			}
			com.pushpull.triplee.konstant.Constant.Companion.Settings.DMS -> {
				val latitudeDegree = latLng.latitude.toInt()
				val _latitudeMinutes = (abs(latLng.latitude - latitudeDegree) * 60).round(6)
				val latitudeMinutes = _latitudeMinutes.toInt()
				val latitudeSeconds = (abs(_latitudeMinutes - latitudeMinutes) * 60).toInt()

				val longitudeDegree = latLng.longitude.toInt()
				val _longitudeMinutes = (abs(latLng.longitude - longitudeDegree) * 60).round(6)
				val longitudeMinutes = _longitudeMinutes.toInt()
				val longitudeSeconds = (abs(_longitudeMinutes - longitudeMinutes) * 60).toInt()

				val latitudeUnit = if (latLng.latitude > 0) {
					"N"
				} else {
					"S"
				}

				val longitudeUnit = if (latLng.longitude > 0) {
					"E"
				} else {
					"W"
				}

				"${abs(latitudeDegree)}°$latitudeMinutes'$latitudeSeconds\" $latitudeUnit, " +
						"${abs(longitudeDegree)}°$longitudeMinutes'$longitudeSeconds\" $longitudeUnit"
			}
			else -> "${latLng.latitude}, ${latLng.longitude}"
		}
	}

	fun distanceFilter(distance: Int, unit: com.pushpull.triplee.konstant.Constant.Companion.Settings): String {
		return when (unit) {
			com.pushpull.triplee.konstant.Constant.Companion.Settings.METRE -> "$distance metres"
			com.pushpull.triplee.konstant.Constant.Companion.Settings.KILOMETRE -> "${(distance * 0.001).round(2)} Kilometres"
			com.pushpull.triplee.konstant.Constant.Companion.Settings.FEET -> "${(distance * 3.28084).round(2)} feet"
			com.pushpull.triplee.konstant.Constant.Companion.Settings.MILE -> "${(distance * 0.000621371).round(2)} Mile"
			else -> "$distance metres"
		}
	}

	fun durationFilter(duration: Long, fullString: Boolean): String {
		val durationTime = abs(duration)
		val noDays: Long = TimeUnit.MILLISECONDS.toDays(durationTime)
		val noHours: Long = TimeUnit.MILLISECONDS.toHours(durationTime)
		val noMinutes: Long = TimeUnit.MILLISECONDS.toMinutes(durationTime)

		return if (fullString) {
			if (noDays == 0L) {
				"${noHours % 24} hours ${noMinutes % 60} minutes"
			} else {
				"$noDays days ${noHours % 24} hours ${noMinutes % 60} minutes"
			}
		} else {
			if (noDays == 0L) {
				"${noHours % 24} hr ${noMinutes % 60} min"
			} else {
				"$noDays days ${noHours % 24} hr ${noMinutes % 60} min"
			}
		}
	}

	fun Double.round(decimals: Int): Double {
		var multiplier = 1.0
		repeat(decimals) { multiplier *= 10 }
		return kotlin.math.round(this * multiplier) / multiplier
	}

	fun Float.round(decimals: Int): Double {
		var multiplier = 1.0
		repeat(decimals) { multiplier *= 10 }
		return kotlin.math.round(this * multiplier) / multiplier
	}

	const val TAG = "UTILITY"
}
