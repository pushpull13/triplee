package com.pushpull.triplee.cloud

import com.pushpull.triplee.custom.BaseActivity


class CloudActivity : BaseActivity() {

//	private lateinit var dataBinding: CloudActivityBinding
//
//	private val auth: FirebaseAuth = Firebase.auth
//	private lateinit var drive: Drive
//	private var isPremium by Delegates.notNull<Boolean>()
//
//	private var driveResult: com.google.api.services.drive.model.About? = null
//
//	override fun onCreate(savedInstanceState: Bundle?) {
//		super.onCreate(savedInstanceState)
//
//		isPremium = intent.getBooleanExtra(Constant.Companion.Settings.IS_PREMIUM.name, false)
//
//		dataBinding = DataBindingUtil.setContentView(this, R.layout.cloud_activity)
//		dataBinding.cloudActivityBack.setOnClickListener { finish() }
//	}
//
//	private fun isNetworkAvailable() =
//		(getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).run {
//			getNetworkCapabilities(activeNetwork)?.run {
//				hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ||
//						hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
//						hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)
//			} ?: false
//		}
//
//	private fun updateConnectionUI(isConnected: Boolean) {
//		mainScope.launch {
//			if (isConnected) {
//				dataBinding.cloudActivityNoConnection.visibility = View.GONE
//			} else {
//				dataBinding.cloudActivityNoConnection.visibility = View.VISIBLE
//				dataBinding.cloudActivityLottieLoading.visibility = View.GONE
//				dataBinding.cloudActivityLottieLoading.pauseAnimation()
//				dataBinding.cloudActivitySignIn.visibility = View.GONE
//				dataBinding.cloudActivityConnect.visibility = View.GONE
//			}
//		}
//	}
//
//	private fun showLoading() {
//		mainScope.launch {
//			dataBinding.cloudActivityLottieLoading.playAnimation()
//			dataBinding.cloudActivityLottieLoading.visibility = View.VISIBLE
//			dataBinding.cloudActivitySignIn.visibility = View.GONE
//			dataBinding.cloudActivityConnect.visibility = View.GONE
//		}
//	}
//
//	private fun showLoaded() {
//		mainScope.launch {
//			dataBinding.cloudActivityLottieLoading.visibility = View.GONE
//			dataBinding.cloudActivitySignIn.visibility = View.GONE
//			dataBinding.cloudActivityLottieLoading.pauseAnimation()
//		}
//	}
//
//	private fun showSignInLayout() {
//		mainScope.launch {
//			dataBinding.cloudActivitySignIn.apply {
//				setOnClickListener {
//					showLoading()
//
//					val googleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//						.requestIdToken(Secret.CLIENT_ID)
//						.requestServerAuthCode(Secret.CLIENT_ID)
//						.requestProfile()
//						.requestEmail()
//						.build()
//
//					val googleSignInClient = GoogleSignIn.getClient(this@CloudActivity, googleSignInOptions)
//					signInActivityLauncher.launch(googleSignInClient.signInIntent)
//				}
//			}
//
//
//			dataBinding.cloudActivityLottieLoading.pauseAnimation()
//			dataBinding.cloudActivitySignIn.visibility = View.VISIBLE
//			dataBinding.cloudActivityConnect.visibility = View.GONE
//			dataBinding.cloudActivityConnectedLayout.visibility = View.GONE
//			dataBinding.cloudActivityLottieLoading.visibility = View.GONE
//		}
//	}
//
//	private fun showConnectLayout() {
//		mainScope.launch {
//			dataBinding.cloudActivitySignIn.visibility = View.GONE
//			dataBinding.cloudActivityConnect.visibility = View.VISIBLE
//			dataBinding.cloudActivityConnectedLayout.visibility = View.GONE
//
//			dataBinding.cloudActivityConnect.setOnClickListener {
//				showLoading()
//				checkIfSyncAvailable()
//			}
//			dataBinding.cloudActivityConnectionStatus.text = "Not connected"
//			showLoaded()
//		}
//	}
//
//	private fun showConnectedLayout() {
//		mainScope.launch {
//			dataBinding.cloudActivitySignIn.visibility = View.GONE
//			dataBinding.cloudActivityConnect.visibility = View.VISIBLE
//			dataBinding.cloudActivityConnectedLayout.visibility = View.VISIBLE
//
//			dataBinding.cloudActivityConnect.setOnClickListener(null)
//
//			showLoaded()
//
//			dataBinding.cloudActivityConnectionStatus.text = "Connected with Google Drive"
//		}
//	}
//
//	private fun refreshUI() {
//		showLoading()
//		updateConnectionUI(isNetworkAvailable())
//
//		if (isNetworkAvailable()) {
//			if (auth.currentUser == null) {
//				showSignInLayout()
//			} else {
//				drive = Drive.Builder(
//					NetHttpTransport(),
//					JacksonFactory.getDefaultInstance(),
//					GoogleAccountCredential
//						.usingOAuth2(this@CloudActivity, listOf(DriveScopes.DRIVE_APPDATA))
//						.setSelectedAccountName(auth.currentUser!!.email))
//					.setApplicationName(getString(R.string.app_name))
//					.build()
//
//				ioScope.launch {
//					try {
//						driveResult = drive.about().get().apply {
//							fields = "storageQuota"
//						}.execute()
//						mainScope.launch { showConnectedLayout() }
//					} catch (userRecoverableAuthIOException: UserRecoverableAuthIOException) {
//						userRecoverableAuthIOException.printStackTrace()
//						showConnectLayout()
//					} catch (ioException: IOException) {
//						ioException.printStackTrace()
//						Log.i(TAG, "ioException")
//						updateConnectionUI(false)
//					} catch (exception: Exception) {
//						exception.printStackTrace()
//						Log.i(TAG, "exception")
//						updateConnectionUI(false)
//					}
//				}
//			}
//		} else {
//			Log.i(TAG, "no internet")
//			updateConnectionUI(false)
//		}
//	}
//
//	@ExperimentalPathApi
//	override fun onStart() {
//		super.onStart()
//
//		refreshUI()
//
//		val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
//		connectivityManager.registerDefaultNetworkCallback(object : ConnectivityManager.NetworkCallback() {
//			override fun onAvailable(network: Network) {
//				refreshUI()
//			}
//
//			override fun onLost(network: Network) {
//				refreshUI()
//			}
//		})
//
//		dataBinding.cloudActivityExportData.setOnClickListener {
//			if (isPremium) {
//				registerForCreateFileActivityResult.launch("Triplee ${Utility.timestampToPretty(System.currentTimeMillis())}.zip")
//			} else {
//				try {
//					Random.nextDouble(1.0).apply {
//						registerForCreateFileActivityResult.launch("Triplee ${Utility.timestampToPretty(System.currentTimeMillis())}.zip")
//					}
//				} catch (exception: Exception) {
//					registerForCreateFileActivityResult.launch("Triplee ${Utility.timestampToPretty(System.currentTimeMillis())}.zip")
//				}
//			}
//		}
//	}
//
//	private fun checkIfSyncAvailable() {
//		val firestore = FirebaseFirestore.getInstance()
//		var isSyncAvailable: Boolean? = false
//		firestore.collection("app").document("config").get()
//			.addOnSuccessListener { document ->
//				if (document != null) {
//					try {
//						isSyncAvailable = document.data?.get("sync") as Boolean?
//					} catch (exception: Exception) {
//						exception.printStackTrace()
//					}
//
//					if (isSyncAvailable == true) {
//						val googleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//							.requestIdToken(Secret.CLIENT_ID)
//							.requestScopes(Scope(DriveScopes.DRIVE_APPDATA))
//							.requestServerAuthCode(Secret.CLIENT_ID)
//							.requestEmail()
//							.build()
//						val googleSignInClient = GoogleSignIn.getClient(this@CloudActivity, googleSignInOptions)
//						signInActivityLauncher.launch(googleSignInClient.signInIntent)
//					} else {
//						mainScope.launch {
//							Toast.makeText(this@CloudActivity, "Synchronization temporarily unavailable. We're looking into that.", Toast.LENGTH_SHORT).show()
//							dataBinding.cloudActivityConnect.visibility = View.VISIBLE
//							showLoaded()
//						}
//					}
//				}
//			}
//			.addOnFailureListener {
//				mainScope.launch {
//					Toast.makeText(this@CloudActivity, "Synchronization temporarily unavailable. We're looking into that.", Toast.LENGTH_SHORT).show()
//					dataBinding.cloudActivityConnect.visibility = View.VISIBLE
//					showLoaded()
//				}
//			}
//	}
//
//	private val signInActivityLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
//		val task = GoogleSignIn.getSignedInAccountFromIntent(result.data)
//		try {
//			val account = task.getResult(ApiException::class.java)!!
//			Log.d(TAG, "firebaseAuthWithGoogle:" + account.id)
//			Log.i(TAG, "serverAuthCode : ${account.serverAuthCode}")
//			Toast.makeText(this, "Connected with your Google Drive ${account.displayName}", Toast.LENGTH_SHORT).show()
//			firebaseAuthWithGoogle(account.idToken!!)
//		} catch (apiException: ApiException) {
//			apiException.printStackTrace()
//			Log.w(TAG, "Google sign in failed : ${apiException.status.statusMessage}", apiException)
//			Toast.makeText(this, "Connection unsuccessful.", Toast.LENGTH_SHORT).show()
//			refreshUI()
//		} catch (exception: Exception) {
//			exception.printStackTrace()
//			Log.w(TAG, "Google sign in failed", exception)
//			Toast.makeText(this, "Connection unsuccessful.", Toast.LENGTH_SHORT).show()
//			refreshUI()
//		}
//	}
//
//	private fun firebaseAuthWithGoogle(idToken: String) {
//		val credential = GoogleAuthProvider.getCredential(idToken, null)
//		auth.signInWithCredential(credential).addOnCompleteListener(this) { task ->
//			if (task.isSuccessful) {
//				ioScope.launch { syncProfileImage() }
//				ioScope.launch { syncWithFirebase() }
//				ioScope.launch { connectWithDrive() }
//				refreshUI()
//			} else {
//				Log.w(GetStartedFragment.TAG, "signInWithCredential:failure", task.exception)
//			}
//		}
//	}
//
//	private fun connectWithDrive() {
//		try {
//			drive = Drive.Builder(
//				NetHttpTransport(),
//				JacksonFactory.getDefaultInstance(),
//				GoogleAccountCredential
//					.usingOAuth2(this@CloudActivity, listOf(DriveScopes.DRIVE_APPDATA))
//					.setSelectedAccountName(auth.currentUser!!.email))
//				.setApplicationName(getString(R.string.app_name))
//				.build()
//
//			driveResult = drive.about().get().apply {
//				fields = "storageQuota"
//			}.execute()
//		} catch (userRecoverableAuthIOException: UserRecoverableAuthIOException) {
//			userRecoverableAuthIOException.printStackTrace()
//			Log.i(TAG, "not connected : userRecoverableAuthIOException")
//		} catch (exception: Exception) {
//			exception.printStackTrace()
//			Log.i(TAG, "not connected : exception")
//		}
//	}
//
//	private fun syncProfileImage() {
//		try {
//			Utility.download(auth.currentUser!!.photoUrl.toString().replace("/s96-c/", "/s512-c/"),
//				"${Path.APP_DATA}/profileImage.png") { copiedLength, totalLength ->
//				if (copiedLength == totalLength) {
//				}
//			}
//		} catch (exception: java.lang.Exception) {
//			exception.printStackTrace()
//			try {
//				Utility.download(auth.currentUser!!.photoUrl.toString(),
//					"${Path.APP_DATA}/profileImage.png") { copiedLength, totalLength ->
//					if (copiedLength == totalLength) {
//					}
//				}
//			} catch (exception: java.lang.Exception) {
//				exception.printStackTrace()
//			}
//		}
//	}
//
//	private fun syncWithFirebase() {
//		if (auth.currentUser?.email != null) {
//			val data: Map<String, Long> = mapOf(
//				"joinTimestamp" to System.currentTimeMillis()
//			)
//
//			val firestore = FirebaseFirestore.getInstance()
//			var joinTimestamp: Long? = null
//			firestore.collection("user").document(auth.currentUser!!.uid).get()
//				.addOnSuccessListener { document ->
//					if (document != null) {
//						try {
//							joinTimestamp = document.data?.get("joinTimestamp") as Long?
//						} catch (exception: Exception) {
//							exception.printStackTrace()
//						}
//					}
//				}
//			if (joinTimestamp == null) {
//				firestore.collection("user").document(auth.currentUser!!.uid).set(data)
//					.addOnCompleteListener {
//						Log.i(GetStartedFragment.TAG, "sync complete...")
//						sharedPreferencesAppConfig.edit().putString(Constant.Companion.Settings.DISPLAY_NAME.name, auth.currentUser!!.displayName).apply()
//						sharedPreferencesAppConfig.edit().putBoolean(Constant.Companion.Settings.IS_SYNCED.name, true).apply()
//					}
//					.addOnFailureListener {
//						val firebaseFirestoreException = it as FirebaseFirestoreException
//						Log.i(GetStartedFragment.TAG, firebaseFirestoreException.toString())
//						sharedPreferencesAppConfig.edit().putBoolean(Constant.Companion.Settings.IS_SYNCED.name, false).apply()
//					}
//			}
//		}
//	}
//
//	private val registerForCreateFileActivityResult = registerForActivityResult(Utility.CreateSpecificTypeDocument("application/zip")) { outputFileUri ->
//		if (outputFileUri!=null) {
//			ioScope.launch {
//				val zipArchiveOutputStream = ZipArchiveOutputStream(contentResolver.openOutputStream(outputFileUri))
//				val tripTable: TripTableDao = UserDatabase.getInstance(this@CloudActivity).tripTableDao
//
//				tripTable.getAll().forEach { trip ->
//					if (!trip.isDeleted) {
//
//						val gpx = Utility.generateGpxObject(trip.primaryKey)
//
//						zipArchiveOutputStream.putArchiveEntry(ZipArchiveEntry("${trip.title}.gpx"))
//						zipArchiveOutputStream.setLevel(1)
//						val gpxByteArray = GPX.writer().toString(gpx).toByteArray()
//						zipArchiveOutputStream.write(gpxByteArray)
//					}
//				}
//
//				mainScope.launch {
//					Toast.makeText(applicationContext, "Trip data exported", Toast.LENGTH_SHORT).show()
//				}
//			}
//		}
//	}
//
//	companion object {
//		const val TAG = "CLOUD_ACTIVITY"
//	}
}
