package com.pushpull.triplee.dataClass

import com.fasterxml.jackson.annotation.JsonProperty

data class TripData(
	@JsonProperty("startTimestamp")
	val startTimestamp: Long,
	@JsonProperty("next")
	var next: String?
) {
	var katoTimestamp: Long = -1
	var panoTimestamp: Long = -1
	var activity: Int = -1
	var pseudoActivity: Int? = -1
	var tripLocationDataMap: MutableMap<Long, List<Double>> = mutableMapOf()
	var address: String? = null
	var locality: String? = null
}
