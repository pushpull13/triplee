package com.pushpull.triplee.dataClass

class AppTheme {
	var colorPrimary: Int = 0
	var colorOnPrimary: Int = 0

	var colorAlpha: Int = 0
	var colorOnAlpha: Int = 0

	var colorBeta: Int = 0
	var colorOnBeta: Int = 0

	var colorGamma: Int = 0
	var colorOnGamma: Int = 0

	var colorSurface: Int = 0
	var colorOnSurface: Int = 0

	var colorBackground: Int = 0
	var colorOnBackground: Int = 0

	var textFontFamily: Int = 0

	var roundedCornerRipple: Int = 0
}
