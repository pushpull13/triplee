package com.pushpull.triplee.konstant

object Path {

	var ROOT_PATH: String? = null

	val DATA_PATH: String = "data"
		get() = "$ROOT_PATH/$field"

	val APP_DATA: String = "appData"
		get() = "$DATA_PATH/$field"

	//	Path
	val TRIP_FOLDER_NAME: String = "tripDetail"
		get() = "$DATA_PATH/$field"
	val TRIP_FILE_XXXXXX_NAME: String = "yyyyyy/tripData_xxxxxx.json"
		get() = "$TRIP_FOLDER_NAME/$field"
	val TRIP_SNAPSHOT_FILE_XXXXXX_NAME: String = "tripData_snapshot_xxxxxx.png"
		get() = "$TRIP_FOLDER_NAME/$field"

}
