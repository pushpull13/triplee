package com.pushpull.triplee.konstant

import com.pushpull.triplee.R


class ActivityId {
	companion object {
		enum class ACTIVITY {
			POPULAR,
			MOUNTAIN,
			FOREST,
			CITY_AND_ROAD,
			WATER,
			AIR,
			WINTER,
			BACKPACKING,
			ROCK_CLIMBING,
			MOUNTAIN_BIKING,
			SKIING,
			SNOW_BOARDING,
			ICE_SKATING,
			TREKKING,
			CAVING,
			ATV_RIDING,
			BIRD_WATCHING,
			CAMPING,
			SAFARI,
			TOUR,
			MUSEUM,
			LONG_DRIVE,
			CYCLING,
			SHOPPING,
			MOVIE,
			DINNER,
			TENNIS,
			PHOTOGRAPHY,
			STARGAZING,
			CANOEING,
			SURFING,
			JET_SKIING,
			SCUBA_DIVING,
			SWIMMING,
			PARASAILING,
			RAFTING,
			BEACH,
			BALLOONING,
			AERO_GLIDING,
			PARAGLIDING,
			OTHER
		}

		val ACTIVITY_ID_NAME_MAP: Map<ACTIVITY, String> = mapOf(
			ACTIVITY.POPULAR to "Popular",
			ACTIVITY.MOUNTAIN to "Mountain",
			ACTIVITY.FOREST to "Forest",
			ACTIVITY.CITY_AND_ROAD to "City and Road",
			ACTIVITY.WATER to "Water",
			ACTIVITY.AIR to "Air",
			ACTIVITY.WINTER to "Winter",
			ACTIVITY.BACKPACKING to "Backpacking",
			ACTIVITY.ROCK_CLIMBING to "Rock Climbing",
			ACTIVITY.MOUNTAIN_BIKING to "Mountain Biking",
			ACTIVITY.SKIING to "Skiing",
			ACTIVITY.SNOW_BOARDING to "Snow Boarding",
			ACTIVITY.ICE_SKATING to "Ice Skating",
			ACTIVITY.TREKKING to "Trekking",
			ACTIVITY.CAVING to "Caving",
			ACTIVITY.ATV_RIDING to "ATV Riding",
			ACTIVITY.BIRD_WATCHING to "Bird Watching",
			ACTIVITY.CAMPING to "Camping",
			ACTIVITY.SAFARI to "Safari",
			ACTIVITY.TOUR to "Tour",
			ACTIVITY.MUSEUM to "Museum",
			ACTIVITY.LONG_DRIVE to "Long Drive",
			ACTIVITY.CYCLING to "Cycling",
			ACTIVITY.SHOPPING to "Shopping",
			ACTIVITY.MOVIE to "Movie",
			ACTIVITY.DINNER to "Dinner",
			ACTIVITY.TENNIS to "Tennis",
			ACTIVITY.PHOTOGRAPHY to "Photography",
			ACTIVITY.STARGAZING to "Stargazing",
			ACTIVITY.CANOEING to "Canoeing",
			ACTIVITY.SURFING to "Surfing",
			ACTIVITY.JET_SKIING to "Jet Skiing",
			ACTIVITY.SCUBA_DIVING to "Scuba Diving",
			ACTIVITY.SWIMMING to "Swimming",
			ACTIVITY.PARASAILING to "Parasailing",
			ACTIVITY.RAFTING to "Rafting",
			ACTIVITY.BEACH to "Beach",
			ACTIVITY.BALLOONING to "Ballooning",
			ACTIVITY.AERO_GLIDING to "Aero Gliding",
			ACTIVITY.PARAGLIDING to "Paragliding",
			ACTIVITY.OTHER to "Other"
		)

		val ACTIVITY_ID_LOCK_MAP: Map<ACTIVITY, Boolean> = mapOf(
			ACTIVITY.POPULAR to false,
			ACTIVITY.MOUNTAIN to false,
			ACTIVITY.FOREST to false,
			ACTIVITY.CITY_AND_ROAD to false,
			ACTIVITY.WATER to false,
			ACTIVITY.AIR to false,
			ACTIVITY.WINTER to false,
			ACTIVITY.BACKPACKING to false,
			ACTIVITY.ROCK_CLIMBING to true,
			ACTIVITY.MOUNTAIN_BIKING to true,
			ACTIVITY.SKIING to true,
			ACTIVITY.SNOW_BOARDING to true,
			ACTIVITY.ICE_SKATING to false,
			ACTIVITY.TREKKING to true,
			ACTIVITY.CAVING to true,
			ACTIVITY.ATV_RIDING to true,
			ACTIVITY.BIRD_WATCHING to true,
			ACTIVITY.CAMPING to true,
			ACTIVITY.SAFARI to true,
			ACTIVITY.TOUR to true,
			ACTIVITY.MUSEUM to false,
			ACTIVITY.LONG_DRIVE to false,
			ACTIVITY.CYCLING to false,
			ACTIVITY.SHOPPING to true,
			ACTIVITY.MOVIE to true,
			ACTIVITY.DINNER to true,
			ACTIVITY.TENNIS to true,
			ACTIVITY.PHOTOGRAPHY to true,
			ACTIVITY.STARGAZING to true,
			ACTIVITY.CANOEING to true,
			ACTIVITY.SURFING to true,
			ACTIVITY.JET_SKIING to true,
			ACTIVITY.SCUBA_DIVING to true,
			ACTIVITY.SWIMMING to true,
			ACTIVITY.PARASAILING to true,
			ACTIVITY.RAFTING to true,
			ACTIVITY.BEACH to true,
			ACTIVITY.BALLOONING to true,
			ACTIVITY.AERO_GLIDING to true,
			ACTIVITY.PARAGLIDING to true,
			ACTIVITY.OTHER to true
		)

		val ACTIVITY_ID_RES_MAP: Map<ACTIVITY, Int> = mapOf(
			ACTIVITY.POPULAR to R.drawable.activity_popular,
			ACTIVITY.MOUNTAIN to R.drawable.activity_mountain,
			ACTIVITY.FOREST to R.drawable.activity_forest,
			ACTIVITY.CITY_AND_ROAD to R.drawable.activity_city_and_road,
			ACTIVITY.WATER to R.drawable.activity_water,
			ACTIVITY.AIR to R.drawable.activity_air,
			ACTIVITY.WINTER to R.drawable.activity_winter,
			ACTIVITY.BACKPACKING to R.drawable.activity_backpacking,
			ACTIVITY.ROCK_CLIMBING to R.drawable.activity_rock_climbing,
			ACTIVITY.MOUNTAIN_BIKING to R.drawable.activity_mountain_biking,
			ACTIVITY.SKIING to R.drawable.activity_skiing,
			ACTIVITY.SNOW_BOARDING to R.drawable.activity_snowboarding,
			ACTIVITY.ICE_SKATING to R.drawable.activity_skating,
			ACTIVITY.TREKKING to R.drawable.activity_trekking,
			ACTIVITY.CAVING to R.drawable.activity_caving,
			ACTIVITY.ATV_RIDING to R.drawable.activity_atv_riding,
			ACTIVITY.BIRD_WATCHING to R.drawable.activity_bird_watching,
			ACTIVITY.CAMPING to R.drawable.activity_caving,
			ACTIVITY.SAFARI to R.drawable.activity_safari,
			ACTIVITY.TOUR to R.drawable.activity_tour,
			ACTIVITY.MUSEUM to R.drawable.activity_museum,
			ACTIVITY.LONG_DRIVE to R.drawable.activity_long_drive,
			ACTIVITY.CYCLING to R.drawable.activity_cycling,
			ACTIVITY.SHOPPING to R.drawable.activity_shopping,
			ACTIVITY.MOVIE to R.drawable.activity_movie,
			ACTIVITY.DINNER to R.drawable.activity_dinner,
			ACTIVITY.TENNIS to R.drawable.activity_tennis,
			ACTIVITY.PHOTOGRAPHY to R.drawable.activity_photography,
			ACTIVITY.STARGAZING to R.drawable.activity_stargazing,
			ACTIVITY.CANOEING to R.drawable.activity_canoeing,
			ACTIVITY.SURFING to R.drawable.activity_surfing,
			ACTIVITY.JET_SKIING to R.drawable.activity_jet_skiing,
			ACTIVITY.SCUBA_DIVING to R.drawable.activity_scuba_diving,
			ACTIVITY.SWIMMING to R.drawable.activity_swimming,
			ACTIVITY.PARASAILING to R.drawable.activity_parasailing,
			ACTIVITY.RAFTING to R.drawable.activity_rafting,
			ACTIVITY.BEACH to R.drawable.activity_beach,
			ACTIVITY.BALLOONING to R.drawable.activity_ballooning,
			ACTIVITY.AERO_GLIDING to R.drawable.activity_aero_gliding,
			ACTIVITY.PARAGLIDING to R.drawable.activity_paragliding,
			ACTIVITY.OTHER to R.drawable.activity_other
		)

		val ACTIVITY_META_ID_LIST: List<ACTIVITY> = listOf(
			ACTIVITY.POPULAR,
			ACTIVITY.MOUNTAIN,
			ACTIVITY.FOREST,
			ACTIVITY.CITY_AND_ROAD,
			ACTIVITY.WATER,
			ACTIVITY.AIR,
			ACTIVITY.WINTER
		)

		val ACTIVITY_POPULAR_ID_LIST: List<ACTIVITY> = listOf(
			ACTIVITY.CYCLING,
			ACTIVITY.TREKKING,
			ACTIVITY.CAMPING,
			ACTIVITY.SWIMMING,
			ACTIVITY.TENNIS,
		)

		val ACTIVITY_MOUNTAIN_ID_LIST: List<ACTIVITY> = listOf(
			ACTIVITY.BACKPACKING,
			ACTIVITY.ROCK_CLIMBING,
			ACTIVITY.MOUNTAIN_BIKING,
			ACTIVITY.SKIING,
			ACTIVITY.SNOW_BOARDING,
			ACTIVITY.TREKKING,
			ACTIVITY.CAVING
		)

		val ACTIVITY_FOREST_ID_LIST: List<ACTIVITY> = listOf(
			ACTIVITY.BACKPACKING,
			ACTIVITY.TREKKING,
			ACTIVITY.ATV_RIDING,
			ACTIVITY.BIRD_WATCHING,
			ACTIVITY.CAMPING,
			ACTIVITY.SAFARI,
		)

		val ACTIVITY_CITY_AND_ROADS_ID_LIST: List<ACTIVITY> = listOf(
			ACTIVITY.LONG_DRIVE,
			ACTIVITY.CYCLING,
			ACTIVITY.TENNIS,
			ACTIVITY.TOUR,
			ACTIVITY.MUSEUM,
			ACTIVITY.SHOPPING,
			ACTIVITY.MOVIE,
			ACTIVITY.DINNER,
			ACTIVITY.PHOTOGRAPHY,
			ACTIVITY.STARGAZING
		)

		val ACTIVITY_WATER_ID_LIST: List<ACTIVITY> = listOf(
			ACTIVITY.BEACH,
			ACTIVITY.CANOEING,
			ACTIVITY.SURFING,
			ACTIVITY.JET_SKIING,
			ACTIVITY.SCUBA_DIVING,
			ACTIVITY.SWIMMING,
			ACTIVITY.PARASAILING,
			ACTIVITY.RAFTING
		)

		val ACTIVITY_AIR_ID_LIST: List<ACTIVITY> = listOf(
			ACTIVITY.BALLOONING,
			ACTIVITY.AERO_GLIDING,
			ACTIVITY.PARAGLIDING
		)

		val ACTIVITY_WINTER_ID_LIST: List<ACTIVITY> = listOf(
			ACTIVITY.SKIING,
			ACTIVITY.SNOW_BOARDING,
			ACTIVITY.ICE_SKATING
		)

		val PSEUDO_ACTIVITY_RES_MAP: Map<Int, Int> = mapOf(
			0 to R.drawable.pseudo_activity_in_vehicle,
			1 to R.drawable.pseudo_activity_on_bicycle,
			2 to R.drawable.pseudo_activity_on_foot,
			3 to R.drawable.pseudo_activity_still,
			7 to R.drawable.pseudo_activity_walking,
			8 to R.drawable.pseudo_activity_running,
			-1 to R.drawable.pseudo_activity_unknown,
			-2 to R.drawable.pseudo_activity_end
		)

		val PSEUDO_ACTIVITY_NAME_MAP: Map<Int, String> = mapOf(
			0 to "In Vehicle",
			1 to "On Bicycle",
			2 to "On Foot",
			3 to "Still",
			7 to "Waling",
			8 to "Running",
			-1 to "Unknown",
			-2 to "End"
		)
	}
}
