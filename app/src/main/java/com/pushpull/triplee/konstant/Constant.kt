package com.pushpull.triplee.konstant

import com.pushpull.triplee.R

class Constant {
	companion object {
		const val HOME_BANNER_AD_ID = "ca-app-pub-1574631117593270/4364170351"
		const val REVERSE_GEOCODE_REWARDED_AD_ID = "ca-app-pub-3940256099942544/5224354917"
		const val TRIP_FRAGMENT_0_BANNED_AD_ID = "ca-app-pub-3940256099942544/6300978111"
		const val TRIP_FRAGMENT_1_BANNED_AD_ID = "ca-app-pub-3940256099942544/6300978111"
		const val TRIP_FRAGMENT_2_BANNED_AD_ID = "ca-app-pub-3940256099942544/6300978111"
		const val TRIP_FRAGMENT_3_BANNED_AD_ID = "ca-app-pub-3940256099942544/6300978111"

		enum class Settings {
			IS_FIRST_INSTANCE,
			APP_CONFIG,
			USERNAME,
			IS_SYNCED,
			DISPLAY_NAME,
			EXPIRY_TIME,
			LOCK_FILE,
			ACTIVITY,
			LAST_LOCATION,
			SYNC_ON_DATA,
			SYNC_ON_ROAMING,
			CLOUD_SYNCER_FILE_ID,
			LAST_SYNC_TIMESTAMP,
			PRIMARY_KEY,
			DISTANCE,
			METRE,
			KILOMETRE,
			FEET,
			MILE,
			SPEED,
			METRE_PER_SECOND,
			KILOMETRE_PER_HOUR,
			FEET_PER_SECOND,
			MILE_PER_HOUR,
			LOCATION_PROVIDER,
			GPS_PROVIDER,
			NETWORK_PROVIDER,
			PASSIVE_PROVIDER,
			COORDINATE,
			DD,
			DM,
			DMS,
			APP_THEME,
			MAP_STYLE,
			MAP_POINTER_COLOR,
			MAP_POLYLINE_COLOR,
		}

		val SETTINGS_NAME_MAP: Map<Settings, Int> = mapOf(
			Settings.APP_CONFIG to R.string.settings,
			Settings.DISTANCE to R.string.distance,
			Settings.METRE to R.string.metre,
			Settings.KILOMETRE to R.string.kiloMetre,
			Settings.FEET to R.string.feet,
			Settings.MILE to R.string.mile,
			Settings.SPEED to R.string.speed,
			Settings.METRE_PER_SECOND to R.string.metrePerSecond,
			Settings.KILOMETRE_PER_HOUR to R.string.kilometrePerHour,
			Settings.FEET_PER_SECOND to R.string.feetPerSecond,
			Settings.MILE_PER_HOUR to R.string.milePerHour,
			Settings.LOCATION_PROVIDER to R.string.locationProvider,
			Settings.GPS_PROVIDER to R.string.gpsProvider,
			Settings.NETWORK_PROVIDER to R.string.networkProvider,
			Settings.PASSIVE_PROVIDER to R.string.passiveProvider,
			Settings.COORDINATE to R.string.coordinate,
			Settings.DD to R.string.decimalDegrees,
			Settings.DM to R.string.degreeDecimalMinutes,
			Settings.DMS to R.string.degreeDecimalMinutes,
			Settings.APP_THEME to R.string.appTheme,
		)

		val APP_THEME_LIST: List<Int> = listOf(
			R.style.Theme_Triplee_Aqua,
			R.style.Theme_Triplee_Rust,
			R.style.Theme_Triplee_Evergreen,
			R.style.Theme_Triplee_Tomato,
			R.style.Theme_Triplee_Coffee,
			R.style.Theme_Triplee_Honey,
			R.style.Theme_Triplee_DeepOcean,
			R.style.Theme_Triplee_Sunset,
			R.style.Theme_Triplee_Basil,
			R.style.Theme_Triplee_Sky,
			R.style.Theme_Triplee_Daffodil,
			R.style.Theme_Triplee_Turquoise,
			R.style.Theme_Triplee_Cocoa
		)

		val APP_THEME_LOCK_MAP: Map<Int, Boolean> = mapOf(
			R.style.Theme_Triplee_Aqua to false,
			R.style.Theme_Triplee_Rust to false,
			R.style.Theme_Triplee_Evergreen to false,
			R.style.Theme_Triplee_Tomato to true,
			R.style.Theme_Triplee_Coffee to true,
			R.style.Theme_Triplee_Honey to true,
			R.style.Theme_Triplee_DeepOcean to true,
			R.style.Theme_Triplee_Sunset to true,
			R.style.Theme_Triplee_Basil to true,
			R.style.Theme_Triplee_Sky to true,
			R.style.Theme_Triplee_Daffodil to true,
			R.style.Theme_Triplee_Turquoise to true,
			R.style.Theme_Triplee_Cocoa to true
		)

		val APP_THEME_NAME_RES_MAP: Map<Int, Int> = mapOf(
			R.style.Theme_Triplee_Aqua to R.string.appThemeAqua,
			R.style.Theme_Triplee_Evergreen to R.string.appThemeEvergreen,
			R.style.Theme_Triplee_DeepOcean to R.string.appThemeDeepOcean,
			R.style.Theme_Triplee_Honey to R.string.appThemeHoney,
			R.style.Theme_Triplee_Sunset to R.string.appThemeSunset,
			R.style.Theme_Triplee_Sky to R.string.appThemeSky,
			R.style.Theme_Triplee_Rust to R.string.appThemeRust,
			R.style.Theme_Triplee_Daffodil to R.string.appThemeDaffodil,
			R.style.Theme_Triplee_Basil to R.string.appThemeBasil,
			R.style.Theme_Triplee_Coffee to R.string.appThemeCoffee,
			R.style.Theme_Triplee_Tomato to R.string.appThemeTomato,
			R.style.Theme_Triplee_Cocoa to R.string.appThemeCocoa,
			R.style.Theme_Triplee_Turquoise to R.string.appThemeTurquoise,
		)

		val APP_THEME_COLOR_RES_MAP: Map<Int, Int> = mapOf(
			R.style.Theme_Triplee_Aqua to R.color.colorPrimaryAqua,
			R.style.Theme_Triplee_Evergreen to R.color.colorPrimaryEvergreen,
			R.style.Theme_Triplee_DeepOcean to R.color.colorPrimaryDeepOcean,
			R.style.Theme_Triplee_Honey to R.color.colorPrimaryHoney,
			R.style.Theme_Triplee_Sunset to R.color.colorPrimarySunset,
			R.style.Theme_Triplee_Sky to R.color.colorPrimarySky,
			R.style.Theme_Triplee_Rust to R.color.colorPrimaryRust,
			R.style.Theme_Triplee_Daffodil to R.color.colorPrimaryDaffodil,
			R.style.Theme_Triplee_Basil to R.color.colorPrimaryBasil,
			R.style.Theme_Triplee_Coffee to R.color.colorPrimaryCoffee,
			R.style.Theme_Triplee_Tomato to R.color.colorPrimaryTomato,
			R.style.Theme_Triplee_Cocoa to R.color.colorPrimaryCocoa,
			R.style.Theme_Triplee_Turquoise to R.color.colorPrimaryTurquoise,
		)

		enum class MapStyle {
			PAPER,
			RETRO,
			BLACK_WHITE,
			GREENER,
			COFFEE,
			WATER,
			MIDNIGHT,
			OREGANO,
			GOLD,
			NIGHT_LIGHT
		}

		val MAP_STYLE_DARK: Map<MapStyle, Boolean> = mapOf(
			MapStyle.PAPER to false,
			MapStyle.RETRO to false,
			MapStyle.BLACK_WHITE to true,
			MapStyle.GREENER to false,
			MapStyle.COFFEE to false,
			MapStyle.WATER to false,
			MapStyle.MIDNIGHT to true,
			MapStyle.OREGANO to true,
			MapStyle.GOLD to true,
			MapStyle.NIGHT_LIGHT to true,
		)

		val MAP_STYLE_NAME_MAP: Map<MapStyle, Int> = mapOf(
			MapStyle.PAPER to R.string.mapStylePaper,
			MapStyle.RETRO to R.string.mapStyleRetro,
			MapStyle.BLACK_WHITE to R.string.mapStyleBlackWhite,
			MapStyle.GREENER to R.string.mapStyleGreener,
			MapStyle.COFFEE to R.string.mapStyleCoffee,
			MapStyle.WATER to R.string.mapStyleWater,
			MapStyle.MIDNIGHT to R.string.mapStyleMidnight,
			MapStyle.OREGANO to R.string.mapStyleOregano,
			MapStyle.GOLD to R.string.mapStyleGold,
			MapStyle.NIGHT_LIGHT to R.string.mapStyleNightLight
		)

		val MAP_STYLE_IMG_MAP: Map<MapStyle, Int> = mapOf(
			MapStyle.PAPER to R.drawable.map_style_paper,
			MapStyle.RETRO to R.drawable.map_style_retro,
			MapStyle.BLACK_WHITE to R.drawable.map_style_black_white,
			MapStyle.GREENER to R.drawable.map_style_greener,
			MapStyle.COFFEE to R.drawable.map_style_coffee,
			MapStyle.WATER to R.drawable.map_style_water,
			MapStyle.MIDNIGHT to R.drawable.map_style_midnight,
			MapStyle.OREGANO to R.drawable.map_style_oregano,
			MapStyle.GOLD to R.drawable.map_style_gold,
			MapStyle.NIGHT_LIGHT to R.drawable.map_style_night_light
		)

		val MAP_STYLE_RES_MAP: Map<MapStyle, Int> = mapOf(
			MapStyle.PAPER to R.raw.map_style_paper,
			MapStyle.RETRO to R.raw.map_style_retro,
			MapStyle.BLACK_WHITE to R.raw.map_style_black_white,
			MapStyle.GREENER to R.raw.map_style_greener,
			MapStyle.COFFEE to R.raw.map_style_coffee,
			MapStyle.WATER to R.raw.map_style_water,
			MapStyle.MIDNIGHT to R.raw.map_style_midnight,
			MapStyle.OREGANO to R.raw.map_style_oregano,
			MapStyle.GOLD to R.raw.map_style_gold,
			MapStyle.NIGHT_LIGHT to R.raw.map_style_night_light
		)
	}
}
