package com.pushpull.triplee.main

import android.app.Application
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.*
import com.pushpull.triplee.background.TripService
import com.pushpull.triplee.database.UserDatabase
import com.pushpull.triplee.database.trip.Trip
import com.pushpull.triplee.konstant.ActivityId
import com.pushpull.triplee.konstant.Path
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.io.File


class MainViewModel(application: Application) : AndroidViewModel(application) {

	private val job: Job = Job()
	private val ioScope = CoroutineScope(Dispatchers.IO + job)

//	lateinit var syncService: SyncService
//	private val _isBoundToSyncService: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply { value = false }
//	val isBoundToSyncService: LiveData<Boolean> = _isBoundToSyncService
//	private val syncServiceConnection = object : ServiceConnection {
//		override fun onServiceConnected(name: ComponentName?, iBinder: IBinder?) {
//			Log.i(TAG, "onServiceConnected...")
//			val binder = iBinder as SyncService.LocalBinder
//			this@MainViewModel.syncService = binder.getService()
//			this@MainViewModel._isBoundToSyncService.value = true
//		}
//
//		override fun onServiceDisconnected(name: ComponentName?) {
//			this@MainViewModel._isBoundToSyncService.value = false
//		}
//	}

	lateinit var tripService: TripService
	var isBoundToTripService: Boolean = false
	private val tripServiceConnection = object : ServiceConnection {
		@RequiresApi(Build.VERSION_CODES.Q)
		override fun onServiceConnected(name: ComponentName?, iBinder: IBinder?) {
			Log.i(TAG, "onServiceConnected...")
			val binder = iBinder as TripService.LocalBinder
			this@MainViewModel.tripService = binder.getService()
			this@MainViewModel.isBoundToTripService = true
			this@MainViewModel.onBoundTripServiceListener?.onBoundTripServiceListener(true)
			this@MainViewModel.onBoundTripService()
		}

		override fun onServiceDisconnected(name: ComponentName?) {
			this@MainViewModel.isBoundToTripService = false
		}
	}

	val tripTable = UserDatabase.getInstance(application).tripTableDao
	val placeTable = UserDatabase.getInstance(application).placeTableDao

	init {
		startAndOrBindServices()
	}

	override fun onCleared() {
		super.onCleared()
		Log.i(TAG, "onCleared...")
		unbindServices()
	}

	fun checkServiceBound() {
		onBoundTripServiceListener?.onBoundTripServiceListener(isBoundToTripService)
	}

	private fun startAndOrBindServices() {
		Log.i(TAG, "bindServices...")
		Intent(getApplication(), TripService::class.java).also { intent ->
			getApplication<Application>().startService(intent)
			getApplication<Application>().bindService(intent, tripServiceConnection, AppCompatActivity.BIND_ABOVE_CLIENT)
		}

//		Intent(getApplication(), SyncService::class.java).also { intent ->
//			getApplication<Application>().startService(intent)
//			getApplication<Application>().bindService(intent, syncServiceConnection, AppCompatActivity.BIND_ABOVE_CLIENT)
//		}
	}

	interface OnBoundTripServiceListener {
		fun onBoundTripServiceListener(isBound: Boolean)
	}

	private var onBoundTripServiceListener: OnBoundTripServiceListener? = null
	fun setOnBoundTripServiceListener(listener: OnBoundTripServiceListener) {
		Log.i(TAG, "OnBoundTripServiceListener")
		onBoundTripServiceListener = listener
	}

	private fun onBoundTripService() {

	}

	fun startTrip() {
		if (getApplication<Application>().checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
			tripService.startTrip()
		}
	}

	fun switchActivity(activityId: ActivityId.Companion.ACTIVITY) {
		if (isBoundToTripService) {
			tripService.dumpAndOrSwitchActivity(activityId)
		}
	}

	fun deleteTrip(primaryKey: Long) {
		ioScope.launch {
			val prevTrip = tripTable.get(primaryKey)!!
			val trip = Trip(primaryKey, 0).apply {
				modifiedTimestamp = System.currentTimeMillis()
				fileId = prevTrip.fileId
				isDeleted = true
			}

			File("${Path.TRIP_FOLDER_NAME}/$primaryKey").apply {
				if(this.exists()) {
					this.deleteRecursively()
				}
			}

			File("${Path.TRIP_FOLDER_NAME}/thumbnail/$primaryKey.png").apply {
				if (this.exists()){
					this.delete()
				}
			}

			tripTable.insert(trip)
		}
	}

	private fun unbindServices() {
		Log.i(TAG, "unBindServices...")
//		if (isBoundToSyncService.value == true) {
//			getApplication<Application>().unbindService(syncServiceConnection)
//		}
		if (isBoundToTripService) {
			getApplication<Application>().unbindService(tripServiceConnection)
		}
	}

	companion object {
		private const val TAG = "MAIN_VIEW_MODEL"
	}
}
