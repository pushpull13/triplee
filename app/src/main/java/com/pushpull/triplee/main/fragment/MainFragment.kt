package com.pushpull.triplee.main.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.GnssStatus
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.google.android.gms.location.*
import com.google.android.gms.maps.GoogleMapOptions
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.pushpull.triplee.background.TripService
import com.pushpull.triplee.custom.HomeMapView
import com.pushpull.triplee.databinding.MainFragmentBinding
import com.pushpull.triplee.databinding.PermissionRationaleBinding
import com.pushpull.triplee.konstant.ActivityId
import com.pushpull.triplee.konstant.Constant
import com.pushpull.triplee.main.MainActivity
import com.pushpull.triplee.main.MainViewModel
import com.pushpull.triplee.main.MainViewModelFactory
import com.pushpull.triplee.utility.Utility
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import com.pushpull.triplee.R


class MainFragment : Fragment(), TripService.OnReceiveLocationUpdate {

	private val job: Job = Job()
	private val ioScope = CoroutineScope(Dispatchers.IO + job)

	lateinit var mainActivity: MainActivity
	lateinit var viewModel: MainViewModel
	private lateinit var dataBinding: MainFragmentBinding

	lateinit var navController: NavController

	private var isLocationPermissionAsked: Boolean = false
	private var isLocationManagerInitialized: Boolean = false

	private val gnssStatusCallback: GnssStatus.Callback = object : GnssStatus.Callback() {
		@SuppressLint("MissingPermission")
		override fun onSatelliteStatusChanged(status: GnssStatus) {
			super.onSatelliteStatusChanged(status)
			satelliteCount = status.satelliteCount
		}
	}

	lateinit var locationManager: LocationManager
	private var nextLocationSaveTime: Long = System.currentTimeMillis()
	private lateinit var fusedLocationClient: FusedLocationProviderClient
	private val locationRequest: LocationRequest = LocationRequest.create()
		.setInterval(100)
		.setMaxWaitTime(200)
		.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
	private val locationCallback = object : LocationCallback() {
		override fun onLocationResult(locationResult: LocationResult) {
			if (viewModel.isBoundToTripService) {
				if (viewModel.tripService.status != TripService.STATUS.RUNNING) {
					location = if (locationResult.locations.isNotEmpty()) {
						locationResult.locations[0]
					} else {
						locationResult.lastLocation
					}
				}
			}
		}
	}

	private var location: Location? = null
		set(value) {
			field = value
			onLocationUpdateListener?.onLocationUpdate(value, satelliteCount)
			if (value != null) {
				homeMapView.updateCurrentLocation(value)
				if (System.currentTimeMillis() > nextLocationSaveTime) {
					mainActivity.sharedPreferencesAppConfig.edit()
						.putString(Constant.Companion.Settings.LAST_LOCATION.name, "${location!!.latitude}, ${location!!.longitude}").commit()
					nextLocationSaveTime += 10 * 1000
				}
			}
		}
	private var satelliteCount: Int? = null
		set(value) {
			field = value
			onLocationUpdateListener?.onLocationUpdate(location, value)
		}

	lateinit var homeMapView: HomeMapView

	interface OnLocationUpdateListener {
		fun onLocationUpdate(location: Location?, satelliteCount: Int?)
	}

	private var onLocationUpdateListener: OnLocationUpdateListener? = null
	fun onLocationUpdate(listener: OnLocationUpdateListener) {
		onLocationUpdateListener = listener
	}

	lateinit var currentActivity: ActivityId.Companion.ACTIVITY

	@RequiresApi(Build.VERSION_CODES.Q)
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
		mainActivity = requireActivity() as MainActivity
		viewModel = ViewModelProvider(this, MainViewModelFactory(application = mainActivity.application)).get(MainViewModel::class.java)
		dataBinding = DataBindingUtil.inflate(inflater, R.layout.main_fragment, container, false)

		locationManager = requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager

		currentActivity = ActivityId.Companion.ACTIVITY
			.values()[mainActivity.sharedPreferencesAppConfig.getInt(Constant.Companion.Settings.ACTIVITY.name, ActivityId.Companion.ACTIVITY.TREKKING.ordinal)]

		return dataBinding.root
	}

	@RequiresApi(Build.VERSION_CODES.Q)
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		val navHostFragment: NavHostFragment = childFragmentManager.findFragmentById(R.id.main_nav_fragment_comp) as NavHostFragment
		navController = navHostFragment.navController

		dataBinding.mainNavigationBottomBar.apply {
			setupWithNavController(navController)
			setOnNavigationItemReselectedListener { }
		}
	}

	override fun onStart() {
		super.onStart()

		checkAndGetLocationPermission()
		generateGoogleMap()
	}

	private fun generateGoogleMap() {
//		TODO    change initial location
		val lastLatLng = mainActivity.sharedPreferencesAppConfig.getString(Constant.Companion.Settings.LAST_LOCATION.name, null)
		var zoom = 1f
		val latLng = if (lastLatLng != null) {
			val coordinate = lastLatLng.split(",")
			zoom = 15f
			LatLng(coordinate[0].toDouble(), coordinate[1].toDouble())
		} else {
			LatLng(0.0, 0.0)
		}
		val options = GoogleMapOptions()
			.compassEnabled(false)
			.mapToolbarEnabled(false)
			.camera(CameraPosition.fromLatLngZoom(latLng, zoom))

		homeMapView = HomeMapView(requireContext(), options)
	}

	@SuppressLint("MissingPermission")
	private fun initializeLocationCallback() {
		if (!isLocationManagerInitialized) {
			isLocationManagerInitialized = true

			fusedLocationClient = LocationServices.getFusedLocationProviderClient(mainActivity)
			fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper())
			locationManager.registerGnssStatusCallback(gnssStatusCallback, Handler(Looper.myLooper()!!))
		}
	}

	override fun onReceiveLocation(
		location: Location?,
		locationList: MutableMap<Long, List<Double>>?,
		activity: ActivityId.Companion.ACTIVITY,
		pseudoActivity: Int,
		fileList: List<String>,
	) {
		this.location = location
		homeMapView.onReceiveTripLocation(location, locationList,activity, pseudoActivity, fileList)
	}

	fun checkAndGetLocationPermission(): Boolean {
		return if (mainActivity.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
			initializeLocationCallback()
			true
		} else {
			if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
				registerForFineLocationPermissionResult.launch(Manifest.permission.ACCESS_FINE_LOCATION)
			} else {
				requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), FINE_LOCATION_REQUEST_CODE)
			}
			false
		}
	}

	fun checkAndGetActivityPermission() {
		if (mainActivity.checkSelfPermission(Manifest.permission.ACTIVITY_RECOGNITION) == PackageManager.PERMISSION_GRANTED) {
			viewModel.startTrip()
		} else {
			if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
				registerForActivityRecognitionPermissionResult.launch(Manifest.permission.ACTIVITY_RECOGNITION)
			}
		}
	}

	override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults)

		when (requestCode) {
			FINE_LOCATION_REQUEST_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				initializeLocationCallback()
			} else {
				Log.e(TAG, "permission not granted")
				showRationale(
					showIMGood = false,
					message = FINE_LOCATION_RATIONALE_MESSAGE_GPS,
					iconId = R.drawable.map_thumbnail,
					onIMGood = { }) {
					Utility.goToSettings(mainActivity)
				}
			}
		}
	}

	@RequiresApi(Build.VERSION_CODES.Q)
	private val registerForFineLocationPermissionResult = registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
		if (isGranted) {
			initializeLocationCallback()
		} else {
			showRationale(
				showIMGood = false,
				message = FINE_LOCATION_RATIONALE_MESSAGE_GPS,
				iconId = R.drawable.map_thumbnail,
				onIMGood = { }) {
				Utility.goToSettings(mainActivity)
			}
		}
	}

	@RequiresApi(Build.VERSION_CODES.Q)
	private val registerForActivityRecognitionPermissionResult = registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
		if (isGranted) {
			viewModel.startTrip()
		} else {
			showRationale(
				showIMGood = true,
				message = ACTIVITY_RECOGNITION_RATIONALE_MESSAGE,
				iconId = R.drawable.map_thumbnail,
				onIMGood = { viewModel.tripService.startTrip() }) {
				Utility.goToSettings(mainActivity)
			}
		}
	}

	private fun showRationale(
		showIMGood: Boolean,
		message: String,
		iconId: Int,
		onIMGood: () -> Unit,
		onContinue: () -> Unit,
	) {
		val bottomSheet = BottomSheetDialog(mainActivity)
		val permissionRationaleDataBinding: PermissionRationaleBinding =
			DataBindingUtil.inflate(bottomSheet.layoutInflater, R.layout.permission_rationale, null, false)
		bottomSheet.setContentView(permissionRationaleDataBinding.root)

		permissionRationaleDataBinding.permissionRationaleCancelButton.setOnClickListener { bottomSheet.dismiss() }

		permissionRationaleDataBinding.permissionRationaleMessage.text = Html.fromHtml(message, Html.TO_HTML_PARAGRAPH_LINES_INDIVIDUAL)
		permissionRationaleDataBinding.permissionRationaleIcon.setImageDrawable(ResourcesCompat.getDrawable(mainActivity.resources, iconId, null))

		permissionRationaleDataBinding.permissionRationaleButtonContinue.setOnClickListener {
			bottomSheet.dismiss()
			onContinue()
		}

		if (showIMGood) {
			permissionRationaleDataBinding.permissionRationaleButtonIMGood.visibility = View.VISIBLE
			permissionRationaleDataBinding.permissionRationaleButtonIMGood.setOnClickListener {
				onIMGood()
				bottomSheet.dismiss()
			}
		} else {
			permissionRationaleDataBinding.permissionRationaleButtonIMGood.visibility = View.INVISIBLE
		}

		bottomSheet.show()
	}

	companion object {
		const val TAG = "MAIN_FRAGMENT"
		private const val FINE_LOCATION_RATIONALE_MESSAGE_GPS =
			"Allow Triplee <b><i>FOREGROUND LOCATION</i></b> permission to access your current location."
		private const val ACTIVITY_RECOGNITION_RATIONALE_MESSAGE =
			"Allow Triplee <b><i>ACTIVITY RECOGNITION</i></b> permission to better save your journey."

		private const val FINE_LOCATION_REQUEST_CODE = 54369
	}
}
