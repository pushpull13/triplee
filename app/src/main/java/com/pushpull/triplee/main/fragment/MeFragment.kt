package com.pushpull.triplee.main.fragment

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.net.toUri
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.asLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pushpull.triplee.R
import com.pushpull.triplee.cloud.CloudActivity
import com.pushpull.triplee.database.trip.Trip
import com.pushpull.triplee.databinding.MeFragmentBinding
import com.pushpull.triplee.konstant.Constant
import com.pushpull.triplee.konstant.Path
import com.pushpull.triplee.place.PlaceActivity
import com.pushpull.triplee.settings.SettingsActivity
import com.pushpull.triplee.tripDetail.TripDetailActivity
import com.pushpull.triplee.utility.Utility
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit


class MeFragment : Fragment() {
	private val job: Job = Job()
	private val ioScope = CoroutineScope(Dispatchers.IO + job)

	lateinit var mainFragment: MainFragment
	private lateinit var dataBinding: MeFragmentBinding

	private var tripList: List<Trip> = listOf()
	private var viewAdapterDataList: MutableList<ViewAdapterData> = mutableListOf()
	private lateinit var tripRecyclerViewAdapter: TripRecyclerViewAdapter

//	private var status: SyncService.STATUS = SyncService.STATUS.NOT_STARTED
	private var notificationState: NotificationState = NotificationState.NONE

	private var distanceUnit: Constant.Companion.Settings = Constant.Companion.Settings.METRE

	private var totalDistance = 0
	private var totalDuration = 0L

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
		dataBinding = DataBindingUtil.inflate(inflater, R.layout.me_fragment, container, false)
		mainFragment = requireParentFragment().requireParentFragment() as MainFragment

		distanceUnit = Constant.Companion.Settings
			.values()[mainFragment.mainActivity.sharedPreferencesAppConfig
			.getInt(Constant.Companion.Settings.DISTANCE.name, Constant.Companion.Settings.METRE.ordinal)]

		return dataBinding.root
	}

	override fun onStart() {
		super.onStart()

		initializeButton()

		mainFragment.viewModel.tripTable.getAllAsFlow().asLiveData().observe(viewLifecycleOwner) { tripList ->
			this.tripList = tripList
			refreshRecyclerView()
		}

//		mainFragment.viewModel.isBoundToSyncService.observe(viewLifecycleOwner) { isBound ->
//			if (isBound) {
//				mainFragment.viewModel.syncService.statusLiveData.observe(viewLifecycleOwner) { status ->
//					this.status = status
//					refreshNotification()
//				}
//			}
//		}

		initializeRecyclerView()
	}

	private fun initializeButton() {
		dataBinding.meFragmentPlaceButton.setOnClickListener { Intent(requireContext(), PlaceActivity::class.java).apply { startActivity(this) } }
		dataBinding.meFragmentSettingsButton.setOnClickListener {
			Intent(requireContext(), SettingsActivity::class.java).apply {
				startActivity(this)
			}
		}
	}

	private fun initializeRecyclerView() {
		dataBinding.meFragmentTripRecyclerView.layoutManager = LinearLayoutManager(mainFragment.mainActivity)
		tripRecyclerViewAdapter = TripRecyclerViewAdapter()
		dataBinding.meFragmentTripRecyclerView.adapter = tripRecyclerViewAdapter
	}

	private fun refreshRecyclerView() {
		mainFragment.mainActivity.mainScope.launch {
			if (tripList.isNotEmpty()) {
				dataBinding.meFragmentNoTripData.visibility = View.GONE
				dataBinding.meFragmentNoTripDataText.visibility = View.GONE
			} else {
				dataBinding.meFragmentNoTripData.visibility = View.VISIBLE
				dataBinding.meFragmentNoTripDataText.visibility = View.VISIBLE
			}

			totalDistance = 0
			totalDuration = 0L

			val viewAdapterDataList: MutableList<ViewAdapterData> = mutableListOf()
			tripList.forEachIndexed { index, trip ->
				if (!trip.isActive) {
					viewAdapterDataList.add(ViewAdapterData(trip = trip, viewType = TripRecyclerViewType.TRIP_PREVIEW))
					totalDistance += trip.distance
					totalDuration += (trip.endTime!! - trip.primaryKey)
				}
			}
			this@MeFragment.viewAdapterDataList = viewAdapterDataList
			tripRecyclerViewAdapter.notifyDataSetChanged()
			refreshDistanceDuration()
		}
	}

	private fun refreshDistanceDuration() {
		distanceUnit = Constant.Companion.Settings.values()[mainFragment.mainActivity.sharedPreferencesAppConfig
				.getInt(Constant.Companion.Settings.DISTANCE.name, Constant.Companion.Settings.METRE.ordinal)]
		dataBinding.meFragmentDistance.text = Utility.distanceFilter(totalDistance, distanceUnit)

		val totalDays = TimeUnit.MILLISECONDS.toDays(totalDuration)
		val totalHour = TimeUnit.MILLISECONDS.toHours(totalDuration)
		val totalMinute = TimeUnit.MILLISECONDS.toMinutes(totalDuration)

		if (totalDays == 0L) {
			dataBinding.meFragmentDuration.text = "$totalHour Hr ${totalMinute % 60} Min"
		} else {
			dataBinding.meFragmentDuration.text = "$totalDays Days ${totalHour % 24} Hr"
		}
	}

	private val tripDetailActivityCallback = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
		result.data?.getLongExtra("TO_DELETE", -1).apply {
			if (this != -1L) {
				this?.let {
					ioScope.launch {
						mainFragment.viewModel.deleteTrip(it)

						refreshNotification()
					}
				}
			}
		}
	}

	private fun refreshNotification() {
		mainFragment.mainActivity.mainScope.launch {
			when (notificationState) {
				NotificationState.SELECTED_TO_DELETE -> {
					dataBinding.meFragmentNotification.visibility = View.VISIBLE
					var noItemsSelected = 0
					viewAdapterDataList.forEach {
						if (it.isSelected) {
							noItemsSelected++
						}
					}
					dataBinding.meFragmentNotificationSubtitle.visibility = View.GONE
					dataBinding.meFragmentNotificationTitle.text = when (noItemsSelected) {
						0 -> "No trip selected"
						1 -> "1 trip selected"
						else -> "$noItemsSelected trips selected"
					}
					dataBinding.meFragmentNotificationButton.text = "DELETE"
					dataBinding.meFragmentNotificationButton.setOnClickListener { showDeleteDialog() }
					dataBinding.meFragmentNotificationIcon.setImageResource(R.drawable.sticker_delete)
				}
				NotificationState.NONE -> {
					dataBinding.meFragmentNotification.visibility = View.GONE

				}
				NotificationState.SYNCING -> {
//					val lastSyncTimestamp = mainFragment.mainActivity.sharedPreferencesAppConfig.getLong(Constant.Companion.Settings.LAST_SYNC_TIMESTAMP.name, 0)
//
//					dataBinding.meFragmentNotificationTitle.text = mainFragment.mainActivity.auth.currentUser?.email ?: "Not Signed In"
//					dataBinding.meFragmentNotificationButton.text = "SYNC NOW"
//					dataBinding.meFragmentNotificationButton.setOnClickListener {
//						if (mainFragment.mainActivity.auth.currentUser == null) {
//							Intent(requireContext(), CloudActivity::class.java).apply {
//								putExtra(Constant.Companion.Settings.IS_PREMIUM.name, mainFragment.mainActivity.isPremium)
//								startActivity(this)
//							}
//						} else {
////							when (status) {
////								SyncService.STATUS.NOT_STARTED -> mainFragment.viewModel.syncService.refreshSyncerState()
////								SyncService.STATUS.READY -> mainFragment.viewModel.syncService.refreshSyncerState()
////								SyncService.STATUS.SUCCEED -> mainFragment.viewModel.syncService.refreshSyncerState()
////								SyncService.STATUS.FAILED -> mainFragment.viewModel.syncService.refreshSyncerState()
////							}
//						}
//					}
//
////					if (mainFragment.mainActivity.auth.currentUser != null) {
////						when (status) {
////							SyncService.STATUS.NOT_STARTED -> {
////								dataBinding.meFragmentNotificationIcon.setImageResource(R.drawable.sticker_sync_wait)
////								dataBinding.meFragmentNotificationSubtitle.text = if (lastSyncTimestamp == 0L){
////									"Not synced"
////								} else {
////									Utility.timestampToPretty(lastSyncTimestamp)
////								}
////								dataBinding.meFragmentNotificationSubtitle.visibility = View.VISIBLE
////								dataBinding.meFragmentNotificationButton.visibility = View.VISIBLE
////							}
////							SyncService.STATUS.NOT_SIGNED_IN -> {
////								dataBinding.meFragmentNotificationIcon.setImageResource(R.drawable.sticker_sync_unavailable)
////								dataBinding.meFragmentNotificationTitle.text = "Not Signed In"
////								dataBinding.meFragmentNotificationSubtitle.text = "Sign In to backup your journey"
////								dataBinding.meFragmentNotificationSubtitle.visibility = View.GONE
////								dataBinding.meFragmentNotificationButton.visibility = View.VISIBLE
////							}
////							SyncService.STATUS.NO_PERMISSION -> {
////								dataBinding.meFragmentNotificationIcon.setImageResource(R.drawable.sticker_sync_unavailable)
////								dataBinding.meFragmentNotificationSubtitle.text = "Drive not connected"
////								dataBinding.meFragmentNotificationSubtitle.visibility = View.VISIBLE
////								dataBinding.meFragmentNotificationButton.visibility = View.VISIBLE
////							}
////							SyncService.STATUS.NO_NETWORK -> {
////								dataBinding.meFragmentNotificationIcon.setImageResource(R.drawable.sticker_sync_failed)
////								dataBinding.meFragmentNotificationSubtitle.text = "Network unavailable"
////								dataBinding.meFragmentNotificationSubtitle.visibility = View.VISIBLE
////								dataBinding.meFragmentNotificationButton.visibility = View.VISIBLE
////							}
////							SyncService.STATUS.READY -> {
////								dataBinding.meFragmentNotificationIcon.setImageResource(R.drawable.sticker_sync_ready)
////								dataBinding.meFragmentNotificationSubtitle.text = if (lastSyncTimestamp == 0L){
////									"Not synced"
////								} else {
////									Utility.timestampToPretty(lastSyncTimestamp)
////								}
////								dataBinding.meFragmentNotificationSubtitle.visibility = View.VISIBLE
////								dataBinding.meFragmentNotificationButton.visibility = View.VISIBLE
////							}
////							SyncService.STATUS.RUNNING -> {
////								dataBinding.meFragmentNotificationIcon.setImageResource(R.drawable.sticker_sync_ready)
////								dataBinding.meFragmentNotificationSubtitle.text = "Syncing..."
////								dataBinding.meFragmentNotificationSubtitle.visibility = View.VISIBLE
////								dataBinding.meFragmentNotificationButton.visibility = View.GONE
////							}
////							SyncService.STATUS.UPLOAD -> {
////								dataBinding.meFragmentNotificationIcon.setImageResource(R.drawable.sticker_sync_upload)
////								dataBinding.meFragmentNotificationSubtitle.text = "Uploading..."
////								dataBinding.meFragmentNotificationSubtitle.visibility = View.VISIBLE
////								dataBinding.meFragmentNotificationButton.visibility = View.GONE
////							}
////							SyncService.STATUS.DOWNLOAD -> {
////								dataBinding.meFragmentNotificationIcon.setImageResource(R.drawable.sticker_sync_download)
////								dataBinding.meFragmentNotificationSubtitle.text = "Downloading..."
////								dataBinding.meFragmentNotificationSubtitle.visibility = View.VISIBLE
////								dataBinding.meFragmentNotificationButton.visibility = View.GONE
////							}
////							SyncService.STATUS.SUCCEED -> {
////								dataBinding.meFragmentNotificationIcon.setImageResource(R.drawable.sticker_sync_succeed)
////								dataBinding.meFragmentNotificationSubtitle.text = "Sync complete"
////								dataBinding.meFragmentNotificationSubtitle.visibility = View.VISIBLE
////								dataBinding.meFragmentNotificationButton.visibility = View.VISIBLE
////							}
////							SyncService.STATUS.FAILED -> {
////								dataBinding.meFragmentNotificationIcon.setImageResource(R.drawable.sticker_sync_failed)
////								dataBinding.meFragmentNotificationSubtitle.text = "Sync failed"
////								dataBinding.meFragmentNotificationSubtitle.visibility = View.VISIBLE
////								dataBinding.meFragmentNotificationButton.visibility = View.VISIBLE
////							}
////						}
////					} else {
////						dataBinding.meFragmentNotificationIcon.setImageResource(R.drawable.sticker_sync_unavailable)
////						dataBinding.meFragmentNotificationTitle.text = "Not Signed In"
////						dataBinding.meFragmentNotificationSubtitle.text = "Sign In to backup"
////						dataBinding.meFragmentNotificationSubtitle.visibility = View.VISIBLE
////						dataBinding.meFragmentNotificationButton.visibility = View.VISIBLE
////					}
				}
			}
		}
	}

	private fun showDeleteDialog() {
		AlertDialog.Builder(requireContext()).apply {
			setTitle("Delete Trip")
			setMessage("Are you sure you want to delete selected trips? You won't be able to recover it afterwards.")
			setPositiveButton("DELETE") { _, _ ->
				viewAdapterDataList.forEach {
					if (it.isSelected) {
						it.trip?.let { it1 -> mainFragment.viewModel.deleteTrip(it1.primaryKey) }
					}
				}
				notificationState = NotificationState.NONE
				refreshNotification()
			}
			setNegativeButton("CANCEL") { _, _ ->
				notificationState = NotificationState.NONE
				refreshNotification()
			}
			show()
		}
	}

	enum class NotificationState {
		SELECTED_TO_DELETE,
		SYNCING,
		NONE
	}

	data class ViewAdapterData(
		val trip: Trip?,
		val viewType: TripRecyclerViewType,
		var isSelected: Boolean = false,
	)

	enum class TripRecyclerViewType {
		NULL,
		TRIP_PREVIEW,
	}

	private inner class TripRecyclerViewAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

		private inner class ViewHolderNull(view: View) : RecyclerView.ViewHolder(view)

		private inner class ViewHolderTripPreview(view: View) : RecyclerView.ViewHolder(view) {
			val layout: LinearLayout = view.findViewById(R.id.trip_fragment_recycler_view_holder)
			val thumbnail: ImageView = view.findViewById(R.id.trip_fragment_recycler_view_thumbnail)
			val title: TextView = view.findViewById(R.id.trip_fragment_recycler_view_title)
			val time: TextView = view.findViewById(R.id.trip_fragment_recycler_view_time)
			val duration: TextView = view.findViewById(R.id.trip_fragment_recycler_view_duration)
			val distance: TextView = view.findViewById(R.id.trip_fragment_recycler_view_distance)

			fun bind(position: Int) {
				val trip = this@MeFragment.viewAdapterDataList[position].trip

				val options = BitmapFactory.Options()
				options.inPreferredConfig = Bitmap.Config.ARGB_8888
				"${Path.TRIP_FOLDER_NAME}/thumbnail/${trip!!.primaryKey}.png".let {
					if(File(it).exists()) {
						val bitmap = BitmapFactory.decodeFile("${Path.TRIP_FOLDER_NAME}/thumbnail/${trip.primaryKey}.png", options)
						thumbnail.setImageBitmap(bitmap)
					} else {
						Utility.getMapSnapShot(requireContext(), trip)
						thumbnail.setImageResource(R.drawable.map_thumbnail)
					}
				}

				title.text = trip.title
				time.text = Utility.timestampToPretty(trip.primaryKey)
				duration.text = Utility.durationFilter(trip.endTime!! - trip.primaryKey, false)
				distance.text = Utility.distanceFilter(trip.distance, distanceUnit)

				fun refreshLayout() {
					if (this@MeFragment.viewAdapterDataList[position].isSelected) {
						Color.alpha(0)
						val colorGamma = mainFragment.mainActivity.localAppTheme.colorOnGamma
						layout.setBackgroundColor(Color.argb(31, Color.red(colorGamma), Color.green(colorGamma), Color.blue(colorGamma)))
					} else {
						layout.setBackgroundResource(mainFragment.mainActivity.localAppTheme.roundedCornerRipple)
					}
				}

				layout.setOnClickListener {
					if (notificationState == NotificationState.SELECTED_TO_DELETE) {
						this@MeFragment.viewAdapterDataList[position].isSelected = !this@MeFragment.viewAdapterDataList[position].isSelected
						refreshLayout()
						refreshNotification()
					} else {
						Intent(mainFragment.mainActivity, TripDetailActivity::class.java).apply {
							putExtra(Constant.Companion.Settings.PRIMARY_KEY.name, trip.primaryKey)
							tripDetailActivityCallback.launch(this)
						}
					}
				}
				layout.setOnLongClickListener {
					this@MeFragment.viewAdapterDataList[position].isSelected = !this@MeFragment.viewAdapterDataList[position].isSelected
					refreshLayout()
					notificationState = NotificationState.SELECTED_TO_DELETE
					refreshNotification()
					true
				}
				refreshLayout()
			}
		}

		override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
			return when (viewType) {
				TripRecyclerViewType.TRIP_PREVIEW.ordinal ->
					ViewHolderTripPreview(LayoutInflater.from(parent.context).inflate(R.layout.trip_fragment_preview_holder, parent, false))
				else -> ViewHolderTripPreview(LayoutInflater.from(parent.context)
					.inflate(R.layout.trip_fragment_preview_holder, parent, false))
			}
		}

		override fun getItemCount(): Int {
			return viewAdapterDataList.size
		}

		override fun getItemViewType(position: Int): Int {
			return viewAdapterDataList[position].viewType.ordinal
		}

		override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
			when (viewAdapterDataList[position].viewType) {
				TripRecyclerViewType.TRIP_PREVIEW -> (holder as ViewHolderTripPreview).bind(position)
			}
		}
	}

	companion object {
		const val TAG = "ME_FRAGMENT"
		var NOTIFICATION_DELAY = 10000L
			get() {
				return kotlin.math.abs((Random().nextLong() % 5000)) + 10000
			}
	}
}
