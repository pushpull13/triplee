package com.pushpull.triplee.main.fragment

import android.Manifest
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.MainThread
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.pushpull.triplee.R
import com.pushpull.triplee.background.TripService
import com.pushpull.triplee.custom.ActivitySelectionView
import com.pushpull.triplee.custom.HomeMapView
import com.pushpull.triplee.custom.LiveInfo
import com.pushpull.triplee.custom.MapStyleBottomSheet
import com.pushpull.triplee.database.place.Place
import com.pushpull.triplee.databinding.ActivitySelectionDialogBinding
import com.pushpull.triplee.databinding.HomeFragmentBinding
import com.pushpull.triplee.konstant.ActivityId
import com.pushpull.triplee.konstant.Constant
import com.pushpull.triplee.konstant.Constant.Companion.Settings
import com.pushpull.triplee.main.MainViewModel
import com.pushpull.triplee.utility.Utility
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class HomeFragment : Fragment(),
	View.OnClickListener,
	View.OnLongClickListener,
	HomeMapView.OnMapReadyListener,
	HomeMapView.OnCompassValueChangeListener,
	MainFragment.OnLocationUpdateListener,
	MainViewModel.OnBoundTripServiceListener,
	TripService.OnTripStatusChange,
	android.location.LocationListener {
	private val ioScope = CoroutineScope(Dispatchers.IO)
	private val mainScope = CoroutineScope(Dispatchers.Main)
	private val objectMapper = ObjectMapper().registerModule(KotlinModule())

	private lateinit var mainFragment: MainFragment
	private lateinit var dataBinding: HomeFragmentBinding

	private lateinit var homeMapView: HomeMapView
	private lateinit var geoCoder: Geocoder

	private var currentLocation: Location? = null
		set(value) {
			field = value
			if (value != null) {
				refreshLiveInfoLocationData(value)
				if (!isCentredOnce) {
					zoomOnCurrentLocation()
				}
				followLocation()
			}
		}

	private var satelliteCount: Int? = null
		set(value) {
			field = value

			dataBinding.homeFragmentSatelliteText.text = "${value ?: "Getting signal..."}"

			when (value) {
				0 -> dataBinding.homeFragmentSatelliteIcon.setColorFilter(Color.argb(255, 247, 14, 14))
				1 -> dataBinding.homeFragmentSatelliteIcon.setColorFilter(Color.argb(255, 247, 14, 14))
				2 -> dataBinding.homeFragmentSatelliteIcon.setColorFilter(Color.argb(255, 247, 14, 14))
				3 -> dataBinding.homeFragmentSatelliteIcon.setColorFilter(Color.argb(255, 247, 14, 14))
				4 -> dataBinding.homeFragmentSatelliteIcon.setColorFilter(Color.argb(255, 202, 151, 74))
				5 -> dataBinding.homeFragmentSatelliteIcon.setColorFilter(Color.argb(255, 202, 151, 74))
				6 -> dataBinding.homeFragmentSatelliteIcon.setColorFilter(Color.argb(255, 19, 151, 89))
				else -> dataBinding.homeFragmentSatelliteIcon.setColorFilter(Color.argb(255, 19, 151, 89))
			}
		}

	override fun onLocationChanged(location: Location) {}

	override fun onProviderEnabled(provider: String) {
		super.onProviderEnabled(provider)
		dataBinding.homeFragmentGpsDisabledCard.visibility = View.GONE
	}

	override fun onProviderDisabled(provider: String) {
		super.onProviderDisabled(provider)
		dataBinding.homeFragmentGpsDisabledCard.visibility = View.VISIBLE
	}

	private var isCentredOnce: Boolean = false
	private var followLocation: Boolean = true
	private var place: Place? = null

	private var coordinateUnit: Settings = Settings.DD
	private var distanceUnit: Settings = Settings.METRE

	private val liveInfoTripDataHandler = Handler(Looper.getMainLooper())
	private val liveInfoTripDataRunnable = object : Runnable {
		override fun run() {
			if (isVisible && mainFragment.viewModel.isBoundToTripService && mainFragment.viewModel.tripService.status == TripService.STATUS.RUNNING) {
				val liveTripInfoTripData = LiveInfo.LiveInfoTripData(
					startTimestamp = mainFragment.viewModel.tripService.primaryKey,
					distance = mainFragment.viewModel.tripService.distance.toInt())
				dataBinding.homeFragmentLiveTripInfo.updateTripData(liveTripInfoTripData, distanceUnit)
				liveInfoTripDataHandler.postDelayed(this, 1000)
			}
		}
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		mainFragment = requireParentFragment().requireParentFragment() as MainFragment

		geoCoder = Geocoder(requireContext())
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
		dataBinding = DataBindingUtil.inflate(inflater, R.layout.home_fragment, container, false)
		dataBinding.homeFragment = this

		return dataBinding.root
	}

	override fun onResume() {
		super.onResume()

		if (requireContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
			mainFragment.locationManager.removeUpdates(this)
			mainFragment.locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0f, this)
		}

		homeMapView = mainFragment.homeMapView
		homeMapView.onMapReady(this)
		homeMapView.onCompassValueChange(this)
		mainFragment.onLocationUpdate(this)
		mainFragment.viewModel.setOnBoundTripServiceListener(this)

		coordinateUnit = Settings.values()[mainFragment.mainActivity.sharedPreferencesAppConfig.getInt(Settings.COORDINATE.name, Settings.DD.ordinal)]
		distanceUnit = Settings.values()[mainFragment.mainActivity.sharedPreferencesAppConfig.getInt(Settings.DISTANCE.name, Settings.METRE.ordinal)]

		initUi()

		isCentredOnce = false
		homeMapView.invokeMapReady()
	}

	override fun onStop() {
		super.onStop()
		dataBinding.homeFragmentGoogleMapLayout.removeAllViews()
		mainFragment.locationManager.removeUpdates(this)
	}

	private fun initUi() {
		dataBinding.homeFragmentCurrentActivityIcon.setImageResource(ActivityId.ACTIVITY_ID_RES_MAP[mainFragment.currentActivity]!!)
		dataBinding.homeFragmentCurrentActivityName.text = ActivityId.ACTIVITY_ID_NAME_MAP[mainFragment.currentActivity]
	}

	override fun onClick(view: View) {
		when (view.id) {
			dataBinding.homeFragmentMapStyleButton.id -> switchMapStyle()
			dataBinding.homeFragmentCurrentActivity.id -> switchActivity()
			dataBinding.homeFragmentZoomOutButton.id -> homeMapView.zoomOnCurrentTrip()
			dataBinding.homeFragmentGpsButton.id -> zoomOnCurrentLocation(true)
			dataBinding.homeFragmentControlTrip.id -> controlTrip()
			dataBinding.homeFragmentSaveTripButton.id -> {
				val tripTitle = dataBinding.homeFragmentTripSaveTitle.text.toString()
				mainFragment.viewModel.tripService.saveTrip(tripTitle)
			}
			dataBinding.homeFragmentDiscardTripButton.id -> {
				mainFragment.viewModel.tripService.discardTrip()
			}
			dataBinding.homeFragmentCopyPlaceCoordinateButton.id -> {
				val clipboard: ClipboardManager = requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
				val text = "${homeMapView.currentPlaceMarker.position.latitude}, ${homeMapView.currentPlaceMarker.position.longitude}"
				val clip = ClipData.newPlainText("Coordinate", text)
				clipboard.setPrimaryClip(clip)
			}
			dataBinding.homeFragmentPlaceCopyAddressButton.id -> {
				val clipboard: ClipboardManager = requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
				val text = dataBinding.homeFragmentPlaceAddress.text
				val clip = ClipData.newPlainText("Coordinate", text)
				clipboard.setPrimaryClip(clip)
			}
			dataBinding.homeFragmentClosePlaceCardButton.id -> hidePlaceView()
			dataBinding.homeFragmentPlaceEditNameButton.id -> editPlaceName()
			dataBinding.homeFragmentPlaceCancelEditNameButton.id -> cancelOrClearEditPlaceName()
			dataBinding.homeFragmentPlaceDoneEditNameButton.id -> doneEditPlaceName()
			dataBinding.homeFragmentPlaceMarkButton.id -> markPlace()
			dataBinding.homeFragmentPlaceSaveButton.id -> savePlace(false)
			dataBinding.homeFragmentPlaceToggleFavouriteButton.id -> toggleFavouritePlace()
			dataBinding.homeFragmentPlaceRemoveButton.id -> removePlace()
			dataBinding.homeFragmentPlaceCardColor0.id -> setPlaceColor(0)
			dataBinding.homeFragmentPlaceCardColor1.id -> setPlaceColor(1)
			dataBinding.homeFragmentPlaceCardColor2.id -> setPlaceColor(2)
			dataBinding.homeFragmentPlaceCardColor3.id -> setPlaceColor(3)
			dataBinding.homeFragmentPlaceCardColor4.id -> setPlaceColor(4)
			dataBinding.homeFragmentPlaceCardColor5.id -> setPlaceColor(5)
			dataBinding.homeFragmentPlaceCardColor6.id -> setPlaceColor(6)
		}
	}

	override fun onLongClick(view: View): Boolean {
		return when (view.id) {
			dataBinding.homeFragmentGpsButton.id -> {
				zoomOnCurrentLocation(true)
				dataBinding.homeFragmentGpsButton.setColorFilter(mainFragment.mainActivity.localAppTheme.colorPrimary)
				followLocation = true
				true
			}
			else -> false
		}
	}

	override fun onMapReady(isMapReady: Boolean) {
		if (isMapReady) {
			if (dataBinding.homeFragmentGoogleMapLayout.childCount == 0) {
				dataBinding.homeFragmentGoogleMapLayout.addView(homeMapView)
			}
			initializeMapEvents()
			mainFragment.viewModel.checkServiceBound()
		}
	}

	override fun onCompassValueChange(azimuth: Float) {
		dataBinding.homeFragmentGpsButton.rotation = azimuth
	}

	override fun onLocationUpdate(location: Location?, satelliteCount: Int?) {
		this.currentLocation = location
		this.satelliteCount = satelliteCount
	}

	override fun onBoundTripServiceListener(isBound: Boolean) {
		if (isBound) {
			mainFragment.viewModel.tripService.onReceiveLocation(mainFragment)
			mainFragment.viewModel.tripService.onTripStatusChange(this)
			updateView()
		}
	}

	override fun onTripStatusChange() {
		updateView()
	}

	private fun updateView() {
		mainScope.launch {
			homeMapView.onTripStatusChange(mainFragment.viewModel.tripService.status, mainFragment.viewModel.tripService.primaryKey)

			when (mainFragment.viewModel.tripService.status) {
				TripService.STATUS.NOT_STARTED -> {
				}
				TripService.STATUS.PROCESSING -> {
					dataBinding.homeFragmentControlTrip.show()
					dataBinding.homeFragmentBottomLayoutSave.visibility = View.GONE
				}
				TripService.STATUS.CRASH_RECOVERY -> {
					dataBinding.homeFragmentControlTrip.hide()
					dataBinding.homeFragmentBottomLayoutSave.visibility = View.VISIBLE
				}
				TripService.STATUS.READY -> {
					dataBinding.homeFragmentControlTrip.text = requireContext().getString(R.string.start)
					dataBinding.homeFragmentControlTrip.setIconResource(R.drawable.ic_record)

					dataBinding.homeFragmentLiveTripInfo.updateTripData(LiveInfo.LiveInfoTripData(null, null), distanceUnit)
				}
				TripService.STATUS.RUNNING -> {
					dataBinding.homeFragmentControlTrip.text = requireContext().getString(R.string.end)
					dataBinding.homeFragmentControlTrip.setIconResource(R.drawable.ic_stop)

					dataBinding.homeFragmentZoomOutButton.visibility = View.VISIBLE

					liveInfoTripDataHandler.post(liveInfoTripDataRunnable)
				}
				TripService.STATUS.PAUSED -> {
				}
				TripService.STATUS.STOPPED -> {
					dataBinding.homeFragmentControlTrip.hide()
					dataBinding.homeFragmentBottomLayoutSave.visibility = View.VISIBLE

					dataBinding.homeFragmentZoomOutButton.visibility = View.GONE
				}
				TripService.STATUS.SUCCEED -> {
				}
				TripService.STATUS.FAILED -> {
				}
			}
		}
	}

	private fun controlTrip() {
		if (mainFragment.viewModel.isBoundToTripService) {
			when (mainFragment.viewModel.tripService.status) {
				TripService.STATUS.NOT_STARTED -> {
				}
				TripService.STATUS.PROCESSING -> {
				}
				TripService.STATUS.CRASH_RECOVERY -> {
				}
				TripService.STATUS.READY -> {
					if (checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
						if (checkSelfPermission(requireContext(), Manifest.permission.ACTIVITY_RECOGNITION) == PackageManager.PERMISSION_GRANTED) {
							mainFragment.viewModel.tripService.startTrip()
						} else {
							mainFragment.checkAndGetActivityPermission()
						}
					} else {
						mainFragment.checkAndGetLocationPermission()
					}
				}
				TripService.STATUS.RUNNING -> mainFragment.viewModel.tripService.endTrip()
				TripService.STATUS.PAUSED -> {
				}
				TripService.STATUS.STOPPED -> {
				}
				TripService.STATUS.SUCCEED -> {
				}
				TripService.STATUS.FAILED -> {
				}
			}

		}
	}

	private fun switchMapStyle() {
		val mapStyleSwitchDialog = MapStyleBottomSheet(requireContext(), homeMapView)

		dataBinding.homeFragmentLiveTripInfo.visibility = View.GONE
		dataBinding.homeFragmentMapStyleButton.visibility = View.GONE
		dataBinding.homeFragmentBottomLayoutInfo.visibility = View.GONE
		dataBinding.homeFragmentControlTrip.visibility = View.GONE
		dataBinding.homeFragmentGpsButton.visibility = View.GONE
		dataBinding.homeFragmentZoomOutButton.visibility = View.GONE
		dataBinding.homeFragmentCurrentActivity.visibility = View.GONE
		mapStyleSwitchDialog.setOnDismissListener {
			dataBinding.homeFragmentLiveTripInfo.visibility = View.VISIBLE
			dataBinding.homeFragmentMapStyleButton.visibility = View.VISIBLE
			dataBinding.homeFragmentBottomLayoutInfo.visibility = View.VISIBLE
			dataBinding.homeFragmentControlTrip.visibility = View.VISIBLE
			dataBinding.homeFragmentGpsButton.visibility = View.VISIBLE
			dataBinding.homeFragmentZoomOutButton.visibility = View.VISIBLE
			dataBinding.homeFragmentCurrentActivity.visibility = View.VISIBLE
		}
		mapStyleSwitchDialog.show()
	}

	private fun switchActivity() {
		val activitySelectionDialog = BottomSheetDialog(requireContext())
		val activitySelectionDataBinding: ActivitySelectionDialogBinding =
			DataBindingUtil.inflate(activitySelectionDialog.layoutInflater, R.layout.activity_selection_dialog, null, false)
		activitySelectionDialog.setContentView(activitySelectionDataBinding.root)

		val activityView = ActivitySelectionView(requireContext())

		activitySelectionDataBinding.activitySelectionDialogLayout.removeAllViews()
		activitySelectionDataBinding.activitySelectionDialogLayout.addView(activityView)

		fun frag0() {
			activityView.updateView(ActivityId.ACTIVITY_META_ID_LIST, mainFragment.currentActivity)
			activitySelectionDialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
		}

		fun frag1(activityId: ActivityId.Companion.ACTIVITY) {
			when (activityId) {
				ActivityId.Companion.ACTIVITY.POPULAR -> activityView.updateView(ActivityId.ACTIVITY_POPULAR_ID_LIST, mainFragment.currentActivity)
				ActivityId.Companion.ACTIVITY.MOUNTAIN -> activityView.updateView(ActivityId.ACTIVITY_MOUNTAIN_ID_LIST, mainFragment.currentActivity)
				ActivityId.Companion.ACTIVITY.FOREST -> activityView.updateView(ActivityId.ACTIVITY_FOREST_ID_LIST, mainFragment.currentActivity)
				ActivityId.Companion.ACTIVITY.CITY_AND_ROAD -> activityView.updateView(ActivityId.ACTIVITY_CITY_AND_ROADS_ID_LIST, mainFragment.currentActivity)
				ActivityId.Companion.ACTIVITY.WATER -> activityView.updateView(ActivityId.ACTIVITY_WATER_ID_LIST, mainFragment.currentActivity)
				ActivityId.Companion.ACTIVITY.AIR -> activityView.updateView(ActivityId.ACTIVITY_AIR_ID_LIST, mainFragment.currentActivity)
				ActivityId.Companion.ACTIVITY.WINTER -> activityView.updateView(ActivityId.ACTIVITY_WINTER_ID_LIST, mainFragment.currentActivity)
			}
			activitySelectionDialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
		}

		activityView.setOnClickActivity(object : ActivitySelectionView.OnClickActivityListener {
			override fun onClickActivity(activity: ActivityId.Companion.ACTIVITY) {
				when (activity) {
					ActivityId.Companion.ACTIVITY.POPULAR -> frag1(activity)
					ActivityId.Companion.ACTIVITY.MOUNTAIN -> frag1(activity)
					ActivityId.Companion.ACTIVITY.FOREST -> frag1(activity)
					ActivityId.Companion.ACTIVITY.CITY_AND_ROAD -> frag1(activity)
					ActivityId.Companion.ACTIVITY.WATER -> frag1(activity)
					ActivityId.Companion.ACTIVITY.AIR -> frag1(activity)
					ActivityId.Companion.ACTIVITY.WINTER -> frag1(activity)
					else -> {
						mainFragment.viewModel.switchActivity(activity)
						mainFragment.mainActivity.sharedPreferencesAppConfig
							.edit()
							.putInt(Constant.Companion.Settings.ACTIVITY.name, activity.ordinal)
							.commit()
						mainFragment.currentActivity = activity
						dataBinding.homeFragmentCurrentActivityIcon.setImageResource(ActivityId.ACTIVITY_ID_RES_MAP[mainFragment.currentActivity]!!)
						dataBinding.homeFragmentCurrentActivityName.text = ActivityId.ACTIVITY_ID_NAME_MAP[mainFragment.currentActivity]
						activitySelectionDialog.dismiss()
					}
				}
			}
		})

		activitySelectionDataBinding.activitySelectionDialogBack.setOnClickListener {
			frag0()
		}

		frag0()

		activitySelectionDialog.show()
	}

	@MainThread
	private fun showPlaceView(isNew: Boolean = true) {
		dataBinding.homeFragmentPlaceCard.visibility = View.VISIBLE
		dataBinding.homeFragmentMapStyleButton.visibility = View.GONE

		if (isNew) {
			dataBinding.homeFragmentPlaceLoading.visibility = View.VISIBLE
			dataBinding.homeFragmentPlaceLoaded.visibility = View.GONE
			dataBinding.homeFragmentPlaceLoading.playAnimation()

			dataBinding.homeFragmentPlaceNameLayout.visibility = View.GONE
			dataBinding.homeFragmentPlaceColorLayout.visibility = View.GONE

			dataBinding.homeFragmentPlaceLatitude.text = "${place!!.latitude.round(6)}"
			dataBinding.homeFragmentPlaceLongitude.text = "${place!!.longitude.round(6)}"

			try {
				geoCoder.getFromLocation(place!!.latitude, place!!.longitude, 1).apply {
					dataBinding.homeFragmentPlaceAddress.text = this[0].getAddressLine(0).replace(", ", ",\n")
					dataBinding.homeFragmentPlaceLoading.visibility = View.GONE
					dataBinding.homeFragmentPlaceLoaded.visibility = View.VISIBLE
					dataBinding.homeFragmentPlaceLoading.pauseAnimation()

					dataBinding.homeFragmentPlaceMarkButton.visibility = View.VISIBLE
					dataBinding.homeFragmentPlaceSaveButton.visibility = View.GONE
					dataBinding.homeFragmentPlaceButtonEditLayout.visibility = View.GONE

					place!!.address = this[0].getAddressLine(0)
				}
			} catch (exception: Exception) {
				dataBinding.homeFragmentPlaceAddress.text = requireContext().getString(R.string.error)
			}
		} else {
			dataBinding.homeFragmentPlaceLoading.visibility = View.GONE
			dataBinding.homeFragmentPlaceLoaded.visibility = View.VISIBLE
			dataBinding.homeFragmentPlaceLoading.pauseAnimation()

			dataBinding.homeFragmentPlaceLatitude.text = "${place!!.latitude.round(6)}"
			dataBinding.homeFragmentPlaceLongitude.text = "${place!!.longitude.round(6)}"

			dataBinding.homeFragmentPlaceMarkButton.visibility = View.GONE
			dataBinding.homeFragmentPlaceSaveButton.visibility = View.GONE
			dataBinding.homeFragmentPlaceButtonEditLayout.visibility = View.VISIBLE

			dataBinding.homeFragmentPlaceNameLayout.visibility = View.VISIBLE
			dataBinding.homeFragmentPlaceNameEditText.isEnabled = false
			dataBinding.homeFragmentPlaceNameEditText.inputType = EditorInfo.TYPE_NULL

			dataBinding.homeFragmentPlaceEditNameButton.visibility = View.VISIBLE
			dataBinding.homeFragmentPlaceCancelEditNameButton.visibility = View.GONE
			dataBinding.homeFragmentPlaceDoneEditNameButton.visibility = View.GONE
			dataBinding.homeFragmentPlaceColorLayout.visibility = View.VISIBLE

			if (place!!.isFavourite) {
				dataBinding.homeFragmentPlaceFavouriteIcon.setImageResource(R.drawable.ic_heart_)
				dataBinding.homeFragmentPlaceFavouriteIcon.setColorFilter(0)
			} else {
				dataBinding.homeFragmentPlaceFavouriteIcon.setImageResource(R.drawable.ic_heart)
				dataBinding.homeFragmentPlaceFavouriteIcon.setColorFilter(mainFragment.mainActivity.localAppTheme.colorPrimary)
			}

			dataBinding.homeFragmentPlaceNameEditText.setText(place!!.name)
			dataBinding.homeFragmentPlaceAddress.text = place!!.address
			setPlaceColor(place!!.colorTag)
		}
	}

	private fun hidePlaceView() {
		dataBinding.homeFragmentPlaceCard.visibility = View.GONE
		dataBinding.homeFragmentPlaceLoading.visibility = View.VISIBLE
		dataBinding.homeFragmentMapStyleButton.visibility = View.VISIBLE

		try {
			homeMapView.currentPlaceMarker.remove()
		} catch (exception: Exception) {
			homeMapView.googleMap?.clear()
			homeMapView.refreshPlaceMarkers()
		}
	}

	private fun editPlaceName() {
		dataBinding.homeFragmentPlaceNameEditText.isEnabled = true
		dataBinding.homeFragmentPlaceNameEditText.inputType = EditorInfo.TYPE_TEXT_FLAG_AUTO_COMPLETE or EditorInfo.TYPE_TEXT_FLAG_AUTO_CORRECT
		dataBinding.homeFragmentPlaceNameEditText.requestFocus()
		val imm: InputMethodManager = requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
		imm.showSoftInput(dataBinding.homeFragmentPlaceNameEditText, InputMethodManager.SHOW_IMPLICIT)

		dataBinding.homeFragmentPlaceEditNameButton.visibility = View.GONE
		dataBinding.homeFragmentPlaceDoneEditNameButton.visibility = View.VISIBLE
	}

	private fun cancelOrClearEditPlaceName() {
		dataBinding.homeFragmentPlaceNameEditText.setText("")
	}

	private fun doneEditPlaceName() {
		savePlace(true)
	}

	private fun markPlace() {
		dataBinding.homeFragmentPlaceNameEditText.requestFocus()
		val imm: InputMethodManager = requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
		imm.showSoftInput(dataBinding.homeFragmentPlaceNameEditText, InputMethodManager.SHOW_IMPLICIT)

		dataBinding.homeFragmentPlaceColorLayout.visibility = View.VISIBLE
		dataBinding.homeFragmentPlaceMarkButton.visibility = View.GONE
		dataBinding.homeFragmentPlaceSaveButton.visibility = View.VISIBLE
		dataBinding.homeFragmentPlaceNameLayout.visibility = View.VISIBLE
		dataBinding.homeFragmentPlaceEditNameButton.visibility = View.GONE

		dataBinding.homeFragmentPlaceNameEditText.isEnabled = true
		dataBinding.homeFragmentPlaceNameEditText.inputType = EditorInfo.TYPE_TEXT_FLAG_AUTO_COMPLETE or EditorInfo.TYPE_TEXT_FLAG_AUTO_CORRECT

		dataBinding.homeFragmentPlaceDoneEditNameButton.visibility = View.GONE
		dataBinding.homeFragmentPlaceCancelEditNameButton.visibility = View.VISIBLE

		val addressList = dataBinding.homeFragmentPlaceAddress.text.split(",")
		if (addressList.isNotEmpty()) {
			dataBinding.homeFragmentPlaceNameEditText.setText("")
			dataBinding.homeFragmentPlaceNameEditText.hint = addressList[0]
		} else {
			dataBinding.homeFragmentPlaceNameEditText.setText("")
			dataBinding.homeFragmentPlaceNameEditText.hint = "Name"
		}

		setPlaceColor(place!!.colorTag)
	}

	private fun savePlace(isEdited: Boolean) {
		ioScope.launch {
			place!!.name = if (dataBinding.homeFragmentPlaceNameEditText.text.toString().isEmpty()) {
				dataBinding.homeFragmentPlaceNameEditText.hint.toString()
			} else {
				dataBinding.homeFragmentPlaceNameEditText.text.toString()
			}

			if (isEdited) {
				mainFragment.viewModel.placeTable.update(place!!)
			} else {
				mainFragment.viewModel.placeTable.insert(place!!)
			}

			mainScope.launch {
				dataBinding.homeFragmentPlaceNameEditText.isEnabled = false
				dataBinding.homeFragmentPlaceNameEditText.inputType = EditorInfo.TYPE_NULL

				showPlaceView(false)
			}
		}
	}

	private fun setPlaceColor(colorTag: Int) {
		place?.colorTag = colorTag
		val tint = when (colorTag) {
			0 -> requireContext().resources.getColor(R.color.placeColor0, null)
			1 -> requireContext().resources.getColor(R.color.placeColor1, null)
			2 -> requireContext().resources.getColor(R.color.placeColor2, null)
			3 -> requireContext().resources.getColor(R.color.placeColor3, null)
			4 -> requireContext().resources.getColor(R.color.placeColor4, null)
			5 -> requireContext().resources.getColor(R.color.placeColor5, null)
			6 -> requireContext().resources.getColor(R.color.placeColor6, null)
			else -> mainFragment.mainActivity.localAppTheme.colorPrimary
		}
		dataBinding.homeFragmentPlaceNameEditText.setTextColor(tint)
		place?.let { homeMapView.onNewCurrentPlace(it) }

		if (place?.primaryKey != -0L) {
			ioScope.launch { mainFragment.viewModel.placeTable.update(place!!) }
		}
	}

	private fun toggleFavouritePlace() {
		ioScope.launch {
			place!!.isFavourite = !place!!.isFavourite
			mainFragment.viewModel.placeTable.update(place!!)

			mainScope.launch { showPlaceView(false) }
		}
	}

	private fun removePlace() {
		ioScope.launch {
			mainFragment.viewModel.placeTable.delete(place!!)

			mainScope.launch { hidePlaceView() }
		}
	}

	private fun refreshLiveInfoLocationData(location: Location?) {
		dataBinding.homeFragmentCoordinateText.text = if (location == null) {
			"Getting location"
		} else {
			Utility.coordinateFilter(LatLng(location.latitude, location.longitude), coordinateUnit)
		}

		if (mainFragment.viewModel.tripService.status != TripService.STATUS.RUNNING) {
			val liveTripInfoLocationData = LiveInfo.LiveInfoLocationData(
				altitude = location?.altitude,
				speed = location?.speed)
			dataBinding.homeFragmentLiveTripInfo.updateLocationData(liveTripInfoLocationData)
		}
	}

	private fun zoomOnCurrentLocation(animate: Boolean = false) {
		if(mainFragment.checkAndGetLocationPermission()) {
			if (currentLocation != null) {
				isCentredOnce = homeMapView.moveCamera(currentLocation!!, animate)
			} else {
				Toast.makeText(requireContext(), "Waiting for location...", Toast.LENGTH_SHORT).show()
			}
		}
	}

	private fun followLocation() {
		if (followLocation) {
			homeMapView.moveCamera(currentLocation!!, true)
		}
	}

	private fun initializeMapEvents() {
		homeMapView.googleMap?.setOnCameraMoveStartedListener { reason ->
			if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
				dataBinding.homeFragmentGpsButton.setColorFilter(mainFragment.mainActivity.localAppTheme.colorOnBackground)
				followLocation = false
			}
		}
		homeMapView.googleMap?.setOnMapLongClickListener { latLng ->
			val prevColorTag = place?.colorTag
			place = Place(latitude = latLng.latitude, longitude = latLng.longitude, name = null).apply { colorTag = prevColorTag ?: 0 }
			showPlaceView(true)
			homeMapView.onNewCurrentPlace(place!!)
		}
		homeMapView.googleMap?.setOnMarkerClickListener {
			val placePrimaryKey: Long? = it.title?.toLong()
			ioScope.launch {
				if (placePrimaryKey == -2L) {
					mainScope.launch { place = Place(it.position.latitude, it.position.longitude, "") }
				} else if (placePrimaryKey != null) {
					place = mainFragment.viewModel.placeTable.get(placePrimaryKey)
				}
				mainScope.launch {
					try {
						homeMapView.currentPlaceMarker.remove()
					} catch (exception: Exception) {
						exception.printStackTrace()
						Log.i(TAG, "place exception...")
						homeMapView.placeMarkerMap.forEach { it.value.remove() }
						homeMapView.placeMarkerMap = mutableMapOf()
						homeMapView.refreshPlaceMarkers()
					}
					showPlaceView(false)
				}
			}
			true
		}
	}

	private fun Double.round(decimals: Int): Double {
		var multiplier = 1.0
		repeat(decimals) { multiplier *= 10 }
		return kotlin.math.round(this * multiplier) / multiplier
	}

	companion object {
		const val TAG = "HOME_FRAGMENT"
	}
}
