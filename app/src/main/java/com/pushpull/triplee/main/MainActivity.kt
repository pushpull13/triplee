package com.pushpull.triplee.main

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.pushpull.triplee.R
import com.pushpull.triplee.alice.Alice
import com.pushpull.triplee.custom.BaseActivity
import com.pushpull.triplee.databinding.MainActivityBinding
import com.pushpull.triplee.konstant.Constant
import com.pushpull.triplee.konstant.Constant.Companion.Settings
import com.pushpull.triplee.konstant.Secret
import com.pushpull.triplee.utility.Utility
import kotlinx.coroutines.launch
import org.json.JSONObject


class MainActivity : BaseActivity() {

	private lateinit var dataBinding: MainActivityBinding

	private var isFirstInstance = false

	lateinit var navController: NavController

	@RequiresApi(Build.VERSION_CODES.Q)
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		window.apply {
			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
			window.statusBarColor = localAppTheme.colorAlpha
		}

		isFirstInstance = sharedPreferencesAppConfig.getBoolean(Settings.IS_FIRST_INSTANCE.name, true)

		dataBinding = DataBindingUtil.setContentView(this, R.layout.main_activity)

		val navHostFragment: NavHostFragment = supportFragmentManager.findFragmentById(R.id.main_fragment_container) as NavHostFragment
		navController = navHostFragment.findNavController()
		val navGraph = navController.navInflater.inflate(R.navigation.main_nav_graph)

		navGraph.startDestination = if (isFirstInstance) {
			R.id.nav_item_get_started
		} else {
			R.id.nav_item_main
		}
		navController.graph = navGraph

	}

	companion object {
		const val TAG = "MAIN_ACTIVITY"
	}
}
