package com.pushpull.triplee.login

import android.animation.Animator
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.browser.customtabs.CustomTabColorSchemeParams
import androidx.browser.customtabs.CustomTabsIntent
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.pushpull.triplee.R
import com.pushpull.triplee.databinding.LoginGetStartedFragmentBinding
import com.pushpull.triplee.konstant.Constant
import com.pushpull.triplee.main.MainActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job


class GetStartedFragment : Fragment() {

	private val job: Job = Job()
	private val ioScope = CoroutineScope(Dispatchers.IO + job)

	lateinit var dataBinding: LoginGetStartedFragmentBinding
	lateinit var mainActivity: MainActivity

	private var isSyncedWithFirebase: Boolean? = false

	private var currentFragment = 0

	@RequiresApi(Build.VERSION_CODES.Q)
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
		mainActivity = requireActivity() as MainActivity

		dataBinding = DataBindingUtil.inflate(inflater, R.layout.login_get_started_fragment, container, false)

		return dataBinding.root
	}

	override fun onStart() {
		super.onStart()

		dataBinding.loginFragmentTermsOfService.setOnClickListener {
			val url = "https://gitlab.com/pushpull13/triplee"

			val defaultColors = CustomTabColorSchemeParams.Builder()
				.setToolbarColor(mainActivity.localAppTheme.colorPrimary)
				.build()

			val customTab = CustomTabsIntent
				.Builder()
				.setDefaultColorSchemeParams(defaultColors)
				.build()
			customTab.launchUrl(requireContext(), Uri.parse(url))
		}

		dataBinding.loginFragmentPrivacyPolicy.setOnClickListener {
			val url = "https://gitlab.com/pushpull13/triplee"

			val defaultColors = CustomTabColorSchemeParams.Builder()
				.setToolbarColor(mainActivity.localAppTheme.colorPrimary)
				.build()

			val customTab = CustomTabsIntent
				.Builder()
				.setDefaultColorSchemeParams(defaultColors)
				.build()
			customTab.launchUrl(requireContext(), Uri.parse(url))
		}

		mainActivity.onBackPressedDispatcher.addCallback(object : OnBackPressedCallback(true) {
			override fun handleOnBackPressed() {
				Log.i(TAG, "back pressed...")
				when (currentFragment) {
					0 -> mainActivity.finishAffinity()
					1 -> frag0()
				}
			}
		})

		dataBinding.loginFragmentGetStartedButton.setOnClickListener {
			frag1()
		}

		fun showAgreeToPolicyToast() {
			Toast.makeText(requireContext(), "Please agree to the Terms of Service and Privacy Policy", Toast.LENGTH_SHORT).show()
		}
		dataBinding.loginFragmentTryButton.setOnClickListener {
			if (dataBinding.loginFragmentPolicyCheckBox.isChecked) {
				isSyncedWithFirebase = null
				frag2()
			} else {
				showAgreeToPolicyToast()
			}
		}
	}

	private fun frag0() {
		dataBinding.loginFragmentIllustration1.visibility = View.GONE
		dataBinding.loginFragmentMessage1.visibility = View.GONE
		dataBinding.loginFragmentTryButton.visibility = View.GONE
		dataBinding.loginFragmentPolicy.visibility = View.GONE

		dataBinding.loginFragmentLoadingIllustrationFlipper.visibility = View.GONE
		dataBinding.loginFragmentLottieLoading.visibility = View.GONE
		dataBinding.loginFragmentGettingReady.visibility = View.GONE

		dataBinding.loginFragmentLottieWelcome.visibility = View.GONE

		dataBinding.loginFragmentAppName.visibility = View.VISIBLE
		dataBinding.loginFragmentAppQuote.visibility = View.VISIBLE
		dataBinding.loginFragmentLottieTrip.visibility = View.VISIBLE
		dataBinding.loginFragmentGetStartedButton.visibility = View.VISIBLE

		currentFragment = 0
	}

	private fun frag1() {
		dataBinding.loginFragmentAppName.visibility = View.GONE
		dataBinding.loginFragmentAppQuote.visibility = View.GONE
		dataBinding.loginFragmentLottieTrip.visibility = View.GONE
		dataBinding.loginFragmentGetStartedButton.visibility = View.GONE

		dataBinding.loginFragmentIllustration1.visibility = View.VISIBLE
		dataBinding.loginFragmentMessage1.visibility = View.VISIBLE
		dataBinding.loginFragmentTryButton.visibility = View.VISIBLE
		dataBinding.loginFragmentPolicy.visibility = View.VISIBLE

		dataBinding.loginFragmentLoadingIllustrationFlipper.visibility = View.GONE
		dataBinding.loginFragmentLottieLoading.visibility = View.GONE
		dataBinding.loginFragmentGettingReady.visibility = View.GONE

		dataBinding.loginFragmentLottieWelcome.visibility = View.GONE

		currentFragment = 1
	}

	private fun frag2() {
		dataBinding.loginFragmentAppName.visibility = View.GONE
		dataBinding.loginFragmentAppQuote.visibility = View.GONE
		dataBinding.loginFragmentLottieTrip.visibility = View.GONE
		dataBinding.loginFragmentGetStartedButton.visibility = View.GONE

		dataBinding.loginFragmentIllustration1.visibility = View.GONE
		dataBinding.loginFragmentMessage1.visibility = View.GONE
		dataBinding.loginFragmentTryButton.visibility = View.GONE
		dataBinding.loginFragmentPolicy.visibility = View.GONE

		dataBinding.loginFragmentLoadingIllustrationFlipper.visibility = View.VISIBLE
		dataBinding.loginFragmentLottieLoading.visibility = View.VISIBLE
		dataBinding.loginFragmentGettingReady.visibility = View.VISIBLE

		dataBinding.loginFragmentLottieWelcome.visibility = View.GONE

		currentFragment = 2

		mainActivity.sharedPreferencesAppConfig.edit().putBoolean(Constant.Companion.Settings.IS_FIRST_INSTANCE.name, false).commit()

		dataBinding.loginFragmentLoadingIllustrationFlipper.startFlipping()

		var animationCount = 0
		val animationHandler = Handler(mainActivity.mainLooper)
		val animationRunnable = object : Runnable {
			override fun run() {
				if (isSyncedWithFirebase == false) {
					animationHandler.postDelayed(this, 10000L)
				} else {
					if (animationCount < 3) {
						animationHandler.postDelayed(this, 10000L)
						animationCount++
					} else {
						dataBinding.loginFragmentLoadingIllustrationFlipper.visibility = View.GONE
						dataBinding.loginFragmentLottieLoading.visibility = View.GONE
						dataBinding.loginFragmentGettingReady.visibility = View.GONE

						dataBinding.loginFragmentLottieWelcome.visibility = View.VISIBLE

						dataBinding.loginFragmentLottieWelcome.addAnimatorListener(object : Animator.AnimatorListener {
							override fun onAnimationStart(animation: Animator?) {}

							override fun onAnimationEnd(animation: Animator?) {
								mainActivity.navController.navigate(R.id.nav_item_main)
							}

							override fun onAnimationCancel(animation: Animator?) {}
							override fun onAnimationRepeat(animation: Animator?) {}
						})

						dataBinding.loginFragmentLottieWelcome.playAnimation()
					}
				}
			}
		}

		animationHandler.post(animationRunnable)
	}

	companion object {
		const val TAG = "GET_STARTED_FRAGMENT"
	}
}
