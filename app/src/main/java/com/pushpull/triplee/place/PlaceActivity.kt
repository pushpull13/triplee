package com.pushpull.triplee.place

import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.view.WindowInsetsControllerCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.asLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pushpull.triplee.R
import com.pushpull.triplee.custom.BaseActivity
import com.pushpull.triplee.custom.round
import com.pushpull.triplee.database.UserDatabase
import com.pushpull.triplee.database.place.Place
import com.pushpull.triplee.databinding.PlaceActivityBinding

class PlaceActivity : BaseActivity() {
	private lateinit var dataBinding: PlaceActivityBinding

	private val placeTable = UserDatabase.getInstance(this).placeTableDao

	private val placeDataViewHolder = PlaceDataViewHolder(mutableListOf())
	private var placeDataList: List<Place> = listOf()

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		initUi()

		dataBinding = DataBindingUtil.setContentView(this, R.layout.place_activity)
		dataBinding.placeActivityBack.setOnClickListener { finish() }
	}

	override fun onStart() {
		super.onStart()

		initCheckChangeListener()

		dataBinding.placeActivityRecyclerView.layoutManager = LinearLayoutManager(this)
		dataBinding.placeActivityRecyclerView.adapter = placeDataViewHolder

		placeTable.getAllAsFlow().asLiveData().observe(this) {
			placeDataList = it
			refreshData()
		}
	}

	private fun initCheckChangeListener() {
		dataBinding.placeActivityChip0.setOnCheckedChangeListener { buttonView, isChecked -> refreshData() }
		dataBinding.placeActivityChip1.setOnCheckedChangeListener { buttonView, isChecked -> refreshData() }
		dataBinding.placeActivityChip2.setOnCheckedChangeListener { buttonView, isChecked -> refreshData() }
		dataBinding.placeActivityChip3.setOnCheckedChangeListener { buttonView, isChecked -> refreshData() }
		dataBinding.placeActivityChip4.setOnCheckedChangeListener { buttonView, isChecked -> refreshData() }
		dataBinding.placeActivityChip5.setOnCheckedChangeListener { buttonView, isChecked -> refreshData() }
		dataBinding.placeActivityChip6.setOnCheckedChangeListener { buttonView, isChecked -> refreshData() }
	}

	private fun refreshData() {
		Log.i(TAG, "refreshData...")
		val placeDataListFiltered: MutableList<Place> = mutableListOf()
		placeDataList.forEach {
			val doInclude = doInclude(it.colorTag)
			Log.i(TAG, doInclude.toString())
			if (doInclude) {
				placeDataListFiltered.add(it)
			}
		}
		placeDataViewHolder.placeList = placeDataListFiltered
		placeDataViewHolder.notifyDataSetChanged()

		if (placeDataListFiltered.isEmpty()) {
			dataBinding.placeActivityLottieNotFound.playAnimation()
			dataBinding.placeActivityLottieNotFound.visibility = View.VISIBLE
			dataBinding.placeActivityTextNotFound.visibility = View.VISIBLE
		} else {
			dataBinding.placeActivityLottieNotFound.visibility = View.GONE
			dataBinding.placeActivityLottieNotFound.playAnimation()
			dataBinding.placeActivityTextNotFound.visibility = View.GONE
		}
	}

	private fun doInclude(colorTag: Int): Boolean {
		when (colorTag) {
			0 -> if (dataBinding.placeActivityChip0.isChecked) {
				return true
			}
			1 -> if (dataBinding.placeActivityChip1.isChecked) {
				return true
			}
			2 -> if (dataBinding.placeActivityChip2.isChecked) {
				return true
			}
			3 -> if (dataBinding.placeActivityChip3.isChecked) {
				return true
			}
			4 -> if (dataBinding.placeActivityChip4.isChecked) {
				return true
			}
			5 -> if (dataBinding.placeActivityChip5.isChecked) {
				return true
			}
			6 -> if (dataBinding.placeActivityChip6.isChecked) {
				return true
			}
		}
		return false
	}


	inner class PlaceDataViewHolder(var placeList: List<Place>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

		private inner class ViewHolderPlaceView(view: View) : RecyclerView.ViewHolder(view) {
			val placeName: TextView = view.findViewById(R.id.place_view_holder_place_name)
			val coordinate: TextView = view.findViewById(R.id.place_view_holder_coordinate)
			val placeAddress: TextView = view.findViewById(R.id.place_view_holder_address)
			val icon: AppCompatImageView = view.findViewById(R.id.place_view_holder_icon)

			fun bind(position: Int) {
				val place = placeList[position]
				placeName.text = place.name
				coordinate.text = "${place.latitude.round(6)}, ${place.longitude.round(6)}"
				placeAddress.text = place.address

				when (place.colorTag) {
					0 -> setTint(resources.getColor(R.color.placeColor0, null))
					1 -> setTint(resources.getColor(R.color.placeColor1, null))
					2 -> setTint(resources.getColor(R.color.placeColor2, null))
					3 -> setTint(resources.getColor(R.color.placeColor3, null))
					4 -> setTint(resources.getColor(R.color.placeColor4, null))
					5 -> setTint(resources.getColor(R.color.placeColor5, null))
					6 -> setTint(resources.getColor(R.color.placeColor6, null))
					else -> icon.setColorFilter(resources.getColor(localAppTheme.colorPrimary, null))
				}

			}

			private fun setTint(tint: Int) {
				icon.setColorFilter(tint)
				placeName.setTextColor(tint)
			}
		}

		override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
			ViewHolderPlaceView(LayoutInflater.from(this@PlaceActivity).inflate(R.layout.place_view_holder, parent, false))

		override fun getItemCount(): Int = placeList.size
		override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) = (holder as ViewHolderPlaceView).bind(position)
	}

	private fun initUi() {
		val decorView = window.decorView
		WindowInsetsControllerCompat(window, decorView).apply {
			when (resources?.configuration?.uiMode?.and(Configuration.UI_MODE_NIGHT_MASK)) {
				Configuration.UI_MODE_NIGHT_YES -> {
					isAppearanceLightStatusBars = false
					isAppearanceLightNavigationBars = true
				}
				Configuration.UI_MODE_NIGHT_NO -> {
					isAppearanceLightStatusBars = true
					isAppearanceLightNavigationBars = true
				}
			}
		}

		window.statusBarColor = localAppTheme.colorBeta
		window.navigationBarColor = localAppTheme.colorBeta
	}

	companion object {
		const val TAG = "PLACE_ACTIVITY"
	}
}
