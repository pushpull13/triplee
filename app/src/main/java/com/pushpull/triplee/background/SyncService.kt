package com.pushpull.triplee.background

import android.content.Intent
import android.content.SharedPreferences
import android.location.Geocoder
import android.os.Binder
import android.os.IBinder
import android.util.Log
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.MutableLiveData
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.pushpull.triplee.R
import com.pushpull.triplee.database.UserDatabase
import com.pushpull.triplee.database.trip.Trip
import com.pushpull.triplee.konstant.Constant
import com.pushpull.triplee.konstant.Path
import com.pushpull.triplee.konstant.Constant.Companion.Settings
import com.pushpull.triplee.utility.Utility
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.apache.commons.compress.archivers.tar.TarArchiveEntry
import java.util.*
import kotlin.properties.Delegates

import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream

import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream
import org.apache.commons.compress.compressors.gzip.GzipParameters
import org.apache.commons.compress.utils.IOUtils
import java.io.*
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream

import org.apache.commons.compress.archivers.tar.TarArchiveInputStream


//class SyncService : LifecycleService() {
//
//	private val job: Job = Job()
//	private val mainScope = CoroutineScope(Dispatchers.Main)
//	private val ioScope = CoroutineScope(Dispatchers.IO + job)
//	private val objectMapper: ObjectMapper = jacksonObjectMapper()
//	private lateinit var sharedPreferencesAppConfig: SharedPreferences
//
//	enum class STATUS {
//		NOT_STARTED,
//		NOT_SIGNED_IN,
//		NO_PERMISSION,
//		NO_NETWORK,
//		NETWORK_DATA,
//		READY,
//		LOCKED,
//		RUNNING,
//		DOWNLOAD,
//		UPLOAD,
//		SUCCEED,
//		FAILED
//	}
//
//	private lateinit var drive: Drive
//	private val tripTable = UserDatabase.getInstance(this).tripTableDao
//	private lateinit var geoCoder: Geocoder
//
//	val statusLiveData: MutableLiveData<STATUS> = MutableLiveData<STATUS>().apply { value = STATUS.NOT_STARTED }
//	private var status: STATUS = STATUS.NOT_STARTED
//		set(value) {
//			Log.i(TAG, value.name)
//			when (value) {
//				STATUS.SUCCEED -> {
//					sharedPreferencesAppConfig.edit().putLong(Settings.LAST_SYNC_TIMESTAMP.name, currentTimestamp).commit()
//					unlockDrive()
//				}
//				STATUS.FAILED -> unlockDrive()
//			}
//			mainScope.launch { statusLiveData.value = value }
//			field = value
//		}
//
//	private var currentTimestamp by Delegates.notNull<Long>()
//	private var lastSyncTimestamp by Delegates.notNull<Long>()
//	private var lockFileId: String? = null
//
//	override fun onCreate() {
//		Log.i(TAG, "onCreate...")
//		super.onCreate()
//
//		sharedPreferencesAppConfig = getSharedPreferences(Settings.APP_CONFIG.name, MODE_PRIVATE)        //  hardCoded
//
//		geoCoder = Geocoder(this)
//	}
//
//	override fun onDestroy() {
//		super.onDestroy()
//		Log.i(TAG, "onDestroy...")
//	}
//
//	fun refreshSyncerState() {
//		lastSyncTimestamp = sharedPreferencesAppConfig.getLong(Settings.LAST_SYNC_TIMESTAMP.name, 0)
//		currentTimestamp = System.currentTimeMillis()
//
//		if (auth.currentUser == null) {
//			status = STATUS.NOT_SIGNED_IN
//		} else if (auth.currentUser!!.isAnonymous) {
//			status = STATUS.NOT_SIGNED_IN
//		} else {
//			status = STATUS.READY
//			ioScope.launch { initSync() }
//		}
//	}
//
//	private fun initSync() {
//		Log.i(TAG, "initSync...")
//		drive = Drive.Builder(
//			NetHttpTransport(),
//			JacksonFactory.getDefaultInstance(),
//			GoogleAccountCredential.usingOAuth2(this@SyncService, listOf(DriveScopes.DRIVE_APPDATA))
//				.setSelectedAccountName(auth.currentUser!!.email))
//			.setApplicationName(getString(R.string.app_name))
//			.build()
//
//		lockDrive()
//
//		if (status == STATUS.RUNNING) {
//			sync()
//		}
//
//		status = STATUS.SUCCEED
//	}
//
//	private fun lockDrive() {
//		var pageToken: String? = null
//		var lockFile: File? = null
//
//		do {
//			val result: FileList = drive.files()
//				.list()
//				.setQ("name = '${Constant.Companion.Settings.LOCK_FILE.name}'")
//				.setSpaces("appDataFolder")
//				.setFields("nextPageToken, files(id, name, createdTime)")
//				.setPageToken(pageToken)
//				.setPageSize(100)
//				.execute()
//
//			for (driveFile in result.files) {
//				if (driveFile.name == Constant.Companion.Settings.LOCK_FILE.name) {
//					lockFile = driveFile
//					pageToken = null
//					break
//				}
//			}
//		} while (pageToken != null)
//
//		if (lockFile == null) {
//			lockFileId = uploadDriveFile(
//				fileId = null,
//				fileName = Constant.Companion.Settings.LOCK_FILE.name,
//				isDeleted = false,
//				mimeType = "application/json",
//				inputStream = "".byteInputStream(),
//				modifiedTimestamp = null,
//				length = "".length.toLong()
//			)
//			status = STATUS.RUNNING
//		} else {
//			if (currentTimestamp - lockFile.createdTime.value > 5 * 60 * 1000) {
//				lockFileId = lockFile.id
//				status = STATUS.RUNNING
//			} else {
//				status = STATUS.LOCKED
//			}
//		}
//	}
//
//	private fun unlockDrive() {
//		lockFileId?.let { deleteDriveFile(it) }
//	}
//
//	private fun sync() {
//		status = STATUS.DOWNLOAD
//
//		val localPrimaryKeyList: List<Long> = tripTable.getAllKeys()
//		val cloudPrimaryKeyList: MutableList<Long> = mutableListOf()
//
//		val cloudDataMap: MutableMap<Long, File> = mutableMapOf()
//		val fileList = queryFile("name contains 'archive_trip'")
//		fileList.forEach {
//			val primaryKey = it.name.substring(13, 26).toLong()
//			cloudPrimaryKeyList.add(primaryKey)
//			cloudDataMap[primaryKey] = it
//		}
//
//		val primaryKeySet: MutableSet<Long> = mutableSetOf()
//		primaryKeySet.addAll(localPrimaryKeyList)
//		primaryKeySet.addAll(cloudPrimaryKeyList)
//
//		primaryKeySet.forEach { primaryKey ->
//			when {
//				primaryKey in localPrimaryKeyList && primaryKey in cloudPrimaryKeyList -> {
//					val trip = tripTable.get(primaryKey)
//					if (trip != null) {
//						if (trip.modifiedTimestamp > cloudDataMap[primaryKey]!!.modifiedTime.value && !trip.isActive) {
//							upSync(trip)
//						} else if (trip.modifiedTimestamp < cloudDataMap[primaryKey]!!.modifiedTime.value) {
//							downSync(cloudDataMap[primaryKey]!!)
//						}
//					}
//				}
//				primaryKey in localPrimaryKeyList -> {
//					val trip = tripTable.get(primaryKey)
//					if (trip != null && !trip.isActive) {
//						if (trip.modifiedTimestamp > lastSyncTimestamp) {
//							upSync(trip)
//						}
//					}
//				}
//				primaryKey in cloudPrimaryKeyList -> {
//					downSync(cloudDataMap[primaryKey])
//				}
//			}
//		}
//	}
//
//	private fun upSync(trip: Trip) {
//		status = STATUS.UPLOAD
//
//		Log.i(TAG, "upSync : ${trip.primaryKey} : ${trip.fileId}")
//		val archiveFile = java.io.File.createTempFile("${trip.primaryKey}_", ".tar.gz")
//
//		if (trip.isDeleted) {
//			uploadDriveFile(
//				fileId = trip.fileId,
//				fileName = "archive_trip_${trip.primaryKey}.tar.gz",
//				isDeleted = true,
//				mimeType = "application/gzip",
//				inputStream = "".byteInputStream(),
//				modifiedTimestamp = trip.modifiedTimestamp,
//				length = 0
//			)
//		} else {
//			val baseFile = Utility.readDataFile(trip.primaryKey, "base")
//			baseFile.writeText(objectMapper.writeValueAsString(trip))
//			archiveData(trip.primaryKey, archiveFile)
//
//			val fileId = uploadDriveFile(
//				fileId = trip.fileId,
//				fileName = "archive_trip_${trip.primaryKey}.tar.gz",
//				isDeleted = trip.isDeleted,
//				mimeType = "application/gzip",
//				inputStream = archiveFile.inputStream(),
//				modifiedTimestamp = trip.modifiedTimestamp,
//				length = archiveFile.length()
//			)
//
//			trip.apply {
//				this.fileId = fileId
//				tripTable.insert(this)
//			}
//		}
//	}
//
//	private fun downSync(file: File?) {
//		status = STATUS.DOWNLOAD
//
//		if (file == null) {
//			return
//		} else {
//			val primaryKey = file.name.substring(13, 26).toLong()
//			Log.i(TAG, "downSync : $primaryKey : ${file.id}")
//
//			if (file.appProperties["isDeleted"] == "true") {
//				deleteTrip(primaryKey, file)
//			} else {
//				val tripDataArchiveFile = java.io.File.createTempFile("${primaryKey}_", ".tar.gz")
//				val fileOutputStream = FileOutputStream(tripDataArchiveFile)
//				downloadDriveFile(file.id, fileOutputStream)
//
//				val dataDestination = File(Path.TRIP_FOLDER_NAME)
//				unarchiveData(tripDataArchiveFile, dataDestination)
//
//				val trip: Trip = objectMapper.readValue(Utility.readDataFile(primaryKey, "base"))
//				trip.fileId = file.id
//				tripTable.insert(trip)
//			}
//		}
//	}
//
//	private fun deleteTrip(primaryKey: Long, file: File) {
//		val trip = Trip(primaryKey, 0).apply {
//			modifiedTimestamp = file.modifiedTime.value
//			fileId = file.id
//			isDeleted = true
//		}
//
//		val tripDir = File("${Path.TRIP_FOLDER_NAME}/$primaryKey")
//		if (tripDir.exists()) {
//			tripDir.deleteRecursively()
//		}
//
//		tripTable.insert(trip)
//	}
//
//	private fun queryFile(query: String? = null): MutableList<File> {
//		var pageToken: String? = null
//		val returnFile: MutableList<File> = mutableListOf()
//
//		do {
//			val result: FileList = drive.files()
//				.list()
//				.setQ(query)
//				.setSpaces("appDataFolder")
//				.setFields("nextPageToken, files(id, name, version, appProperties, modifiedTime)")
//				.setPageToken(pageToken)
//				.execute()
//
//			returnFile.addAll(result.files)
//
//			pageToken = result.nextPageToken
//		} while (pageToken != null)
//
//		Log.i(TAG, "queryFileList : ${returnFile.size}")
//		return returnFile
//	}
//
//	private fun uploadDriveFile(
//		fileId: String?,
//		fileName: String,
//		isDeleted: Boolean,
//		mimeType: String,
//		inputStream: InputStream,
//		modifiedTimestamp: Long?,
//		length: Long,
//	): String {
//		Log.i(TAG, "uploadDriveFile : $fileId")
//		val driveFile = File()
//		driveFile.name = fileName
//		driveFile.appProperties = mapOf(
//			"isDeleted" to "$isDeleted",
//			"syncerVersion" to "1"
//		)
//		driveFile.modifiedTime = DateTime(modifiedTimestamp ?: currentTimestamp)
//		val fileContent = FileContent(mimeType, inputStream, length)
//
//		return if (fileId == null) {
//			driveFile.parents = Collections.singletonList("appDataFolder")
//			val newFileId = drive.files()
//				.create(driveFile, fileContent)
//				.setFields("id")
//				.execute().id
//			newFileId
//		} else {
//			drive.files()
//				.update(fileId, driveFile, fileContent)
//				.execute()
//			fileId
//		}
//	}
//
//	private fun downloadDriveFile(fileId: String, fileOutputStream: FileOutputStream) {
//		drive.files()
//			.get(fileId)
//			.executeMediaAndDownloadTo(fileOutputStream)
//	}
//
//	private fun deleteDriveFile(fileId: String) {
//		try {
//			drive.files()
//				.delete(fileId)
//				.execute()
//		} catch (exception: HttpResponseException) {
//			if (exception.statusCode == 416) {
//				TODO()
//			}
//		}
//	}
//
//	private fun archiveData(primaryKey: Long, archiveFile: java.io.File) {
//		val dirPath = "${Path.TRIP_FOLDER_NAME}/$primaryKey"
//
//		val fileOut = FileOutputStream(archiveFile)
//		val buffOut = BufferedOutputStream(fileOut)
//		val gzOut = GzipCompressorOutputStream(buffOut, GzipParameters().apply { filename = "$primaryKey.gz" })
//		val tarOut = TarArchiveOutputStream(gzOut)
//
//		addFileToTarGz(tarOut, dirPath, "")
//
//		tarOut.finish()
//		tarOut.close()
//		gzOut.close()
//		buffOut.close()
//		fileOut.close()
//	}
//
//	private fun addFileToTarGz(tOut: TarArchiveOutputStream, path: String, base: String) {
//		val file = File(path)
//		val entryName = base + file.name
//		val tarEntry = TarArchiveEntry(file, entryName)
//		tOut.putArchiveEntry(tarEntry)
//		if (file.isFile) {
//			IOUtils.copy(FileInputStream(file), tOut)
//			tOut.closeArchiveEntry()
//		} else {
//			tOut.closeArchiveEntry()
//			val children = file.listFiles()
//			children?.iterator()?.forEach { child ->
//				addFileToTarGz(tOut, child.absolutePath, "$entryName/")
//			}
//		}
//	}
//
//	private fun unarchiveData(tarFile: java.io.File, dest: java.io.File) {
//		dest.mkdirs()
//		val fileIn = FileInputStream(tarFile)
//		val buffIn = BufferedInputStream(fileIn)
//		val gzIn = GzipCompressorInputStream(buffIn)
//		val tarIn = TarArchiveInputStream(gzIn)
//
//		var tarEntry = tarIn.nextTarEntry
//		while (tarEntry != null) { // create a file with the same name as the tarEntry
//			val destPath = File(dest, tarEntry.name)
//			if (tarEntry.isDirectory) {
//				destPath.mkdirs()
//			} else {
//				destPath.createNewFile()
//				val btoRead = ByteArray(1024)
//				val bout = BufferedOutputStream(FileOutputStream(destPath))
//				var len = 0
//				while (tarIn.read(btoRead).also { len = it } != -1) {
//					bout.write(btoRead, 0, len)
//				}
//				bout.close()
//			}
//			tarEntry = tarIn.nextTarEntry
//		}
//		tarIn.close()
//		gzIn.close()
//		buffIn.close()
//		fileIn.close()
//	}
//
//	inner class FileContent(type: String?, val _inputStream: InputStream, val _length: Long) : AbstractInputStreamContent(type) {
//		override fun getLength(): Long = _length
//
//		override fun retrySupported(): Boolean = true
//
//		@Throws(FileNotFoundException::class)
//		override fun getInputStream(): InputStream = _inputStream
//
//		override fun setType(type: String): FileContent = super.setType(type) as FileContent
//
//		override fun setCloseInputStream(closeInputStream: Boolean): FileContent = super.setCloseInputStream(closeInputStream) as FileContent
//	}
//
//	private val binder: LocalBinder = LocalBinder()
//
//	override fun onBind(intent: Intent): IBinder {
//		super.onBind(intent)
//		Log.i(TAG, "onBind...")
//		return binder
//	}
//
//	inner class LocalBinder : Binder() {
//		fun getService(): SyncService = this@SyncService
//	}
//
//	override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
//		super.onStartCommand(intent, flags, startId)
//		return START_NOT_STICKY
//	}
//
//	companion object {
//		const val TAG = "SYNC_SERVICE"
//	}
//}
