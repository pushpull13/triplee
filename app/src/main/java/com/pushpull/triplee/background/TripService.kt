package com.pushpull.triplee.background

import android.annotation.SuppressLint
import android.app.*
import android.content.*
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.*
import android.text.TextUtils
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.LifecycleService
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.SphericalUtil
import com.pushpull.triplee.BuildConfig
import com.pushpull.triplee.dataClass.TripData
import com.pushpull.triplee.database.UserDatabase
import com.pushpull.triplee.database.trip.Trip
import com.pushpull.triplee.database.trip.TripTableDao
import com.pushpull.triplee.konstant.ActivityId
import com.pushpull.triplee.konstant.Constant
import com.pushpull.triplee.konstant.Path
import com.pushpull.triplee.main.MainActivity
import com.pushpull.triplee.utility.Utility
import kotlinx.coroutines.*
import java.io.File
import java.util.*
import com.pushpull.triplee.R


class TripService : LifecycleService() {

	private val job: Job = Job()
	private val ioScope = CoroutineScope(Dispatchers.IO + job)
	private val mainScope = CoroutineScope(Dispatchers.Main)
	private val objectMapper = ObjectMapper().registerModule(KotlinModule())

	private val binder: LocalBinder = LocalBinder()

	inner class LocalBinder : Binder() {
		fun getService(): TripService = this@TripService
	}

	override fun onBind(intent: Intent): IBinder {
		super.onBind(intent)
		return binder
	}

	enum class STATUS {
		NOT_STARTED,
		PROCESSING,
		CRASH_RECOVERY,
		READY,
		RUNNING,
		PAUSED,
		STOPPED,
		SUCCEED,
		FAILED
	}

	var status: STATUS = STATUS.NOT_STARTED
		set(value) {
			mainScope.launch {
				field = value
			}
			onTripStatusChange?.onTripStatusChange()
		}

	interface OnTripStatusChange {
		fun onTripStatusChange()
	}

	private var onTripStatusChange: OnTripStatusChange? = null
	fun onTripStatusChange(listener: OnTripStatusChange) {
		onTripStatusChange = listener
	}

	interface OnReceiveLocationUpdate {
		fun onReceiveLocation(
			location: Location?,
			locationList: MutableMap<Long, List<Double>>?,
			activity: ActivityId.Companion.ACTIVITY,
			pseudoActivity: Int,
			fileList: List<String>,
		)
	}

	private var onReceiveLocationUpdate: OnReceiveLocationUpdate? = null
	fun onReceiveLocation(listener: OnReceiveLocationUpdate) {
		onReceiveLocationUpdate = listener
	}

	private var currentLocation: Location? = null
		set(value) {
			Log.i(TAG, "prevLocation : $currentLocation : newLocation : $value")
			if (currentLocation != null && value != null) {
				distance +=
					SphericalUtil.computeDistanceBetween(LatLng(currentLocation!!.latitude, currentLocation!!.longitude),
						LatLng(value.latitude, value.longitude))
			}
			field = value
			if (value != null) {
				onReceiveLocationUpdate?.onReceiveLocation(value, tripData?.tripLocationDataMap, currentActivity, -1, fileList)
			}
		}

	private lateinit var sharedPreferencesAppConfig: SharedPreferences
	private lateinit var tripTable: TripTableDao

	private lateinit var notificationIntent: Intent
	private lateinit var notificationPendingIntent: PendingIntent
	private lateinit var notificationManager: NotificationManager
	private var notification: NotificationCompat.Builder? = null

	private val notificationHandler = Handler(Looper.getMainLooper())
	private val notificationRunnable = object : Runnable {
		override fun run() {
			notification!!
				.setContentText("Distance : ${distance.toInt()}")
				.setLargeIcon(getBitmap(ActivityId.ACTIVITY_ID_RES_MAP[currentActivity]!!, Color.BLACK))
				.setSubText(ActivityId.ACTIVITY_ID_NAME_MAP[currentActivity])
				.setNumber(1)
				.setContentIntent(notificationPendingIntent)
			notificationManager.notify(1, notification!!.build())

			when (status) {
				STATUS.NOT_STARTED -> stopForeground(true)
				STATUS.PROCESSING -> notificationHandler.postDelayed(this, 1000)
				STATUS.CRASH_RECOVERY -> stopForeground(true)
				STATUS.READY -> stopForeground(true)
				STATUS.RUNNING -> notificationHandler.postDelayed(this, 1000)
				STATUS.PAUSED -> notificationHandler.postDelayed(this, 1000)
				STATUS.STOPPED -> stopForeground(true)
				STATUS.SUCCEED -> stopForeground(true)
				STATUS.FAILED -> stopForeground(true)
			}
		}
	}

	private lateinit var locationManager: LocationManager
	private val locationListener: LocationListener = LocationListener {
		processLocation(it)
	}

	private fun processLocation(location: Location) {
		tripData!!.tripLocationDataMap[location.time] =
			listOf(
				location.latitude.round(6),
				location.longitude.round(6),
				location.altitude.round(6),
				location.speed.toDouble(),
				location.bearing.toDouble())

		if (trip!!.startLatitude > 90F || trip!!.startLongitude > 180F) {
			trip!!.startLatitude = location.latitude

			trip!!.startLongitude = location.longitude
			ioScope.launch { tripTable.insert(trip!!) }
		}
		trip!!.endLatitude = location.latitude
		trip!!.endLongitude = location.longitude
		trip!!.endTime = location.time
		currentLocation = location
	}

	var primaryKey: Long = -1
	var trip: Trip? = null
	private var tripData: TripData? = null
	private var toSavedFile: File? = null

	var currentActivity: ActivityId.Companion.ACTIVITY = ActivityId.Companion.ACTIVITY.TREKKING

	private var activitySet: MutableSet<Int> = mutableSetOf()
	var fileList: MutableList<String> = mutableListOf()
	var distance: Double = 0.0

	private lateinit var activityTransitionPendingIntent: PendingIntent

	override fun onCreate() {
		super.onCreate()
		status = STATUS.NOT_STARTED

		Path.ROOT_PATH = applicationContext.applicationInfo.dataDir
		sharedPreferencesAppConfig = getSharedPreferences(Constant.Companion.Settings.APP_CONFIG.name, AppCompatActivity.MODE_PRIVATE)
		tripTable = UserDatabase.getInstance(applicationContext).tripTableDao

		currentActivity = ActivityId.Companion.ACTIVITY.values()[sharedPreferencesAppConfig
			.getInt(Constant.Companion.Settings.ACTIVITY.name, ActivityId.Companion.ACTIVITY.TREKKING.ordinal)]

		locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

		notificationIntent = Intent(this, MainActivity::class.java)
		notificationPendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)
		notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager

		Intent(ACTIVITY_TRANSITION_RECEIVER_ACTION).apply {
			activityTransitionPendingIntent = PendingIntent.getBroadcast(this@TripService, 0, this, 0)
		}

		crashRecovery()
	}

	private fun crashRecovery() {
		status = STATUS.PROCESSING

		primaryKey = sharedPreferencesAppConfig.getLong("TRIP_PRIMARY_KEY", -1L)
		if (primaryKey == -1L) {
			status = STATUS.READY
		} else {
			runBlocking { ioScope.launch { trip = tripTable.get(primaryKey) }.join() }
			status = STATUS.CRASH_RECOVERY
		}
		Log.i(TAG, status.name)
	}

	private fun recoverCrash() {
		Log.i(TAG, "recoverCrash")
	}

	@SuppressLint("MissingPermission")
	fun startTrip() {
		Log.i(TAG, "startTrip...")
		status = STATUS.PROCESSING

		startActivityTransition()
		initializeNotificationUpdater()

		Intent(application, TripService::class.java).also { intent ->
			startForegroundService(intent)
		}

		val calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"), Locale.getDefault())
		primaryKey = calendar.timeInMillis
		trip = Trip(
			primaryKey = primaryKey,
			timezoneOffset = calendar.timeZone.rawOffset
		).apply {
			modifiedTimestamp = this@TripService.primaryKey
			isActive = true
			sharedPreferencesAppConfig.edit().putLong("TRIP_PRIMARY_KEY", this@TripService.primaryKey).commit()
			ioScope.launch { tripTable.insert(this@apply) }
		}

		tripData = TripData(startTimestamp = primaryKey, next = null).apply {
			activity = currentActivity.ordinal
		}
		toSavedFile = Utility.readDataFile(primaryKey, "0")
		toSavedFile!!.createNewFile()

		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0F, locationListener)

		status = STATUS.RUNNING
	}

	fun dumpAndOrSwitchActivity(activityId: ActivityId.Companion.ACTIVITY? = null, pseudoActivity: Int? = null) {
		Log.i(TAG, "dumpAndOrSwitchActivity...")
		if (status == STATUS.RUNNING) {
			while (true) {
				val nextFile = Utility.readDataFile(primaryKey, Utility.getRandomString())
				if (nextFile.createNewFile()) {
					tripData!!.next = nextFile.name.substring(23, 31)
					val currentTimestamp = System.currentTimeMillis()
					if (tripData!!.katoTimestamp == -1L) {
						tripData!!.katoTimestamp = currentTimestamp
					}
					if (tripData!!.panoTimestamp == -1L) {
						tripData!!.panoTimestamp = currentTimestamp
					}
					Utility.writeFileAsEncryptedObject(toSavedFile!!, tripData)
					updateFileSet()

//					Continue to next activity
					tripData = TripData(startTimestamp = primaryKey, next = null).apply { activity = activityId?.ordinal ?: currentActivity.ordinal }
					tripData!!.activity = activityId?.ordinal ?: currentActivity.ordinal
					tripData!!.pseudoActivity = pseudoActivity
					toSavedFile = nextFile
					break
				}
			}
		}

		currentActivity = activityId ?: currentActivity
		activitySet.add(activityId?.ordinal ?: currentActivity.ordinal)
		onReceiveLocationUpdate?.onReceiveLocation(currentLocation, tripData?.tripLocationDataMap, currentActivity, -1, fileList)
	}

	fun endTrip() {
		Utility.writeFileAsEncryptedObject(toSavedFile!!, tripData)
		updateFileSet()
		locationManager.removeUpdates(locationListener)
		ioScope.launch {
			trip!!.apply {
				activityList = objectMapper.writeValueAsString(activitySet)
				distance = this@TripService.distance.toInt()
				modifiedTimestamp = System.currentTimeMillis()
				endTime = endTime ?: System.currentTimeMillis()
				tripTable.insert(this)
			}
			status = STATUS.STOPPED
		}
	}

	fun saveTrip(title: String) {
		if (status == STATUS.STOPPED || status == STATUS.CRASH_RECOVERY) {
			status = STATUS.PROCESSING

			var distance = 0.0
			val latLngList: MutableList<LatLng> = mutableListOf()
			var minLatitude: Double = Double.MAX_VALUE
			var maxLatitude: Double = -Double.MAX_VALUE
			var minLongitude: Double = Double.MAX_VALUE
			var maxLongitude: Double = -Double.MAX_VALUE

			fun calcCoordinateRange(latLng: LatLng) {
				minLatitude = minOf(minLatitude, latLng.latitude)
				maxLatitude = maxOf(maxLatitude, latLng.latitude)
				minLongitude = minOf(minLongitude, latLng.longitude)
				maxLongitude = maxOf(maxLongitude, latLng.longitude)
			}

			try {
				val baseFile = Utility.readDataFile(primaryKey, "0")
				val baseTripData: TripData = Utility.readEncryptedFileAsObject(baseFile)
				var currentLatLng = LatLng(trip!!.startLatitude, trip!!.startLongitude)
				val baseTripLocationDataList = baseTripData.tripLocationDataMap.toList()
				baseTripLocationDataList.forEach {
					val nextLatLng = LatLng(it.second[0], it.second[1])
					latLngList.add(nextLatLng)
					calcCoordinateRange(nextLatLng)

					distance += SphericalUtil.computeDistanceBetween(currentLatLng, nextLatLng)
					currentLatLng = nextLatLng
				}
				var pointer = baseTripData.next
				while (pointer != null) {
					val nextFile = Utility.readDataFile(primaryKey, pointer)
					val nextTripData: TripData = Utility.readEncryptedFileAsObject(nextFile)
					val nextTripLocationDataList = nextTripData.tripLocationDataMap.toList()
					nextTripLocationDataList.forEach {
						val nextLatLng = LatLng(it.second[0], it.second[1])
						latLngList.add(nextLatLng)
						calcCoordinateRange(nextLatLng)

						distance += SphericalUtil.computeDistanceBetween(currentLatLng, nextLatLng)
						currentLatLng = nextLatLng
					}
					pointer = nextTripData.next
				}
			} catch (exception: Exception) {
			}

			trip!!.apply {
				this.title = if (title.isBlank()) {
					"Trip on ${Utility.timestampToPretty(primaryKey)}"
				} else {
					title
				}
				this.distance = distance.toInt()
				this.isActive = false
			}
			ioScope.launch { tripTable.insert(trip!!) }
			Utility.getMapSnapShot(this@TripService, trip!!)
			status = STATUS.SUCCEED
			sharedPreferencesAppConfig.edit().putLong("TRIP_PRIMARY_KEY", -1L).commit()
			resetData()
		}
	}

	fun discardTrip() {
		Log.i(TAG, "discardTrip...")
		if (status == STATUS.STOPPED || status == STATUS.CRASH_RECOVERY) {
			Log.i(TAG, "discardTrip...")
			status = STATUS.PROCESSING
			try {
				val baseFile = Utility.readDataFile(primaryKey, "0")
				val baseFileData: TripData = Utility.readEncryptedFileAsObject(baseFile)
				baseFile.delete()
				var pointer = baseFileData.next
				while (pointer != null) {
					val nextFile = Utility.readDataFile(primaryKey, pointer)
					val nextFileData: TripData = Utility.readEncryptedFileAsObject(nextFile)
					nextFile.delete()
					pointer = nextFileData.next
				}
			} catch (exception: Exception) {
			}
			ioScope.launch { tripTable.delete(trip!!) }
			status = STATUS.SUCCEED
			sharedPreferencesAppConfig.edit().putLong("TRIP_PRIMARY_KEY", -1L).commit()
			resetData()
		}
	}

	private fun resetData() {
		primaryKey = -1
		trip = null
		tripData = null
		toSavedFile = null
		activitySet = mutableSetOf()
		fileList = mutableListOf()
		distance = 0.0

		status = STATUS.NOT_STARTED
		crashRecovery()
	}

	private fun updateFileSet() {
		if (toSavedFile!!.name.length == 29) {
			fileList.add(toSavedFile!!.name.substring(23, 24))
		} else {
			fileList.add(toSavedFile!!.name.substring(23, 31))
		}
		ioScope.launch { tripTable.insert(trip!!) }
	}

	private fun initializeNotificationUpdater() {
		NotificationChannel(TRIP_NOTIFICATION_ID, "Trip Notification", NotificationManager.IMPORTANCE_NONE).apply {
			lockscreenVisibility = NotificationCompat.VISIBILITY_PRIVATE
			notificationManager.createNotificationChannel(this)
		}

		notification = NotificationCompat.Builder(this@TripService, TRIP_NOTIFICATION_ID)
			.setStyle(NotificationCompat.BigTextStyle())
			.setContentTitle("Triplee running")
			.setContentText("Distance : ${distance.toInt()}")
			.setLargeIcon(getBitmap(ActivityId.ACTIVITY_ID_RES_MAP[currentActivity]!!, Color.BLACK))
			.setSubText(ActivityId.ACTIVITY_ID_NAME_MAP[currentActivity])
			.setNumber(1)
			.setOnlyAlertOnce(true)
			.setUsesChronometer(true)
			.setSmallIcon(R.drawable.ic_record)
			.setColorized(true)
			.setContentIntent(notificationPendingIntent)
		startForeground(1, notification!!.build())
		notificationHandler.post(notificationRunnable)
	}

	private fun startActivityTransition() {

		val activityTransitionList = listOf(
			ActivityTransition.Builder()
				.setActivityType(DetectedActivity.IN_VEHICLE)
				.setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
				.build(),
			ActivityTransition.Builder()
				.setActivityType(DetectedActivity.ON_BICYCLE)
				.setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
				.build(),
			ActivityTransition.Builder()
				.setActivityType(DetectedActivity.RUNNING)
				.setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
				.build(),
			ActivityTransition.Builder()
				.setActivityType(DetectedActivity.STILL)
				.setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
				.build(),
			ActivityTransition.Builder()
				.setActivityType(DetectedActivity.WALKING)
				.setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
				.build()
		)

		val activityTransitionReceiver = ActivityTransitionReceiver()
		registerReceiver(activityTransitionReceiver, IntentFilter(ACTIVITY_TRANSITION_RECEIVER_ACTION))
		enableActivityTransition(activityTransitionList)
	}

	private fun enableActivityTransition(activityTransitionList: List<ActivityTransition>) {
		val request = ActivityTransitionRequest(activityTransitionList)

		ActivityRecognition.getClient(this@TripService).requestActivityTransitionUpdates(request, activityTransitionPendingIntent!!)
			.addOnSuccessListener {
				Log.i(TAG, "activity transition added")
			}
			.addOnFailureListener { e ->
				e.printStackTrace()
			}
	}

	inner class ActivityTransitionReceiver : BroadcastReceiver() {

		override fun onReceive(context: Context, intent: Intent) {
			if (!TextUtils.equals(ACTIVITY_TRANSITION_RECEIVER_ACTION, intent.action)) {
				Log.i(TAG, "unknown error...")
				return
			}
			if (ActivityTransitionResult.hasResult(intent)) {
				val result = ActivityTransitionResult.extractResult(intent)
				Log.i(TAG, "onReceiveActivity ${result?.transitionEvents?.size}")
				for (event in result?.transitionEvents!!) {
					if (event.transitionType == 0) {    // On transition enter
						Log.i(TAG, "${event.activityType}")
						dumpAndOrSwitchActivity(pseudoActivity = event.activityType)
					}
				}
			}
		}
	}

	private fun getBitmap(drawableRes: Int, tint: Int): Bitmap? {
		val drawable = ResourcesCompat.getDrawable(resources, drawableRes, null)!!
		drawable.setTint(tint)
		val canvas = Canvas()
		val bitmap = Bitmap.createBitmap(64, 64, Bitmap.Config.ARGB_8888)
		canvas.setBitmap(bitmap)
		drawable.setBounds(16, 16, 48, 48)
		drawable.draw(canvas)
		return bitmap
	}

	private fun Double.round(decimals: Int): Double {
		var multiplier = 1.0
		repeat(decimals) { multiplier *= 10 }
		return kotlin.math.round(this * multiplier) / multiplier
	}

	companion object {
		const val TAG = "TRIP_SERVICE"
		const val TRIP_NOTIFICATION_ID = "TRIP_NOTIFICATION_ID"
		const val ACTIVITY_TRANSITION_RECEIVER_ACTION: String = BuildConfig.APPLICATION_ID + "ACTIVITY_TRANSITION_RECEIVER_ACTION"
	}
}
