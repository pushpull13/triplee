package com.pushpull.triplee

import com.pushpull.triplee.alice.Alice
import org.junit.Test


class AliceUnitTest {
	@Test
	fun testEncrypt() {
		val plainTest = "Looking down the misty path to uncertain destinations."
		val password = "tetramand"

		val encryptedText = Alice.encrypt(plainTest, password)
		val decryptedText = encryptedText?.let { Alice.decrypt(it, password) }

		println(encryptedText)
		println(decryptedText)
		assert(decryptedText != null)
		assert(decryptedText == plainTest)
	}
}
